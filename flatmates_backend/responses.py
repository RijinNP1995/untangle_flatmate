from rest_framework import status
from rest_framework.response import Response


def fetch_data_response(model_info, data):
    """success response"""
    return Response(
        {
            'status': 200,
            'message': f'{model_info} details fetched successfully',
            'data': data
        },
        status=status.HTTP_200_OK
    )


def get_success_200(user, token):
    return Response(
        {
            'status': 200,
            'message': 'success',
            'data': user.username.__str__(),
            'token': token[0].key
        },
        status=status.HTTP_200_OK)


def get_conflicts_409(user_id):
    return Response(
        {
            'status': 409,
            'message': f'You are already registered with us. Please login to continue.',
            'data': user_id,
        },
        status=status.HTTP_409_CONFLICT
    )


def get_conflicts(time_otp):
    return Response(
        {
            'status': 409,
            'message': f'You are already registered with us. Please complete the registration',
            'OTP': time_otp,
        },
        status=status.HTTP_409_CONFLICT
    )


def get_otp(time_otp):
    return Response(
        {
            'status': 200,
            'message': f'User OTP: {time_otp} sent successfully',
        },
        status=status.HTTP_200_OK
    )


def api_failed():
    return Response(
        {
            'status': 400,
            'message': 'failed',
            'error': 'Invalid user'
        },
        status=status.HTTP_400_BAD_REQUEST
    )


def verify_otp():
    return Response(
        {
            'status': 202,
            'message': 'OTP verification successful',
        },
        status=status.HTTP_202_ACCEPTED
    )


def otp_verification_failed():
    return Response(
        {
            'status': 400,
            'message': 'Failed',
            'error': 'OTP verification failed',
        },
        status=status.HTTP_400_BAD_REQUEST
    )


def create_password():
    return Response(
        {
            'status': 201,
            'message': 'Users password successfully created'
        },
        status=status.HTTP_201_CREATED
    )


def check_the_user_id():
    return Response(
        {
            'status': 400,
            'message': 'Please check your user_id'
        },
        status=status.HTTP_400_BAD_REQUEST
    )


def task_un_complete():
    return Response(
        {
            "status": 406,
            "message": "Please Rate the task which is completed"
        },
        status.HTTP_406_NOT_ACCEPTABLE
    )


def cant_rate():
    return Response(
        {
            "status": 406,
            "message": "You cant rate your own task"
        },
        status.HTTP_406_NOT_ACCEPTABLE
    )


def unauthorized_user():
    return Response(
        {
            'status': 401,
            'message': 'failed',
            'error': 'User email/phone is not verified'
        },
        status=status.HTTP_401_UNAUTHORIZED
    )


def user_not_exist():
    return Response(
        {
            'status': 400,
            'message': 'failed',
            'error': 'User does not exists'
        },
        status=status.HTTP_400_BAD_REQUEST
    )


def user_sign_in(token, user, in_the_flat, flat_id, user_data, flat_ser, user_percentage, active_request=False):
    return Response(
        {
            'status': 200,
            'user_id': user.id,
            'have_flat': in_the_flat,
            'flat_id': flat_id,
            'message': 'success',
            'data': 'User log in successful',
            'token': token.key,
            'user_data': user_data.data,
            'flat_data': flat_ser.data,
            'percentage': user_percentage,
            'active_request': active_request
        },
        status=status.HTTP_200_OK
    )


def fac_auth_success(token, new_user, user, in_the_flat, flat_id, user_data, flat_ser, user_percentage,
                     active_request=False):
    return Response(
        {
            'status': 200,
            'user_id': user.id,
            'have_flat': in_the_flat,
            'flat_id': flat_id,
            'message': 'success',
            'token': str(token[0].key),
            'new_user': new_user,
            'user_data': user_data.data,
            'flat_data': flat_ser.data,
            'percentage': user_percentage,
            'active_request': active_request
        },
        status=status.HTTP_200_OK
    )


def google_auth_success(user, token, in_the_flat, flat_id, user_data, flat_ser, user_percentage, active_request=False):
    return Response(
        {
            'status': 200,
            'user_id': user.id,
            'have_flat': in_the_flat,
            'flat_id': flat_id,
            'message': 'success',
            'token': str(token[0].key),
            'user_data': user_data.data,
            'flat_data': flat_ser.data,
            'percentage': user_percentage,
            'active_request': active_request
        },
        status=status.HTTP_200_OK
    )


def invalid_password():
    return Response(
        {
            'status': 400,
            'message': 'failed',
            'error': 'Invalid user password'
        },
        status=status.HTTP_400_BAD_REQUEST
    )


def not_found():
    return Response(
        {
            'status': 404,
            'message': 'failed',
            'error': "User not found"
        },
        status=status.HTTP_404_NOT_FOUND
    )


def update_user_data(serializer, user_percentage=None):
    return Response(
        {
            'status': 200,
            'message': 'success',
            'data': serializer.data,
            'percentage': user_percentage,
        },
        status=status.HTTP_200_OK
    )


def update_success(data=None):
    return Response(
        {
            'status': 200,
            'message': 'updated the data successfully',
            'data': data
        }
    )


def update_failed(serializer):
    return Response(
        {
            'status': 400,
            'message': 'failed',
            'error': serializer.errors
        },
        status=status.HTTP_400_BAD_REQUEST
    )


def get_profile_data(user_info, total):
    return Response(
        {
            'status': 200,
            'message': 'success',
            'data': user_info.data,
            'percentage': total
        },
        status=status.HTTP_200_OK
    )


def no_data_found():
    return Response(
        {
            'status': 204,
            'message': 'failed',
            'error': 'no data found'
        },
        status=status.HTTP_204_NO_CONTENT
    )


def upload_success():
    return Response(
        {
            'status': 201,
            'message': 'photo upload successfully',
        },
        status=status.HTTP_201_CREATED
    )


def upload_failed(serializer):
    return Response(
        {
            'status': 400,
            'message': 'something went wrong',
            'error': serializer.errors,
        },

        status=status.HTTP_400_BAD_REQUEST
    )


def view_photo(user_profile_photo):
    return Response(
        {
            'status': 200,
            'message': 'success',
            'data': user_profile_photo.data
        },
        status=status.HTTP_200_OK
    )


def delete_profile():
    return Response(
        {
            "status": 200,
            "message": "Profile details deleted successfully"
        },
        status.HTTP_200_OK
    )


def delete_photo():
    return Response(
        {
            "status": 200,
            "message": "Profile photo deleted successfully"
        },
        status.HTTP_200_OK
    )


def delete_task(serializer, flat_ser):
    return Response(
        {
            "status": 200,
            "message": "Task details deleted successfully",
            "task_data": serializer.data,
            "flat_data": flat_ser.data
        },
        status.HTTP_200_OK
    )


def delete_participant(assign_ser):
    return Response(
        {
            "status": 200,
            "message": "Participant removed from the task",
            "data": assign_ser.data

        },
        status.HTTP_200_OK
    )


def search_result(people_count, serializer):
    return Response(
        {
            "status": 200,
            'message': 'People data fetched successfully.',
            'peoples count': people_count,
            'search data': serializer.data
        },
        status=status.HTTP_200_OK)


def add_task(assign_task_ser):
    return Response(
        {
            'status': 201,
            'message': 'New task created successfully',
            'data': assign_task_ser.data
        },
        status=status.HTTP_201_CREATED
    )


def view_task(task_ser, flat_data):
    return Response(
        {
            'status': 200,
            'message': 'Task fetched successfully',
            'data': task_ser.data,
            'flat_data': flat_data.data
        },
        status=status.HTTP_200_OK
    )


def view_task_by_id(task_ser):
    return Response(
        {
            'status': 200,
            'message': 'Task fetched successfully',
            'data': task_ser.data
        },
        status=status.HTTP_200_OK
    )


def view_task_fail(serializer):
    return Response(
        {
            'status': 400,
            'message': 'Bad request',
            'data': serializer.errors,
        },
        status=status.HTTP_400_BAD_REQUEST
    )


def get_task_badge(task_value):
    return Response(
        {
            "status": 200,
            "message": "success",
            "data": task_value
        },
        status.HTTP_200_OK
    )


def get_badge_data(serializer, percentage):
    return Response(
        {
            "status": 200,
            "message": "success",
            "data": serializer.data,
            'badge_percentage': percentage
        },
        status.HTTP_200_OK
    )


def update_task(serializer):
    return Response(
        {
            'status': 200,
            'message': 'Update task successfully',
            'data': serializer.data
        },
        status=status.HTTP_200_OK
    )


def invalid_serializer(serializer):
    return Response(
        {
            'status': 400,
            'message': 'something went wrong',
            'error': serializer.errors,
        },
        status=status.HTTP_400_BAD_REQUEST
    )


def add_flat(flat_id, data=''):
    return Response(
        {
            'status': 201,
            'message': 'New post added successfully',
            'flat_id': flat_id,
            'data': data
        },
        status=status.HTTP_201_CREATED
    )


def invalid_id():
    return Response(
        {
            'status': 400,
            'message': 'Invalid task_id or user_id.',
        },
        status=status.HTTP_400_BAD_REQUEST
    )


def invalid_data(serializer):
    return Response(
        {
            'status': 400,
            'message': 'Invalid data.',
            'error': serializer.errors,
        },
        status=status.HTTP_400_BAD_REQUEST
    )


def invalid_user():
    return Response(
        {
            'status': 400,
            'message': 'Invalid user.',
        },
        status=status.HTTP_400_BAD_REQUEST
    )


def update_flat_data(serializer, flat_id=None):
    return Response(
        {
            'status': 200,
            'message': 'Update success',
            'data': serializer.data,
            'flat_id': serializer.data['id']
        },
        status=status.HTTP_200_OK
    )


def flat_does_not_exist():
    return Response(
        {
            'status': 400,
            'message': 'Flat does not exist.',
        },
        status=status.HTTP_400_BAD_REQUEST
    )


def invalid_ser():
    return Response(
        {
            'status': 400,
            'message': 'Something went wrong.',
        },
        status=status.HTTP_400_BAD_REQUEST
    )


def permission_denied():
    return Response(
        {
            'status': 400,
            'message': 'User does not have permission.',
        },
        status=status.HTTP_403_FORBIDDEN
    )


def remove_flat():
    return Response(
        {
            'status': 200,
            'message': 'Flat removed.',
        },
        status=status.HTTP_200_OK
    )


def load_flat(flat_ser):
    return Response(
        {
            'status': 200,
            'message': 'Flat loaded successfully',
            'data': flat_ser.data
        },
        status=status.HTTP_200_OK
    )


def sent_request():
    return Response(
        {
            'status': 200,
            'message': 'Request sent',
        },
        status=status.HTTP_200_OK
    )


def invalid_data_403():
    return Response(
        {
            'status': 403,
            'message': 'Invalid data',
        },
        status=status.HTTP_403_FORBIDDEN
    )


def load_request_list(request_list_ser):
    return Response(
        {
            'status': 200,
            'message': 'Request list loaded successfully',
            'data': request_list_ser.data
        },
        status=status.HTTP_200_OK
    )


def accept_request():
    return Response(
        {
            'status': 200,
            'message': 'Accept request.'
        },
        status=status.HTTP_200_OK
    )


def invalid_request_id():
    return Response(
        {
            'status': 400,
            'message': 'Invalid request id.',
        },
        status=status.HTTP_400_BAD_REQUEST
    )


def reject_request():
    return Response(
        {
            'status': 200,
            'message': 'Reject request.',
        },
        status=status.HTTP_200_OK
    )


def cancel_request():
    return Response(
        {
            'status': 200,
            'message': 'Cancel request.',
        },
        status=status.HTTP_200_OK
    )


def exit_flat():
    return Response(
        {
            'status': 200,
            'message': 'Exit from flat.',
        },
        status=status.HTTP_200_OK
    )


def exception_response(e):
    return Response(
        {
            'status': 403,
            'error': e.__str__()
        },
        status=status.HTTP_403_FORBIDDEN
    )


def id_does_not_exist():
    return Response(
        {
            'status': 400,
            'message': 'failed',
            'error': 'Id does not exists'
        },
        status=status.HTTP_400_BAD_REQUEST
    )


def error_400(message):
    return Response(
        {
            'status': 400,
            'message': message,
        },
        status=status.HTTP_400_BAD_REQUEST
    )


def update_task_status(serializer):
    return Response(
        {
            'status': 200,
            'message': 'Task status updated successfully',
            'data': serializer.data
        },
        status=status.HTTP_200_OK
    )


def give_badge():
    return Response(
        {
            'status': 200,
            'message': 'Task badge given successfully',
        },
        status=status.HTTP_200_OK
    )


def update_badge(task_ser):
    return Response(
        {
            'status': 200,
            'message': 'Update the badges successfully',
            'data': task_ser.data
        },
        status=status.HTTP_200_OK
    )


def create_object(model_info, data):
    """success response"""
    return Response(
        {
            'status': 201,
            'message': f'{model_info} added successfully',
            'data': data
        },
        status=status.HTTP_201_CREATED
    )


def ratings_data_response(model_info, data, avg_rating):
    """success response"""
    return Response(
        {
            'status': 200,
            'message': f'{model_info} details fetched successfully',
            'data': data,
            'rating': avg_rating
        },
        status=status.HTTP_200_OK
    )


def error_403(message):
    return Response(
        {
            'status': 403,
            'message': message,
        },
        status=status.HTTP_403_FORBIDDEN
    )


def error_409(message):
    return Response(
        {
            'status': 409,
            'message': message,
        },
        status=status.HTTP_409_CONFLICT
    )


def success_message(message="", data=''):
    return Response(
        {
            'status': 200,
            'message': message,
            'data': data
        },
        status=status.HTTP_200_OK
    )


def execution_issue(message=""):
    return Response(
        {
            'status': 400,
            'message': message,
        },
        status=status.HTTP_400_BAD_REQUEST
    )


def check_user_have_flat(message='', flat_value=False, data=None, active_request=False, flat_id=None):
    return Response(
        {
            'status': 200,
            'message': message,
            'have_flat': flat_value,
            'data': data
        },
        status=status.HTTP_200_OK
    )


def duplicate_entry():
    return Response(
        {
            'status': 406,
            'message': 'Duplicate number not allowed'
        },
        status=status.HTTP_406_NOT_ACCEPTABLE
    )


def success_data_response(model_info, data, count):
    """success response"""
    return Response(
        {
            'status': 200,
            'message': f'{model_info} details fetched successfully',
            'data': data,
            'count': count
        },
        status=status.HTTP_200_OK
    )


def service_search_result(service_count, ser_service_provider_list):
    """search services"""
    return Response(
        {
            "status": 200,
            'message': 'Service provider data fetched successfully.',
            'peoples count': service_count,
            'search data': ser_service_provider_list.data
        },
        status=status.HTTP_200_OK)


def create_expense(user, expense_id):
    """create group for expense management"""
    return Response(
        {
            'status': 201,
            'message': f'{user.display_name} added expense successfully',
            'expense_id': expense_id
        },
        status=status.HTTP_201_CREATED
    )


def update_group(user):
    """update the expense details"""
    return Response(
        {
            'status': 200,
            'message': f'{user.display_name} updated the expenses successfully'
        },
        status=status.HTTP_200_OK
    )


def delete_group(expense_owed_ser):
    """Delete the group details"""
    return Response(
        {
            'status': 200,
            'message': 'Delete the group successfully',
            'data': expense_owed_ser.data
        },
        status=status.HTTP_200_OK
    )


def view_expense_details(expense_ser):
    """View the expense details"""
    return Response(
        {
            'status': 200,
            'message': 'Get the expenses successfully',
            'data': expense_ser.data
        },
        status=status.HTTP_200_OK
    )


def amount_not_match():
    """ spent_amount and total amount is not match"""
    return Response(
        {
            'status': 406,
            'message': 'spent_amount and total amount is not match'
        },
        status=status.HTTP_406_NOT_ACCEPTABLE
    )


def remover_user_from_expense(expense_data_ser):
    """Remove a particular user from the expense data"""
    return Response(
        {
            'status': 200,
            'message': 'remove the user from the particular expense',
            'data': expense_data_ser.data

        },
        status=status.HTTP_200_OK

    )


def settle_expense(settled_data):
    """Settle the expense"""
    return Response(
        {
            'status': 202,
            'message': 'settle the expense of particular user',
            'data': settled_data.data
        },
        status=status.HTTP_202_ACCEPTED
    )

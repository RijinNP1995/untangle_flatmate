from flatmates_backend.base_manager import BaseManager


class NotificationManager(BaseManager):
    # for notifications
    def get_all_notifications(self, **filter_args):
        return self.get_all().filter(**filter_args)

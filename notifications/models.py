from django.db import models
from notifications.managers import NotificationManager
from user import models as user_models
from master import models as master_models

NOTIFICATION_TYPE = (
    ('NEW_REQ', 'New Request'),
    ('REQ_RES_ACCEPT', 'Request Accept'),
    ('REQ_RES_REJECT', 'Request Reject'),
    ('NEW_TASK', 'New Task'),
    ('TASK_COMPLETED', 'Task Completed'),
    ('REMOVED_TENANT', 'Removed Tenant'),
    ('EXIT_FLAT', 'Exit Flat'),
    ('NEW_EVENT', 'New Event'),
)


class Notification(models.Model):
    user = models.ForeignKey(user_models.User,
                             on_delete=models.CASCADE, related_name='notification')
    actor = models.CharField(max_length=50, blank=True, null=True)
    verb = models.CharField(max_length=250, blank=True, null=True)
    action = models.CharField(max_length=150, blank=True, null=True)
    type = models.CharField(
        max_length=25, default='NEW_REQ', choices=NOTIFICATION_TYPE)
    description = models.TextField(blank=True, null=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    unread = models.BooleanField(default=True, blank=False, db_index=True)
    icon = models.ForeignKey(master_models.NotificationIcon,
                             on_delete=models.CASCADE)

    objects = NotificationManager()

    def __str__(self):
        return f"{self.actor} {self.verb} {self.action} {self.type} at {self.timestamp}"

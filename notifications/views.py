from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.views import APIView

from core import log_data
from flatmates_backend import responses
from .services import NotificationServices

notification_services = NotificationServices()


class NotificationList(APIView):
    def get(self, request):
        """
        list the notifications
        :param request: contains user
        :return: list of notifications
        """
        try:
            return notification_services.get_notification_list(user=request.user)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)

    @swagger_auto_schema(
        operation_id='Notification mark as read',
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                'pk': openapi.Schema(
                    type=openapi.TYPE_INTEGER,
                )
            }
        ),
    )
    def patch(self, request, pk):
        """
        single notification mark as read, update the boolean field
        :param pk: primary key(id) of notification
        :param request: contains user
        :return: list of notifications
        """
        try:
            return notification_services.read_notification(user=request.user, pk=pk)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)


class ReadAllNotifications(APIView):
    @swagger_auto_schema(
        operation_id='All notifications mark as read',
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
            }
        ),
    )
    def patch(self, request):
        """
        all user notifications mark as read, update the boolean field
        :param request: contains user
        :return: list of notifications
        """
        try:
            return notification_services.read_all_notifications(user=request.user)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)

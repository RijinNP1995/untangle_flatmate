import json
import requests

serverToken = "AAAA0ZLTS3o:APA91bF3dKaJeKTzMAtUZC6KkW5Xa3azlilsksTqckJBbmNO4W8N2BN46haTGsnL74gd9rjDCruQYv_3Pj4flzanYu5jcLl4PsaM3BsEE-4wxWSl9lg0mh_1s8XVTDyX_04PaEr16uel"


def send_notification(device_id, data, title, message_body):
    """
    method for firebase push notifications
    :param device_id: device id
    :param data: extra data ex: type and id
    :param title: notification title
    :param message_body: message string
    :return: message response
    """
    headers = {
        "Content-Type": "application/json",
        "Authorization": "key=" + serverToken
    }
    body = {
        'notification': {
            'title': title,
            'body': message_body,
        },
        'to': device_id,
        'priority': 'high',
        'data': data,
    }
    response = requests.post("https://fcm.googleapis.com/fcm/send", headers=headers, data=json.dumps(body))
    # print(response.json())
    return response

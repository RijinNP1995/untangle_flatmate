from django.db.models import Count, Q

from core import log_data
from flatmates_backend import responses
from master.models import NotificationIcon
from notifications.push_notification import send_notification
from notifications.serializers import NotificationListSerializer
from user import models as user_models
from .models import Notification


def notify(user, actor, verb, action='', notification_type='', description='', icon='', title='', details=''):
    """
    create a single user notification
    :param details: complete details of object.
    :param title: notification title
    :param icon: notification icon
    :param user: user
    :param actor: The object that performed the activity.
    :param verb: The verb phrase that identifies the action of the activity
    :param action: An object of any type. (Optional)
    :param notification_type: The object to which the activity was performed. (Optional)
    :param description: An string. (Optional)
    :return: notification object
    """

    if notification_type:
        if NotificationIcon.objects.get_by_filter(label=notification_type).exists():
            icon = NotificationIcon.objects.get(label=notification_type)
        else:
            icon = NotificationIcon.objects.get(label='FLAT')

    n = user.notification.create(
        actor=actor, verb=verb, action=action, type=notification_type, description=description, icon=icon)
    if user.fcm_token:
        msg_body = actor + ' ' + verb
        notification_data = {
            'type': notification_type,
            'id': action,
            'data': details
        }
        send_notification(data=notification_data,
                          title=title,
                          message_body=msg_body,
                          device_id=user.fcm_token)

    return n


def multi_notify(users, actor, verb, action='', notification_type='', description='', icon='', title='', details=''):
    """
    create multiple users notification
    :param details: complete details of object.
    :param title:
    :param icon: notification icon
    :param users: users
    :param actor: The object that performed the activity.
    :param verb: The verb phrase that identifies the action of the activity
    :param action: An object of any type. (Optional)
    :param notification_type: The object to which the activity was performed. (Optional)
    :param description: An string. (Optional)
    :return: notification object
    """
    if notification_type:
        if NotificationIcon.objects.get_by_filter(label=notification_type).exists():
            icon = NotificationIcon.objects.get(label=notification_type)
        else:
            icon = NotificationIcon.objects.get(label='FLAT')

    if users:
        for u in user_models.User.objects.filter(pk__in=users):
            u.notification.create(
                actor=actor, verb=verb, action=action, type=notification_type, description=description, icon=icon)
            if u.fcm_token:
                msg_body = actor + ' ' + verb
                notification_data = {
                    'type': notification_type,
                    'id': action,
                    'data': details
                }
                send_notification(data=notification_data,
                                  title=title,
                                  message_body=msg_body,
                                  device_id=u.fcm_token)


class NotificationServices:
    def get_notification_list(self, user):
        """
        get the list of notifications
        :param user: logged in user
        :return: list of notifications
        """
        if user:
            notifications_list = Notification.objects.get_all_notifications(user=user).order_by('-timestamp')
            notifications_list_ser = NotificationListSerializer(notifications_list, many=True)
            return responses.success_data_response(model_info="Notification",
                                                   data=notifications_list_ser.data,
                                                   count=notifications_list.aggregate(
                                                       unread_notifications=Count('pk',
                                                                                  filter=Q(unread=True)
                                                                                  ))['unread_notifications']
                                                   )
        log_data.error(f'user does not exist')
        return responses.invalid_user()

    def read_notification(self, user, pk):
        """
        read the notification
        :param pk:
        :param user: logged in user
        :return: success message
        """
        if user:
            if Notification.objects.filter(id=pk).exists():
                notification_obj = Notification.objects.get(pk=pk)
                notification_obj.unread = False
                notification_obj.save()
            return self.get_notification_list(user=user)
        log_data.error(f'user does not exist')
        return responses.invalid_user()

    def read_all_notifications(self, user):
        """
        read the notification
        :param user: logged in user
        :return: success message
        """
        if user:
            notifications_list = Notification.objects.get_all_notifications(user=user)
            notifications_list.update(unread=False)
            return self.get_notification_list(user=user)
        log_data.error(f'user does not exist')
        return responses.invalid_user()

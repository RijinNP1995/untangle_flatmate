from rest_framework import serializers
from notifications import models
from master import serializers as master_ser
from master import models as master_model


class NotificationListSerializer(serializers.ModelSerializer):
    """
    Serializer class for notifications
    """
    icon_details = serializers.SerializerMethodField()

    class Meta:
        model = models.Notification
        fields = ['id', 'user', 'actor', 'verb', 'action',
                  'type', 'description', 'timestamp', 'unread', 'icon_details']

    def get_icon_details(self, obj):
        """
        get the notification icon details
        :param obj: instance
        :return: icon details
        """
        if obj.icon:
            icon_data = master_model.NotificationIcon.objects.get_by_id(pk=obj.icon.id)
            icon_data_ser = master_ser.NotificationIconSerializer(icon_data)
            return icon_data_ser.data
        return ''

from django.urls import path
from . import views

urlpatterns = [
    # to get the notification list
    path('notification-list/', views.NotificationList.as_view(), name='notification_list'),
    # make a single notification as read
    path('read-notification/<int:pk>/', views.NotificationList.as_view(), name='read-notification'),
    # make all notification as read
    path('read-all-notification/', views.ReadAllNotifications.as_view(), name='read-all-notification'),
]

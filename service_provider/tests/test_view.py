import json

from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from flat_api.models import Flat
from master.models import ServiceCategory, Amenity
from user import models as user_model


class MyTestCase(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.gender = user_model.Gender(1)
        self.locality = 'Kochi'
        self.gender.save()
        self.user = user_model.User(
            first_name='Rijin',
            last_name='NP',
            display_name='Rijin',
            gender=self.gender,
            phone='+919895203121',
            profession='Software engineer',
            company_name='Untangle',
            age=24,
            locality=self.locality,
            description='#test description',
            quote='test_quote'
        )
        self.user.save()
        self.token = Token.objects.get_or_create(user=self.user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token[0].key)
        self.service_category = ServiceCategory.objects.create(label='test_label',
                                                               created_by=self.user)
        self.amenities = Amenity.objects.create(label='test_label', description='test_description',
                                                created_by=self.user)
        # enter the valid payload
        self.flat = Flat(
            flat_name="my flat",
            flat_no="house no",
            landmark="landmark",
            city="kochi",
            rent_per_month=2000,
            num_of_beds=3,
            flat_description="short description about flat/house",
            flatmate_description="short description about expected flatmate",
            flatmate_min_age=18,
            flatmate_max_age=40,
            expected_gender="male",
            lat=10.0014,
            lon=76.3101,
        )
        self.flat.save()

        self.valid_add_service_payload = {
            "contact_name": "prasad",
            "contact_no": "9048893654",
            "private": True,
            "service_category": 1,
            "flat": 1
        }
        self.invalid_add_service_contact_name = {
            "contact_name": True,
            "contact_no": "9048893654",
            "private": True,
            "service_category": 1,
            "flat": 1
        }

        self.invalid_add_service_contact_no = {
            "contact_name": "prasad",
            "contact_no": True,
            "private": True,
            "service_category": 1,
            "flat": 1
        }
        self.invalid_add_service_private = {
            "contact_name": "prasad",
            "contact_no": "9048893654",
            "private": 10,
            "service_category": 1,
            "flat": 1
        }
        self.invalid_add_service_category = {
            "contact_name": "prasad",
            "contact_no": "9048893654",
            "private": True,
            "service_category": "Laundry",
            "flat": 1
        }
        self.invalid_add_service_flat = {
            "contact_name": "prasad",
            "contact_no": "9048893654",
            "private": True,
            "service_category": 1,
            "flat": "Confident group"
        }

    def test_add_service(self):
        """
        :return: Pass the valid data to add the service provider
        """
        response = self.client.post(
            reverse('add_service_provider'),
            data=json.dumps(self.valid_add_service_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['message'], 'Added New service provider successfully.')

    def test_invalid_contact_name(self):
        """
        :return: Pass the invalid contact name to add the service provider
        """
        response = self.client.post(
            reverse('add_service_provider'),
            data=json.dumps(self.invalid_add_service_contact_name),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['error'],
                         '{\'contact_name\': [ErrorDetail(string=\'Not a valid string.\', code=\'invalid\')]}')

    def test_invalid_contact_no(self):
        """
        :return: Pass the invalid contact no to add the service provider
        """
        response = self.client.post(
            reverse('add_service_provider'),
            data=json.dumps(self.invalid_add_service_contact_no),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['error'],
                         '{\'contact_no\': [ErrorDetail(string=\'Not a valid string.\', code=\'invalid\')]}')

    def test_invalid_boolean(self):
        """
        :return: Pass the invalid contact no to add the service provider
        """
        response = self.client.post(
            reverse('add_service_provider'),
            data=json.dumps(self.invalid_add_service_private),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['error'],
                         '{\'private\': [ErrorDetail(string=\'Must be a valid boolean.\', code=\'invalid\')]}')

    def test_invalid_category(self):
        """
        :return: Pass the invalid category id to add the service provider
        """
        response = self.client.post(
            reverse('add_service_provider'),
            data=json.dumps(self.invalid_add_service_category),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['error'],
                         '{\'service_category\': [ErrorDetail(string=\'Incorrect type. Expected pk value, received str.\', code=\'incorrect_type\')]}')

    def test_invalid_flat(self):
        """
        :return: Pass the invalid category id to add the service provider
        """
        response = self.client.post(
            reverse('add_service_provider'),
            data=json.dumps(self.invalid_add_service_flat),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['error'],
                         '{\'flat\': [ErrorDetail(string=\'Incorrect type. Expected pk value, received str.\', code=\'incorrect_type\')]}')

from django.test import TestCase

from flat_api.models import Flat
from master.models import ServiceCategory
from service_provider.models import ServiceProvider
from user.models import User, Gender


class MyTestCase(TestCase):
    def setUp(self):
        self.contact_name = 'Contact name'
        self.contact_no = '9999999999'
        self.gender = Gender.objects.create(type='male')
        self.created_by = User.objects.create(first_name='first_name', last_name='last_name',
                                              display_name='test',
                                              gender=self.gender,
                                              profession='profession', company_name='company_name', age=20,
                                              locality='locality', phone='9048893681',
                                              description='description',
                                              email='test@email.com',
                                              date_joined='date_joined', is_email_verified=False,
                                              is_phone_verified=False)
        self.service_category = ServiceCategory.objects.create(label='label_name', icon='amenities-icon.jpg',
                                                               created_by=self.created_by)
        self.flat = Flat.objects.create(flat_name='flat_name', flat_no='flat_no', landmark='landmark',
                                        rent_per_month=1500.50, num_of_beds=1,
                                        flat_description='flat_description',
                                        flatmate_description='flatmate_description',
                                        flatmate_min_age=20, flatmate_max_age=30,
                                        expected_gender='male')
        self.private = True
        self.lat = '18.045'
        self.lon = '24.452'
        self.service_provider = ServiceProvider(contact_name=self.contact_name, contact_no=self.contact_no,
                                                service_category=self.service_category, flat=self.flat,
                                                private=self.private, lat=self.lat, lon=self.lon)

    def test_model_service_provider(self):
        old_count = ServiceProvider.objects.count()
        self.service_provider.save()
        new_count = ServiceProvider.objects.count()
        self.assertNotEqual(old_count, new_count)

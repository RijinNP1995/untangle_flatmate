from django.urls import path

from . import views

urlpatterns = [
    # add service provider
    path('add-service-provider/', views.ServiceProvider.as_view(), name='add_service_provider'),
    # update the service provider
    path('update-service-provider/<int:pk>/', views.ServiceProvider.as_view(), name='update_service_providers'),
    # delete the service provider
    path('delete-service-provider/', views.ServiceProvider.as_view(), name='delete_service_provider'),
    # get public service providers
    path('public-service-provider/', views.ServiceProvider.as_view(), name='view_public_service_provider'),
    # get the private service provider
    path('private-service-provider/', views.PrivateServiceProvider.as_view(), name='view_private_service_provider'),
    # get service provider by category
    path('service-provider-by-category/<int:pk>/', views.ServiceProviderByCategory.as_view(),
         name='view_service_provider_by_category'),
    # get private service provider by category
    path('private-provider-by-category/<int:pk>/', views.PrivateServiceProviderByCategory.as_view(),
         name='view_private_service_provider_by_category'),
    # search the service providers
    # path('search-service-provider/', views.SearchServiceProvider.as_view(), name='search_service_provider'),
    path('get_service_list/', views.ViewService.as_view(), name='view_service_list'),
    path('most-viewed-category/', views.MostViewedCategory.as_view(), name='most_viewed_category')
]

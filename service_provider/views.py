import json

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.views import APIView

from core import log_data
from flatmates_backend import responses as message
from .serializers import AddServiceProviderSerializer, UpdateServiceProviderSerializer
from .services import ServiceProviderServices

service_management_service = ServiceProviderServices()


class ServiceProvider(APIView):

    @swagger_auto_schema(request_body=AddServiceProviderSerializer)
    def post(self, request):
        """
        Api to add service provider
        :param request:
        :return:
        """
        try:
            return service_management_service.add_service_provider(user=request.user, data=request.data)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            return message.exception_response(e)

    def get(self, request):
        """
        Api to get the list of public service provider
        :param request:
        :return:
        """
        try:
            return service_management_service.view_public_service_provider()
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            return message.exception_response(e)

    @swagger_auto_schema(request_body=UpdateServiceProviderSerializer)
    def patch(self, request, pk):
        """
        :param request:
        :return: update the service providers
        """
        try:
            return service_management_service.update_service_provider(user=request.user, data=request.data,
                                                                      service_id=pk)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            return message.exception_response(e)

    # API Documentation for Delete Service API
    @swagger_auto_schema(
        operation_id='Delete Service',
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                'flat_id': openapi.Schema(
                    type=openapi.TYPE_INTEGER,
                ),
                'service_id': openapi.Schema(
                    type=openapi.TYPE_INTEGER,
                )
            }
        ),
    )
    def delete(self, request):
        try:
            flat_id = request.data.get('flat_id')
            service_id = request.data.get('service_id')
            return service_management_service.delete_service_provider(service_id=service_id, flat_id=flat_id)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            return message.exception_response(e)


class PrivateServiceProvider(APIView):
    def get(self, request):
        """
        Api to get the list of private service providers
        :param request:
        :return: list of private service providers
        """
        try:
            return service_management_service.view_private_service_provider(flat_id=request.GET.get('flat_id')
                                                                            )
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            return message.exception_response(e)


class ServiceProviderByCategory(APIView):
    def get(self, request, pk):
        """
        :param pk:
        :return:
        """
        try:
            filter_data = {}
            if 'data' in request.GET:
                filter_data = json.loads(request.GET.get('data'))
            return service_management_service.view_provider_by_category(
                data=filter_data, category_id=pk)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            message.exception_response(e)


class PrivateServiceProviderByCategory(APIView):
    def get(self, request, pk):
        """
        :param pk:
        :return:
        """
        try:
            flat_id = request.GET.get('flat')
            return service_management_service.view_private_provider_by_category(category_id=pk, flat_id=flat_id)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            return message.exception_response(e)


class ViewService(APIView):
    def get(self, request):
        """
        :param request:
        :return: return the list of services
        """
        try:
            filter_data = {}
            if 'data' in request.GET:
                filter_data = json.loads(request.GET.get('data'))
            return service_management_service.list_service(
                data=filter_data)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            message.exception_response(e)


class MostViewedCategory(APIView):
    def get(self, request):

        try:
            return service_management_service.mostly_viewed_category()
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            return message.exception_response(e)

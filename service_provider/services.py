from django.db.models import Count

from core import log_data
from core.location_based_data import get_service_list_by_category, get_service_list
from flatmates_backend import responses as message
from . import models
from . import serializers


class ServiceProviderServices:
    # add new service provider details
    def add_service_provider(self, user, data):
        """
        add a new service provider contacts
        :param user: created by
        :param data: data
        :return: added service provider details
        """
        if not user:
            log_data.error(f'user does not exist')
            return message.invalid_user()
        serializer = serializers.AddServiceProviderSerializer(data=data)
        if serializer.is_valid(raise_exception=True):
            service_provider_obj = serializer.save(created_by=user)
            if service_provider_obj.private is True:
                flat_instance = {'flat': data.get('flat'), 'service_provider': service_provider_obj.id}
                assign_flat_service = serializers.AssignFlatServiceSerializer(data=flat_instance)
                if assign_flat_service.is_valid(raise_exception=True):
                    assign_flat_service.save()
                    flat_obj = models.ServiceProvider.objects.get_by_filter(flat=data.get('flat'), is_active=True,
                                                                            private=True,
                                                                            is_delete=False)
                    ser_data = serializers.ServiceProviderDetailSerializer(flat_obj, many=True)

                    # private_service_provider_list = serializers.ServiceProviderDetailSerializer(flat_obj,
                    #                                                                             many=True)
                    log_data.info(f'Added New service provider')
                    return message.success_message(message='Added New service provider successfully.',
                                                   data=ser_data.data)
            return message.execution_issue(message='cant add public service_provider')
        log_data.error(f'{serializer.errors}')
        return message.invalid_serializer(serializer)

    # update the service providers details
    def update_service_provider(self, user, data, service_id):
        """
        :param user:
        :param data:
        :param service_id:
        :return: updated service provider list
        """
        if not user:
            log_data.error(f'user does not exist')
            return message.invalid_user()
        serializer = serializers.UpdateServiceProviderSerializer(data=data, partial=True)
        if serializer.is_valid(raise_exception=True):
            service_obj = serializer.update(
                instance=models.ServiceProvider.objects.get(pk=service_id, is_delete=False, is_active=True,
                                                            private=True),
                validated_data=serializer.validated_data)
            service_obj.modified_by = user
            service_obj.save()
            if service_obj.private is True:
                if data.get('flat'):
                    if models.FlatServiceProvider.objects.get_by_filter(service_provider=service_obj).exists():
                        models.FlatServiceProvider.objects.get_by_filter(service_provider=service_obj).delete()
                        flat_instance = {'flat': data.get('flat'), 'service_provider': service_obj.id}
                        assign_flat_service = serializers.AssignFlatServiceSerializer(data=flat_instance)
                        if assign_flat_service.is_valid(raise_exception=True):
                            assign_flat_service.save()
                        flat_obj = models.ServiceProvider.objects.get_by_filter(flat=data.get('flat'),
                                                                                is_active=True,
                                                                                private=True,
                                                                                is_delete=False)
                        ser_data = serializers.ServiceProviderDetailSerializer(flat_obj, many=True)
                        # ser_data = serializers.ServiceProviderDetailSerializer(service_obj, many=False)
                        log_data.info(f'Updated the service provider')
                        return message.success_message(message='Update the service provider successfully.',
                                                       data=ser_data.data)
                    return message.execution_issue(message='service does not exists')
                return message.execution_issue(message='invalid flat')
            return message.execution_issue(message='cant update public service_provider')
        log_data.error(f'{serializer.errors}')
        return message.invalid_serializer(serializer)

    # delete the service providers from the list
    def delete_service_provider(self, service_id, flat_id):
        """
        Api to delete the service provider from the list
        :param service_id:
        :return:
        """
        if models.ServiceProvider.objects.filter(id=service_id, flat=flat_id, is_delete=False).exists():
            service_provider = models.ServiceProvider.objects.get(id=service_id)
            service_provider.is_delete = True
            service_provider.is_active = False
            service_provider.save()
            service_provider_list = models.ServiceProvider.objects.filter(flat=flat_id, is_active=True, private=True,
                                                                          is_delete=False)
            ser_service_provider_list = serializers.ServiceProviderDetailSerializer(service_provider_list, many=True)
            return message.success_message(message='Deleted the service provider successfully.',
                                           data=ser_service_provider_list.data)
        else:
            log_data.error(f'the service provider does not exist')
            return message.no_data_found()

    # view the public service provider details
    def view_public_service_provider(self):
        service_provider_list = models.ServiceProvider.objects.filter(is_active=True, private=False, is_delete=False)
        ser_service_provider_list = serializers.ServiceProviderDetailSerializer(service_provider_list, many=True)
        log_data.info(f'Fetch the public service provider list')
        return message.success_message(message='Fetch the public service provider list.',
                                       data=ser_service_provider_list.data)

    # view the private service providers list
    def view_private_service_provider(self, flat_id):
        """
        :return: list of private service providers
        """
        flat_obj = models.ServiceProvider.objects.get_by_filter(flat=flat_id, is_active=True, private=True,
                                                                is_delete=False)

        private_service_provider_list = serializers.ServiceProviderDetailSerializer(flat_obj,
                                                                                    many=True)
        log_data.info(f'Fetch the private service provider list')
        return message.success_message(message='Fetch the private service provider list.',
                                       data=private_service_provider_list.data)

    # view the list of service providers by category
    def view_provider_by_category(self, data, category_id):
        """
        Api to view the service providers according to category
        :param data:
        :param category_id:
        :return:
        """
        service_list = get_service_list_by_category(filter_data=data, category_id=category_id)
        if data.get('screen'):
            if data.get('screen') == 'SERVICE':
                service_list = service_list[:4]
        ser_service_provider_list = serializers.ServiceProviderDetailSerializer(service_list, many=True)
        log_data.info(f'Fetch the public service provider list by category')
        return message.success_message(message='Fetch the public service provider list by category.',
                                       data=ser_service_provider_list.data)

    # view the list of private service providers by category
    def view_private_provider_by_category(self, category_id, flat_id):
        """
        Api to view the service providers according to category
        :param flat_id:
        :param category_id:
        :return:
        """
        provider_category = models.ServiceProvider.objects.get_by_filter(flat=flat_id, service_category_id=category_id,
                                                                         is_delete=False, private=True,
                                                                         is_active=True)

        ser_service_provider_list = serializers.ServiceProviderDetailSerializer(provider_category, many=True)
        log_data.info(f'Fetch the private service provider list by category')
        return message.success_message(message='Fetch the private service provider list by category.',
                                       data=ser_service_provider_list.data)

    # list the public service according to the data and search param
    def list_service(self, data):
        """
        :param data:
        :return: list of services
        """
        service_list = get_service_list(filter_data=data)
        if data.get('screen'):
            if data.get('screen') == 'SERVICE':
                service_list = service_list[:4]
        ser_service_provider_list = serializers.ServiceProviderDetailSerializer(service_list, many=True)
        log_data.info(f'service list fetched successfully')
        return message.success_message(message='Fetch the public service provider list successfully',
                                       data=ser_service_provider_list.data)

    # display most viewed category list
    def mostly_viewed_category(self):

        service_list = models.ServiceCategory.objects.values_list('id', 'label', 'icon').annotate(
            category_count=Count('label')).order_by('-category_count')

        return message.success_message(message='Fetch the public service categories successfully',
                                       data=service_list.distinct())

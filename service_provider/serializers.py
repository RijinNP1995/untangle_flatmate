from rest_framework import serializers

from flat_api import models as flat_models
from flat_api import serializers as flat_serializers
from master import models as master_models
from master import serializers as master_serializers
from . import models


class AddServiceProviderSerializer(serializers.ModelSerializer):
    """
    Serializer class for adding new service provider
    """

    class Meta:
        model = models.ServiceProvider
        fields = (
            'contact_name',
            'contact_no',
            'private',
            'service_category',
            'flat'
        )


class AssignFlatServiceSerializer(serializers.ModelSerializer):
    """
    Serializer class for assign service provider to a flat
    """

    class Meta:
        model = models.FlatServiceProvider
        fields = (
            'service_provider',
            'flat'
        )


class ServiceProviderDetailSerializer(serializers.ModelSerializer):
    """
    serializer class for passing service provider details
    """

    flat = serializers.SerializerMethodField()
    service_category = serializers.SerializerMethodField()

    class Meta:
        model = models.ServiceProvider
        fields = (
            'id',
            'contact_name',
            'contact_no',
            'private',
            'service_category',
            'flat',
            'created_by',
            'created_on',
            'modified_by',
            'modified_on',
        )

    def get_flat(self, obj):
        if obj.private is True:
            flat_service = models.FlatServiceProvider.objects.get(service_provider=obj.id)
            flat_obj = flat_models.Flat.objects.get(pk=flat_service.flat.id)
            flat_obj_ser = flat_serializers.FlatDetailsSerializer(flat_obj, many=False)
            return flat_obj_ser.data
        return None

    def get_service_category(self, obj):
        service_category = master_models.ServiceCategory.objects.get(pk=obj.service_category.pk)
        ser_service_category = master_serializers.ServiceCategoryListSerializer(service_category, many=False)
        return ser_service_category.data


class UpdateServiceProviderSerializer(serializers.ModelSerializer):
    """
    Serializer class for updating the service provider
    """

    class Meta:
        model = models.ServiceProvider
        fields = (
            'contact_name',
            'contact_no',
            'private',
            'service_category',
            'flat'
        )


class ServiceCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ServiceCategory
        fields = (
            'label',
            'icon'
        )

from django.contrib import admin
from . import models


@admin.register(models.ServiceProvider)
class ServiceProviderAdmin(admin.ModelAdmin):
    list_display = ('contact_name', 'contact_no')


@admin.register(models.FlatServiceProvider)
class ServiceProviderAdmin(admin.ModelAdmin):
    list_display = ('flat', 'service_provider')

from django.db import models
from django.utils.translation import ugettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField

from flat_api.models import Flat
from master.models import BaseModel, ServiceCategory
from .managers import ServiceProviderManager, FlatServiceProviderManager


class ServiceProvider(BaseModel):
    contact_name = models.CharField(_('Contact Name'), max_length=160, blank=True, null=True)
    contact_no = PhoneNumberField(_('Contact Number'), blank=True, null=True, unique=False)
    service_category = models.ForeignKey(ServiceCategory, on_delete=models.CASCADE)
    flat = models.ForeignKey(Flat, on_delete=models.CASCADE)
    private = models.BooleanField(_('Private'), default=False)
    lat = models.DecimalField(decimal_places=17, max_digits=20, null=True, blank=True)
    lon = models.DecimalField(decimal_places=17, max_digits=20, null=True, blank=True)
    objects = ServiceProviderManager()

    def __str__(self):
        return self.contact_name.__str__()


class FlatServiceProvider(BaseModel):
    service_provider = models.ForeignKey(ServiceProvider, on_delete=models.CASCADE)
    flat = models.ForeignKey(Flat, on_delete=models.CASCADE)
    objects = FlatServiceProviderManager()

    def __str__(self):
        return self.service_provider.__str__()

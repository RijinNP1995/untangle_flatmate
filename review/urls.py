from django.urls import path

from . import views

urlpatterns = [
    path('rate-tenant/', views.RateTenant.as_view(), name='create_tenant_ratings'),
    path('tenant-rating/<int:tenant_id>/', views.RateTenant.as_view(), name='get_tenant_ratings'),
    path('edit-tenant-rating/<int:pk>/', views.RateTenant.as_view(), name='update_tenant_ratings'),

    path('rate-flat/', views.RateFlat.as_view(), name='create_flat_ratings'),
    path('flat-rating/<int:flat_id>/', views.RateFlat.as_view(), name='get_flat_ratings'),
    path('edit-flat-rating/<int:pk>/', views.RateFlat.as_view(), name='update_Flat_ratings'),
]

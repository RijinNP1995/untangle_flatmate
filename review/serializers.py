from rest_framework import serializers

from . import models
from master import serializers as master_serializers
from user import models as user_models
from user import serializers as user_serializers


class AddTenantRatingSerializer(serializers.ModelSerializer):
    """
    Serializer class for tenant ratings
    """

    class Meta:
        model = models.TenantRating
        fields = '__all__'


class UpdateTenantRatingSerializer(serializers.ModelSerializer):
    """
    Serializer class for tenant ratings
    """
    flat = serializers.IntegerField()
    tenant = serializers.IntegerField()
    rating = serializers.FloatField()
    remark = serializers.CharField()

    class Meta:
        model = models.TenantRating
        fields = ('flat',
                  'tenant',
                  'rating',
                  'remark')

    def update(self, instance, validated_data):
        instance.flat = validated_data.get('flat')
        instance.tenant = validated_data.get('tenant')
        instance.rating = validated_data.get('rating')
        instance.remark = validated_data.get('remark')

        instance.save()

        return instance


class AddFlatRatingSerializer(serializers.ModelSerializer):
    """
    Serializer class for tenant ratings
    """

    class Meta:
        model = models.FlatRating
        fields = '__all__'


class UpdateFlatRatingSerializer(serializers.ModelSerializer):
    """
    Serializer class for tenant ratings
    """
    flat = serializers.IntegerField()
    rating = serializers.FloatField()
    remark = serializers.CharField()

    class Meta:
        model = models.FlatRating
        fields = ('flat',
                  'rating',
                  'remark')

    def update(self, instance, validated_data):
        instance.flat = validated_data.get('flat')
        instance.rating = validated_data.get('rating')
        instance.remark = validated_data.get('remark')

        instance.save()

        return instance

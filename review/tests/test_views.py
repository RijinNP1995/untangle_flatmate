import json

from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from flat_api.models import Flat
from user import models as user_model


class TestTenantReview(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = user_model.User(first_name='first_name', last_name='last_name', display_name='test',
                                    profession='profession', company_name='company_name', age=20,
                                    locality='locality', phone='9048893681', description='description',
                                    email='test@email.com',
                                    date_joined='date_joined', is_email_verified=False,
                                    is_phone_verified=False)
        self.user.save()
        self.token1 = Token.objects.get_or_create(user=self.user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token1[0].key)
        self.rating = 3
        self.remark = 'good'
        self.tenant = self.user
        self.flat = Flat.objects.create(flat_name='flat_name', flat_no='flat_no', landmark='landmark',
                                        rent_per_month=1500.50, num_of_beds=1,
                                        flat_description='flat_description',
                                        flatmate_description='flatmate_description',
                                        flatmate_min_age=20, flatmate_max_age=30,
                                        expected_gender='male')

        self.valid_payload = {
            "flat": 1,
            "tenant": 1,
            "rating": 5,
            "remark": "good person and a honest human being."
        }

        self.flat_rating_valid_payload = {
            "flat": 1,
            "rating": 5,
            "remark": "good atmosphere and friendly owner."
        }

    def test_create_tenant_rating(self):
        response = self.client.post(
            reverse('create_tenant_ratings'),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_flat_rating(self):
        response = self.client.post(
            reverse('create_flat_ratings'),
            data=json.dumps(self.flat_rating_valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

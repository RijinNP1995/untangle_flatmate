from django.test import TestCase

from flat_api.models import Flat
from master.models import TaskStatus
from task import models as task_model
from user import models as user_model
from review import models as review_model


class ModelTestCase(TestCase):
    """This class defines the test suite for the tenant's rating model."""

    def setUp(self):
        """Define the test client and other test variables."""
        self.rating = 5.0
        self.remark = 'good and nice guy'
        self.gender = user_model.Gender.objects.create(type='male')
        self.tenant = user_model.User.objects.create(first_name='first_name_1', last_name='last_name',
                                                         display_name='test_1',
                                                         gender=self.gender,
                                                         profession='profession', company_name='company_name', age=20,
                                                         locality='locality', phone='9048893687',
                                                         description='description',
                                                         email='test1@email.com',
                                                         date_joined='date_joined', is_email_verified=False,
                                                         is_phone_verified=False)
        self.created_by = user_model.User.objects.create(first_name='first_name', last_name='last_name',
                                                         display_name='test',
                                                         gender=self.gender,
                                                         profession='profession', company_name='company_name', age=20,
                                                         locality='locality', phone='9048893681',
                                                         description='description',
                                                         email='test2@email.com',
                                                         date_joined='date_joined', is_email_verified=False,
                                                         is_phone_verified=False)
        self.flat = Flat.objects.create(flat_name='flat_name', flat_no='flat_no', landmark='landmark',
                                        rent_per_month=1500, num_of_beds=1,
                                        flat_description='flat_description',
                                        flatmate_description='flatmate_description',
                                        flatmate_min_age=20, flatmate_max_age=30,
                                        expected_gender='male')
        self.rating_obj = review_model.TenantRating(rating=self.rating, remark=self.remark,
                                    flat=self.flat, created_by=self.created_by, tenant=self.tenant)
        self.flat_rating_obj = review_model.FlatRating(rating=self.rating, remark=self.remark,
                                                    flat=self.flat, created_by=self.created_by)

    def test_model_create_tenant_rating(self):
        old_count = review_model.TenantRating.objects.count()
        self.rating_obj.save()
        new_count = review_model.TenantRating.objects.count()
        self.assertNotEqual(old_count, new_count)

    def test_model_create_flat_rating(self):
        old_count = review_model.FlatRating.objects.count()
        self.flat_rating_obj.save()
        new_count = review_model.FlatRating.objects.count()
        self.assertNotEqual(old_count, new_count)

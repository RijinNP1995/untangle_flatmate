from drf_yasg.utils import swagger_auto_schema
from rest_framework.views import APIView

from core import log_data
from flatmates_backend import responses
from . import serializers
from .services import ReviewManagementServices

review_service = ReviewManagementServices()


class RateTenant(APIView):
    """
    For rate and review the tenant
    """
    @swagger_auto_schema(request_body=serializers.AddTenantRatingSerializer)
    def post(self, request):
        """
        create a tenant review and rating object
        :param request: request details like data
        :return: data and status
        """
        try:
            return review_service.create_tenant_ratings(user=request.user,
                                                        data=request.data,
                                                        files=request.FILES)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)

    def get(self, request, tenant_id):
        """
        get a tenant's complete review and avg rating
        :param request: request
        :param tenant_id: tenant's id
        :return: avg rating and complete review
        """
        try:
            return review_service.get_tenant_rating(tenant_id=tenant_id)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)

    @swagger_auto_schema(request_body=serializers.UpdateTenantRatingSerializer)
    def patch(self, request, pk):
        """
        update flat ratings
        :param request:
        :param pk:
        :return:
        """
        try:
            return review_service.update_tenant_rating(user=request.user,
                                                       data=request.data,
                                                       id=pk,
                                                       files=request.FILES)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)


class RateFlat(APIView):
    """
    For rate and review the flat
    """

    @swagger_auto_schema(request_body=serializers.AddFlatRatingSerializer)
    def post(self, request):
        """
        create a flat review and rating object
        :param request: request details like data
        :return: data and status
        """
        try:
            return review_service.create_flat_ratings(user=request.user,
                                                      data=request.data,
                                                      files=request.FILES)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)

    def get(self, request, flat_id):
        """
        get a flat's complete review and avg rating
        :param request: request
        :param tenant_id: tenant's id
        :return: avg rating and complete review
        """
        try:
            return review_service.get_flat_rating(flat_id=flat_id)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)

    @swagger_auto_schema(request_body=serializers.UpdateFlatRatingSerializer)
    def patch(self, request, pk):
        """
        update flat ratings
        :param request:
        :param pk:
        :return:
        """
        try:
            return review_service.update_flat_rating(user=request.user,
                                                     data=request.data,
                                                     id=pk,
                                                     files=request.FILES)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)

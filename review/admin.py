from django.contrib import admin
from . import models

# Register your models here.
admin.site.register(models.TenantRating)
admin.site.register(models.FlatRating)

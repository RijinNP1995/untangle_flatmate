from django.db import models
from master.models import BaseModel
from django.utils.translation import ugettext_lazy as _
from flat_api.models import Flat
from user.models import User
from .managers import TenantRatingManager, TenantRemarkFileManager, FlatRatingManager, FlatRemarkFileManager
from django.core.validators import MaxValueValidator, MinValueValidator


class TenantRating(BaseModel):
    flat = models.ForeignKey(Flat, on_delete=models.CASCADE)
    tenant = models.ForeignKey(User, on_delete=models.CASCADE)
    rating = models.FloatField(_('rating'), default=0,
                               validators=[MinValueValidator(0), MaxValueValidator(5)])
    remark = models.TextField(_('remark'), max_length=1600, blank=True, null=True)
    objects = TenantRatingManager()

    def __str__(self):
        return self.flat.__str__() + "-" + self.tenant.__str__()


class TenantRemarkFile(models.Model):
    file = models.ImageField()
    tenant_rating = models.ForeignKey(TenantRating, on_delete=models.CASCADE)
    objects = TenantRemarkFileManager()

    def __str__(self):
        return self.tenant_rating.__str__() + "-" + self.file.__str__()


class FlatRating(BaseModel):
    flat = models.ForeignKey(Flat, on_delete=models.CASCADE)
    rating = models.FloatField(_('rating'), default=0,
                               validators=[MinValueValidator(0), MaxValueValidator(5)])
    remark = models.TextField(_('remark'), max_length=1600, blank=True, null=True)
    objects = FlatRatingManager()

    def __str__(self):
        return self.flat.__str__() + "-" + self.rating.__str__()


class FlatRemarkFile(models.Model):
    file = models.ImageField()
    flat_rating = models.ForeignKey(FlatRating, on_delete=models.CASCADE)
    objects = FlatRemarkFileManager()

    def __str__(self):
        return self.flat_rating.__str__() + "-" + self.file.__str__()

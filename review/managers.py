from flatmates_backend.base_manager import BaseManager


# manager for tenant rating model
class TenantRatingManager(BaseManager):
    pass


class TenantRemarkFileManager(BaseManager):
    #  to filter the files of the tenant remarks
    def get_files(self, **filter_args):
        return self.get_all().filter(**filter_args)


# manager for tenant rating model
class FlatRatingManager(BaseManager):
    pass


class FlatRemarkFileManager(BaseManager):
    #  to filter the files of the tenant remarks
    def get_files(self, **filter_args):
        return self.get_all().filter(**filter_args)

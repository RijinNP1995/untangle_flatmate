import pyrebase
from django.db.models import Q
from django.db.models.expressions import RawSQL
from pusher_push_notifications import PushNotifications

from core import log_data
from flat_api import models as flat_model
from . import serializers
from flatmates_backend import responses
from flatmates_backend import responses as message
from user import models as user_model
from . import models
from django.db.models import Sum


class ReviewManagementServices:
    def create_tenant_ratings(self, user, data, files):
        """
        create a tenat's rating object
        :param user: dweller
        :param data: data to store
        :param files: images (optional)
        :return: status and data
        """
        if user:
            serializer = serializers.AddTenantRatingSerializer(data=data)
            if serializer.is_valid():
                review_obj = serializer.save(created_by=user)
                return message.create_object(model_info='Rating',
                                             data=serializers.AddTenantRatingSerializer(review_obj).data)
            log_data.error(f'{serializer.errors} invalid data')
            return message.invalid_data(serializer)
        log_data.error(f'user does not exist')
        return message.invalid_user()

    def get_tenant_rating(self, tenant_id):
        """
        tenant's rating
        :param tenant_id: tenant id
        :return: avg rating and list of reviews
        """
        if user_model.User.objects.pk_does_exist(id=tenant_id):
            review_data = models.TenantRating.objects.filter(tenant=tenant_id)
            total_rating = review_data.aggregate(Sum('rating'))
            avg_rating = round(total_rating['rating__sum']/review_data.count(), 1)
            review_data_ser = serializers.AddTenantRatingSerializer(review_data, many=True)
            log_data.info(f'Tenant review details fetched successfully')
            return responses.ratings_data_response(model_info='Tenant review', data=review_data_ser.data,
                                                   avg_rating=avg_rating)
        log_data.error(f'User does not exist')
        return message.id_does_not_exist()

    def update_tenant_rating(self, user, data, id, files):
        if user:
            if not models.TenantRating.objects.pk_does_exist(id=id):
                serializer = serializers.UpdateTenantRatingSerializer(data=data)
                if serializer.is_valid():
                    review_obj = serializer.update(instance=models.TenantRating.objects.get_by_id(pk=id),
                                                 validated_data=serializer.validated_data)
                    review_obj.modified_by = user
                    review_obj.save()

        log_data.error(f'user does not exist')
        return message.invalid_user()

    def create_flat_ratings(self, user, data, files):
        """
        create a flat's rating object
        :param user: tenant
        :param data: data to store
        :param files: images (optional)
        :return: status and data
        """
        if user:
            serializer = serializers.AddFlatRatingSerializer(data=data)
            if serializer.is_valid():
                review_obj = serializer.save(created_by=user)
                return message.create_object(model_info='Rating',
                                             data=serializers.AddFlatRatingSerializer(review_obj).data)
            log_data.error(f'{serializer.errors} invalid data')
            return message.invalid_data(serializer)
        log_data.error(f'user does not exist')
        return message.invalid_user()

    def get_flat_rating(self, flat_id):
        """
        flat's rating
        :param flat_id: flat id
        :return: avg rating and list of reviews
        """
        if flat_model.Flat.objects.pk_does_exist(id=flat_id):
            review_data = models.FlatRating.objects.filter(flat=flat_id)
            total_rating = review_data.aggregate(Sum('rating'))
            avg_rating = round(total_rating['rating__sum']/review_data.count(), 1)
            review_data_ser = serializers.AddFlatRatingSerializer(review_data, many=True)
            log_data.info(f'Flat review details fetched successfully')
            return responses.ratings_data_response(model_info='Flat review', data=review_data_ser.data,
                                                   avg_rating=avg_rating)
        log_data.error(f'User does not exist')
        return message.id_does_not_exist()

    def update_flat_rating(self, user, data, id, files):
        if user:
            if not models.FlatRating.objects.pk_does_exist(id=id):
                serializer = serializers.UpdateFlatRatingSerializer(data=data)
                if serializer.is_valid():
                    review_obj = serializer.update(instance=models.FlatRating.objects.get_by_id(pk=id),
                                                   validated_data=serializer.validated_data)
                    review_obj.modified_by = user
                    review_obj.save()

        log_data.error(f'user does not exist')
        return message.invalid_user()

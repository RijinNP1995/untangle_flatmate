from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.views import APIView

from core import log_data
from flatmates_backend import responses as message
from task.services import TaskManagementServices

task_management_service = TaskManagementServices()


class AddNewTask(APIView):

    def post(self, request):
        """
        API to add new task
        :param request:
        :return:
        """
        try:

            return task_management_service.add_new_task_service(user=request.user, data=request.data)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            return message.exception_response(e)

    def get(self, request):
        """
        API to view the task
        :param request:
        :return:
        """
        try:

            user = request.user
            flat_id = request.GET.get('flat_id')
            return task_management_service.view_task_service(user=user, flat_id=flat_id,
                                                             from_date=request.GET.get('from_date'),
                                                             to_date=request.GET.get('to_date'))
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            return message.exception_response(e)

    def patch(self, request, pk):
        """
        API to update the task details
        :param pk:
        :param request:
        :return:
        """
        try:
            return task_management_service.update_task_service(data=request.data, id=pk, user=request.user)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            return message.exception_response(e)

    @swagger_auto_schema(
        operation_id='Delete Participant',
        request_body=openapi.Schema(type=openapi.TYPE_OBJECT,
                                    properties={
                                        'id': openapi.Schema(
                                            type=openapi.TYPE_INTEGER,
                                        ),
                                        'flat_id': openapi.Schema(
                                            type=openapi.TYPE_INTEGER,
                                        )
                                    }
                                    ),
    )
    def delete(self, request):
        """
        API to delete the task
        :param request:
        :return:
        """
        try:
            id = request.data.get('id')
            flat_id = request.data.get('flat_id')
            log_data.warning(f'enter the valid task id to delete the particular task')
            return task_management_service.delete_task(id=id, flat_id=flat_id)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            return message.exception_response(e)


class TaskParticipant(APIView):
    def post(self, request):
        """
        API to delete the task participant
        :param request:
        :return:
        """
        try:
            task_id = request.data.get('task_id')
            assigned_to = request.data.get('assigned_to')
            log_data.warning('enter the valid task_id and assigned_to')
            return task_management_service.delete_participant(task_id=task_id, assigned_to=assigned_to)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            return message.exception_response(e)


class ViewTask(APIView):
    def get(self, request):
        """
        API to view the task
        :param request:
        :return:
        """
        try:

            task_id = request.GET.get('task_id')
            return task_management_service.view_task_by_task_id(task_id=task_id)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            return message.exception_response(e)

    @swagger_auto_schema(
        operation_id='Update the task status',
        request_body=openapi.Schema(type=openapi.TYPE_OBJECT,
                                    properties={
                                        'task_id': openapi.Schema(
                                            type=openapi.TYPE_INTEGER,
                                        ),
                                        'task_status': openapi.Schema(
                                            type=openapi.TYPE_INTEGER,
                                        )
                                    }
                                    ),
    )
    def patch(self, request):
        """
        :param request:
        :return: API to update the task status
        """
        try:
            task_id = request.data.get('task_id')
            task_status = request.data.get('task_status')
            return task_management_service.update_task_status(task_id=task_id,
                                                              task_status=task_status)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            return message.exception_response(e)

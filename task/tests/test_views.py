import json

from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from flat_api.models import Flat
from master.models import TaskStatus, NotificationIcon
from task import models as task_model
from user import models as user_model


class TestAddTask(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = user_model.User(first_name='first_name', last_name='last_name', display_name='test',
                                    profession='profession', company_name='company_name', age=20,
                                    locality='locality', phone='9048893681', description='description',
                                    email='test@email.com',
                                    date_joined='date_joined', is_email_verified=False,
                                    is_phone_verified=False)
        self.user.save()
        self.token1 = Token.objects.get_or_create(user=self.user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token1[0].key)
        self.task_name = 'Task_name'
        self.task_description = 'Task_description'
        self.due_date = '2020-03-28'
        self.due_time = '12:12:12'
        self.assigned_to = self.user
        self.task_status = TaskStatus.objects.create(task_status='Completed')
        self.task_icon = task_model.TaskIcon.objects.create(icon='facebook.png')
        self.notification = NotificationIcon.objects.create(label='FLAT', icon='facebook.png')
        self.flat = Flat.objects.create(flat_name='flat_name', flat_no='flat_no', landmark='landmark',
                                        rent_per_month=1500.50, num_of_beds=1,
                                        flat_description='flat_description',
                                        flatmate_description='flatmate_description',
                                        flatmate_min_age=20, flatmate_max_age=30,
                                        expected_gender='male')

        self.valid_payload = {
            "task_name": "Washing",
            "task_description": "test description",
            "due_date": "2019-04-28",
            "due_time": "12:12:12",
            "assigned_to": [1],
            "task_status": 1,
            "task_icon": 1,
            "flat": 1
        }

        self.invalid_payload_1 = {
            "task_name": "Washing",
            "task_description": "test description",
            "due_date": "2020-04-28",
            "due_time": "12:12:12",
            "assigned_to": 10,
            "task_status": 1,
            "task_icon": 1,
            "flat": 1
        }

        self.invalid_payload2 = {
            "task_name": "Washing",
            "task_description": "test description",
            "due_date": "2019-04-28",
            "due_time": "12:12:12",
            "assigned_to": [1],
            "task_status": 20,
            "task_icon": 1,
            "flat": 1
        }
        self.invalid_payload3 = {
            "task_name": "Washing",
            "task_description": "test description",
            "due_date": "2019-04-28",
            "due_time": "12:12:12",
            "assigned_to": [1],
            "task_status": 1,
            "task_icon": 30,
            "flat": 1
        }
        self.invalid_payload4 = {
            "task_name": "Washing",
            "task_description": "test description",
            "due_date": "2019-04-28",
            "due_time": "12:12:12",
            "assigned_to": [1],
            "task_status": 1,
            "task_icon": 1,
            "flat": 40
        }

    def test_add_task(self):
        """
        :return: test case with valid payload to add the task
        """
        response = self.client.post(
            reverse('add_task'),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['message'], 'New task created successfully')

    def test_add_task_invalid_1(self):
        """
        :return: test case with invalid assigned_to id
        """
        response = self.client.post(
            reverse('add_task'),
            data=json.dumps(self.invalid_payload_1),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['error'], '\'int\' object is not iterable')

    def test_add_task_invalid_2(self):
        """
        :return: test case with invalid task_status
        """
        response = self.client.post(
            reverse('add_task'),
            data=json.dumps(self.invalid_payload2),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['error'],
                         '{\'task_status\': [ErrorDetail(string=\'Invalid pk "20" - object does not exist.\', code=\'does_not_exist\')]}')

    def test_add_task_invalid_3(self):
        """
        :return: test case with invalid task_icon
        """
        response = self.client.post(
            reverse('add_task'),
            data=json.dumps(self.invalid_payload3),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['error'],
                         '{\'task_icons\': [ErrorDetail(string=\'Invalid pk "30" - object does not exist.\', code=\'does_not_exist\')]}')

    def test_add_task_invalid_4(self):
        """
        :return: test case with invalid flat id
        """
        response = self.client.post(
            reverse('add_task'),
            data=json.dumps(self.invalid_payload4),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['error'],
                         '{\'flat\': [ErrorDetail(string=\'Invalid pk "40" - object does not exist.\', code=\'does_not_exist\')]}')


class DeleteTask(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = user_model.User.objects.create(first_name='first_name', last_name='last_name', display_name='test',
                                                   profession='profession', company_name='company_name', age=20,
                                                   locality='locality', phone='9048893681', description='description',
                                                   email='test@email.com',
                                                   date_joined='date_joined', is_email_verified=False,
                                                   is_phone_verified=False)
        self.user.save()
        self.token1 = Token.objects.get_or_create(user=self.user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token1[0].key)
        self.task_name = 'Task_name'
        self.task_description = 'Task_description'
        self.due_date = '2020-03-28'
        self.due_time = '12:12:12'
        self.assigned_to = self.user
        self.task_status = TaskStatus.objects.create(task_status='Completed')
        self.task_icon = task_model.TaskIcon.objects.create(icon='facebook.png')
        self.flat = Flat.objects.create(flat_name='flat_name', flat_no='flat_no', landmark='landmark',
                                        rent_per_month=1500.50, num_of_beds=1,
                                        flat_description='flat_description',
                                        flatmate_description='flatmate_description',
                                        flatmate_min_age=20, flatmate_max_age=30,
                                        expected_gender='male')
        self.flat = task_model.Task.objects.create(task_name=self.task_name, task_description=self.task_description,
                                                   due_date=self.due_date,
                                                   due_time=self.due_time,
                                                   flat=self.flat,
                                                   created_by=self.user)
        self.valid_payload = {
            "id": 1,
            "flat_id": 1
        }
        self.invalid_payload = {
            "id": 10,
            "flat_id": 10
        }

    def test_delete_task(self):
        response = self.client.delete(
            reverse('delete_task'),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['message'], 'Task details deleted successfully')

    def test_invalid_delete_task(self):
        response = self.client.delete(
            reverse('delete_task'),
            data=json.dumps(self.invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(response.data['error'], 'no data found')


class TestDeleteParticipant(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = user_model.User.objects.create(first_name='first_name', last_name='last_name', display_name='test',
                                                   profession='profession', company_name='company_name', age=20,
                                                   locality='locality', phone='9048893681', description='description',
                                                   email='test@email.com',
                                                   date_joined='date_joined', is_email_verified=False,
                                                   is_phone_verified=False)
        self.user.save()
        self.token1 = Token.objects.get_or_create(user=self.user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token1[0].key)
        self.task_name = 'Task_name'
        self.task_description = 'Task_description'
        self.due_date = '2020-03-28'
        self.due_time = '12:12:12'
        self.assign_to = user_model.User.objects.create(first_name='first_name', last_name='last_name',
                                                        display_name='test',
                                                        profession='profession', company_name='company_name', age=20,
                                                        locality='locality', email='rijin@gmail.com',
                                                        description='description',
                                                        date_joined='date_joined', is_email_verified=False,
                                                        is_phone_verified=False)
        self.task_status = TaskStatus.objects.create(task_status='Completed')
        self.task_icon = task_model.TaskIcon.objects.create(icon='facebook.png')
        self.flat = Flat.objects.create(flat_name='flat_name', flat_no='flat_no', landmark='landmark',
                                        rent_per_month=1500.50, num_of_beds=1,
                                        flat_description='flat_description',
                                        flatmate_description='flatmate_description',
                                        flatmate_min_age=20, flatmate_max_age=30,
                                        expected_gender='male')
        self.task = task_model.Task.objects.create(task_name=self.task_name, task_description=self.task_description,
                                                   due_date=self.due_date,
                                                   due_time=self.due_time,
                                                   flat=self.flat,
                                                   created_by=self.user)
        self.assign_task = task_model.AssignTask.objects.create(assigned_to=self.assign_to, task=self.task)
        self.valid_payload = {
            "task_id": 1,
            "assigned_to": 2
        }
        self.invalid_task_payload = {
            "task_id": 10,
            "assigned_to": 1
        }
        self.invalid_payload_assign_to = {
            "task_id": 1,
            "assigned_to": 10
        }

    def test_delete_participant(self):
        response = self.client.post(
            reverse('delete_participant'),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['message'], 'Participant removed from the task')

    def test_invalid_delete_participant1(self):
        response = self.client.post(
            reverse('delete_participant'),
            data=json.dumps(self.invalid_task_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(response.data['error'], 'no data found')

    def test_invalid_delete_participant2(self):
        response = self.client.post(
            reverse('delete_participant'),
            data=json.dumps(self.invalid_payload_assign_to),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(response.data['error'], 'no data found')

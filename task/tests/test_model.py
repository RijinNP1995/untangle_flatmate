from django.test import TestCase

from flat_api.models import Flat
from master.models import TaskStatus
from task import models as task_model
from user import models as user_model


class ModelTestCase(TestCase):
    """This class defines the test suite for the user model."""

    def setUp(self):
        """Define the test client and other test variables."""
        self.task_name = 'Task_name'
        self.task_description = 'Task_description'
        self.due_date = '2020-03-28'
        self.due_time = '12:12:12'
        self.gender = user_model.Gender.objects.create(type='male')
        self.created_by = user_model.User.objects.create(first_name='first_name', last_name='last_name',
                                                         display_name='display_name',
                                                         gender=self.gender,
                                                         profession='profession', company_name='company_name', age=20,
                                                         locality='locality', phone='9048893681',
                                                         description='description',
                                                         email='email_testing@email.com',
                                                         date_joined='date_joined', is_email_verified=False,
                                                         is_phone_verified=False)
        self.task_status = TaskStatus.objects.create(label='test_label', task_status='Completed',
                                                     created_by=self.created_by)
        self.icon = task_model.TaskIcon.objects.create(label='icon_label', icon='facebook.png',
                                                       created_by=self.created_by)
        self.flat = Flat.objects.create(flat_name='flat_name', flat_no='flat_no', landmark='landmark',
                                        rent_per_month=1500.50, num_of_beds=1,
                                        flat_description='flat_description',
                                        flatmate_description='flatmate_description',
                                        flatmate_min_age=20, flatmate_max_age=30,
                                        expected_gender='male')
        self.task = task_model.Task(task_name=self.task_name, task_description=self.task_description,
                                    due_date=self.due_date,
                                    due_time=self.due_time,
                                    flat=self.flat, created_by=self.created_by)

    def test_model_add_task(self):
        old_count = task_model.Task.objects.count()
        self.task.save()
        new_count = task_model.Task.objects.count()
        self.assertNotEqual(old_count, new_count)

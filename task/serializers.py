from rest_framework import serializers

from core.utils import find_day_of_week
from master.serializers import TaskIconsSerializer
from task import models as model
from user.models import ProfilePhoto
from user import serializers as user_ser


class AssignTaskSerializer(serializers.ModelSerializer):
    display_name = serializers.SerializerMethodField()
    photo = serializers.SerializerMethodField()

    class Meta:
        model = model.AssignTask
        fields = (
            'assigned_to',
            'display_name',
            'photo',
            'task'
        )

    def get_display_name(self, obj):
        return obj.assigned_to.display_name

    def get_photo(self, obj):
        """
        :param obj:
        :return: serializer to get the photo
        """
        user_id = obj.assigned_to.id
        if model.ProfilePhoto.objects.filter(user_id=user_id).exists():
            profile_photo = ProfilePhoto.objects.get(user_id_id=user_id)
            photo_ser = user_ser.ProfilePhotoSerializer(profile_photo)
            return photo_ser.data
        else:
            model.User.objects.filter(id=user_id).exists()
            serializer = user_ser.PeopleSearchDataSerializer(user_id)
            return serializer.data


class AssignToSerializer(serializers.ModelSerializer):
    class Meta:
        model = model.AssignTask
        fields = (
            '__all__'
        )


class IconSerializer(serializers.ModelSerializer):
    class Meta:
        model = model.TaskIcons
        fields = (
            '__all__'
        )


class StatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = model.Status
        fields = (
            '__all__'
        )


class FlatTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = model.FlatTask
        fields = (
            '__all__'
        )


class TaskIconSerializer(serializers.ModelSerializer):
    icon_label = serializers.SerializerMethodField()
    icon_data = serializers.SerializerMethodField()

    class Meta:
        model = model.TaskIcons
        fields = (
            'id',
            'icon_label',
            'icon_data'
        )

    def get_icon_label(self, obj):
        return obj.task_icons.label

    def get_icon_data(self, obj):
        icon_obj = TaskIconsSerializer(obj.task_icons)
        return icon_obj.data['icon']


class TaskStatusSerializer(serializers.ModelSerializer):
    status_label = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()

    class Meta:
        model = model.Status
        fields = (
            'status_label',
            'status'
        )

    def get_status_label(self, obj):
        return obj.task_status.label

    def get_status(self, obj):
        return obj.task_status.task_status


# class TaskFlatSerializer(serializers.ModelSerializer):
#     flat_no = serializers.SerializerMethodField()
#     flat_name = serializers.SerializerMethodField()
#     landmark = serializers.SerializerMethodField()
#     city = serializers.SerializerMethodField()
#
#     class Meta:
#         model = model.FlatTask
#         fields = (
#             'flat_no',
#             'flat_name',
#             'landmark',
#             'city'
#         )
#
#     def get_flat_no(self, obj):
#         return obj.flat.flat_no
#
#     def get_flat_name(self, obj):
#         return obj.flat.flat_name
#
#     def get_landmark(self, obj):
#         return obj.flat.landmark
#
#     def get_city(self, obj):
#         return obj.flat.city


class TaskSerializer(serializers.ModelSerializer):
    """Serializer which serialize the Add task module"""

    assigned_to = serializers.SerializerMethodField()
    task_status = serializers.SerializerMethodField()
    task_icon = serializers.SerializerMethodField()

    class Meta:
        model = model.Task
        fields = (
            'id',
            'task_name',
            'task_description',
            'due_date',
            'due_time',
            'assigned_to',
            'task_status',
            'task_icon',
            'flat',
        )

    def get_assigned_to(self, obj):
        assign_to = model.AssignTask.objects.get_assign_to(task=obj.id)
        assign_to_ser = AssignTaskSerializer(assign_to, many=True)
        return assign_to_ser.data

    def get_task_status(self, obj):
        task_status = model.Status.objects.get(task=obj.id)
        task_status_ser = StatusSerializer(task_status, many=False)
        return task_status_ser.data

    def get_task_icon(self, obj):
        task_icon = model.TaskIcons.objects.get(task=obj.id)
        task_icon_ser = IconSerializer(task_icon, many=False)
        return task_icon_ser.data


class TaskUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = model.Task
        fields = (
            'task_name',
            'task_description',
            'due_date',
            'due_time'
        )


class ViewTaskSerializer(serializers.ModelSerializer):
    """Serializer which serialize the Add task module"""
    day_of_week = serializers.SerializerMethodField()  # Day of week from menu date
    assigned_to = serializers.SerializerMethodField()
    task_icon = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()

    class Meta:
        model = model.Task
        fields = (
            'id',
            'task_name',
            'task_description',
            'due_date',
            'day_of_week',
            'due_time',
            'assigned_to',
            'status',
            'task_icon',
        )

    def get_day_of_week(self, obj):
        """
        :param obj: menu instance
        :return: day of menu date
        """
        return find_day_of_week(obj.due_date)

    def get_assigned_to(self, obj):
        assign_to = model.AssignTask.objects.get_assign_to(task=obj.id)
        assign_to_ser = AssignTaskSerializer(assign_to, many=True)
        return assign_to_ser.data

    def get_status(self, obj):
        task_status = model.Status.objects.get(task=obj.id)
        task_status_ser = TaskStatusSerializer(task_status, many=False)
        return task_status_ser.data

    def get_task_icon(self, obj):
        task_icon = model.TaskIcons.objects.get(task=obj.id)
        task_icon_ser = TaskIconSerializer(task_icon, many=False)
        return task_icon_ser.data

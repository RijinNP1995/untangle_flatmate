from django.db import models

from flat_api.models import Flat
from master.models import BaseModel, TaskStatus, TaskIcon, TaskBadge
from task.managers import AssignManager, TaskManager
from user.models import User, ProfilePhoto


class Task(BaseModel):
    task_name = models.CharField('task_name', max_length=20)
    task_description = models.TextField('task_description', blank=True, null=True)
    due_date = models.DateField('due_date')
    due_time = models.TimeField('due_time')
    flat = models.ForeignKey(Flat, on_delete=models.CASCADE)
    objects = TaskManager()

    def __str__(self):
        return f"{self.task_name} :: {self.flat}"


class AssignTask(models.Model):
    assigned_to = models.ForeignKey(User, on_delete=models.CASCADE)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    objects = AssignManager()


class TaskIcons(models.Model):
    task_icons = models.ForeignKey(TaskIcon, on_delete=models.CASCADE)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)


class Status(models.Model):
    task_status = models.ForeignKey(TaskStatus, on_delete=models.CASCADE)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    objects = TaskManager()


class FlatTask(models.Model):
    flat = models.ForeignKey(Flat, on_delete=models.CASCADE)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)

    objects = TaskManager()

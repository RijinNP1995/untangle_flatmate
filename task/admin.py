from django.contrib import admin

# Register your models here.
from task import models as model


@admin.register(model.Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ('task_name', 'flat', 'created_by', 'created_on')
    list_display_links = ('task_name', 'flat',)


@admin.register(model.AssignTask)
class AssignTaskAdmin(admin.ModelAdmin):
    list_display = ('task', 'assigned_to')


@admin.register(model.Status)
class StatusTaskAdmin(admin.ModelAdmin):
    list_display = ('task_status', 'task')


@admin.register(model.TaskIcons)
class StatusTaskAdmin(admin.ModelAdmin):
    list_display = ('task_icons', 'task')

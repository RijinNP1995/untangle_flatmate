from django.db import transaction

from core import log_data
from flat_api.models import Flat
from flatmates_backend import responses as message
from master import models as master_models
from notifications.services import multi_notify
from task import models as task_model
from task import serializers as serializer_
from user.serializers import ResidenceSerializer


class TaskManagementServices:
    @transaction.atomic
    def add_new_task_service(self, user, data):
        """
            Method to perform the add new task operation
            :param user : logged in user info
            :param data : data add new task
        """
        if not user:
            return message.user_not_exist()
        serializer = serializer_.TaskSerializer(data=data)
        if serializer.is_valid(raise_exception=True):
            task_obj = serializer.save(created_by=user)
            log_data.warning(f'enter the valid assigned_to id as a list')
            if data.get('assigned_to'):
                assigned_to_ids = [{'task': task_obj.id, 'assigned_to': x} for x in data.get('assigned_to') if x]
                assign_task_ser = serializer_.AssignTaskSerializer(data=assigned_to_ids, many=True)
                if assign_task_ser.is_valid(raise_exception=True):
                    assign_task_ser.save()
                    log_data.warning(f'enter the valid task_status id ')
                    if data.get('task_status'):
                        task_status_ids = {'task': task_obj.id, 'task_status': data.get('task_status')}
                        task_data_ser = serializer_.StatusSerializer(data=task_status_ids, many=False)
                        if task_data_ser.is_valid(raise_exception=True):
                            task_data_ser.save()
                            log_data.warning(f'enter the valid task_icon id ')
                    if data.get('task_icon'):
                        task_icon_ids = {'task': task_obj.id, 'task_icons': data.get('task_icon')}
                        task_data_ser = serializer_.IconSerializer(data=task_icon_ids, many=False)
                        if task_data_ser.is_valid(raise_exception=True):
                            task_data_ser.save()
                            log_data.warning(f'enter the valid flat id ')
                        if data.get('flat'):
                            flat_task_id = {'task': task_obj.id, 'flat': data.get('flat')}
                            flat_data_ser = serializer_.FlatTaskSerializer(data=flat_task_id, many=False)
                            if flat_data_ser.is_valid(raise_exception=True):
                                flat_data_ser.save()
                                log_data.debug(f'{assign_task_ser.data} Add the task successfully')
                                user_list = data.get('assigned_to')
                                if user.id in user_list:
                                    user_list.remove(user.id)
                                multi_notify(users=user_list, actor=user.display_name,
                                             verb='has assigned you a new task',
                                             action="",
                                             notification_type='NEW_TASK', description='',
                                             icon=master_models.NotificationIcon.objects.get(label='FLAT'),
                                             title="New Task")
                                return message.add_task(assign_task_ser)
        else:
            log_data.error(f'{serializer.errors}')
            return message.invalid_serializer(serializer)

    def view_task_service(self, user, flat_id, from_date='', to_date=''):
        """
        :param to_date: to due date
        :param from_date: from task due date
        :param user:
        :param flat_id:
        :return:
        """
        if user:
            task_details = task_model.Task.objects.get_tasks(flat_id=flat_id,is_delete=False)
            if from_date:
                task_details = task_details.filter(due_date__gte=from_date)
            if to_date:
                task_details = task_details.filter(due_date__lte=to_date)
            task_ser = serializer_.ViewTaskSerializer(task_details, many=True)
            flat_task = Flat.objects.get_by_id(pk=flat_id)
            flat_data = ResidenceSerializer(flat_task, many=False)
            log_data.info(f'{flat_id} get the task details according to the flat id')
            return message.view_task(task_ser, flat_data)
        log_data.error('user does not exist')
        return message.user_not_exist()

    @transaction.atomic()
    def update_task_service(self, data, user, id):
        """
        :param data:
        :param user:
        :param id:
        :return: api to update the task
        """
        if not user:
            log_data.error('user does not exist')
            return message.user_not_exist()
        # get the values according to the task id
        task_obj = task_model.Task.objects.filter(pk=id, is_delete=False)
        if task_obj.exists():
            # serializer to update the task objects
            serializer = serializer_.TaskUpdateSerializer(data=data, partial=True)
            if serializer.is_valid(raise_exception=True):
                task_obj = serializer.update(instance=task_model.Task.objects.get(pk=id, is_delete=False),
                                             validated_data=serializer.validated_data)
                task_obj.modified_by = user
                task_obj.save()
                log_data.debug(f'update the common information about task')
                if data.get('assigned_to'):
                    # check whether the data is existed of the model AssignTask for this particular id
                    if task_model.AssignTask.objects.get_assign_to(task=task_obj).exists():
                        task_model.AssignTask.objects.get_assign_to(task=task_obj).delete()
                    assigned_to_ids = [{'task': task_obj.id, 'assigned_to': x} for x in data.get('assigned_to') if x]
                    assign_task_ser = serializer_.AssignTaskSerializer(data=assigned_to_ids, many=True)
                    if assign_task_ser.is_valid(raise_exception=True):
                        assign_task_ser.save()
                        log_data.warning(f'enter the valid task_status id ')
                        if data.get('task_status'):
                            # check the data is exist in the status model with the task id
                            if task_model.Status.objects.select_related('task').filter(task=task_obj.id).exists():
                                task_model.Status.objects.get(task=task_obj).delete()
                            task_status_ids = {'task': task_obj.id, 'task_status': data.get('task_status')}
                            task_data_ser = serializer_.StatusSerializer(data=task_status_ids, many=False)
                            if task_data_ser.is_valid(raise_exception=True):
                                task_data_ser.save()
                                log_data.warning(f'enter the valid task_icon id ')

                        if data.get('task_icon'):
                            # check the data is exist in the TaskIcon Model with the same task id
                            if task_model.TaskIcons.objects.select_related('task').filter(task=task_obj):
                                task_model.TaskIcons.objects.get(task=task_obj).delete()
                            task_icon_ids = {'task': task_obj.id, 'task_icons': data.get('task_icon')}
                            task_data_ser = serializer_.IconSerializer(data=task_icon_ids, many=False)
                            if task_data_ser.is_valid(raise_exception=True):
                                task_data_ser.save()
                                log_data.debug(f'task assigned to multiple users')
                                task_ser = serializer_.ViewTaskSerializer(task_obj, many=False)
                                log_data.info(f'The task updated successfully')
                                return message.update_task(task_ser)
            else:
                log_data.error(f'{serializer.errors} update task operation failed')
                return message.update_failed(serializer)
        else:
            log_data.error(f'task id not found')
            return message.no_data_found()

    def delete_task(self, id, flat_id):
        """
        :param id:
        :param flat_id:
        :return:
        """
        if task_model.Task.objects.filter(id=id).exists():
            task_screen = task_model.Task.objects.get(id=id)
            task_screen.is_delete = True
            task_screen.save()
            task_data = task_model.Task.objects.get_tasks(flat_id=flat_id, is_delete=False)
            serializer = serializer_.ViewTaskSerializer(task_data, many=True)
            flat_task = Flat.objects.get_by_id(pk=flat_id)
            flat_ser = ResidenceSerializer(flat_task, many=False)
            log_data.info(f'delete the particular task successfully')
            return message.delete_task(serializer, flat_ser)
        else:
            log_data.error(f'the task does not exist')
            return message.no_data_found()

    def delete_participant(self, task_id, assigned_to):
        """
        Delete participant
        :param task_id:
        :param assigned_to:
        :return:
        """
        if task_model.AssignTask.objects.select_related('task').filter(task_id=task_id).exists():
            if task_model.AssignTask.objects.select_related('task').filter(assigned_to=assigned_to).exists():
                task_data = task_model.AssignTask.objects.get_assign_to(task_id=task_id)
                task_model.AssignTask.objects.get_assign_to(assigned_to=assigned_to).delete()
                assign_ser = serializer_.AssignTaskSerializer(data=task_data, many=True)
                assign_ser.is_valid()
                log_data.info(f'remove the participant from the particular task')
                return message.delete_participant(assign_ser)
            else:
                log_data.error(f'assigned_to id does not exist')
                return message.no_data_found()
        else:
            log_data.error(f'task id does not exist')
            return message.no_data_found()

    def view_task_by_task_id(self, task_id):
        """
        :param task_id:
        :return: view the task according to the task id
        """
        task_details = task_model.Task.objects.get(id=task_id, is_delete=False)
        task_ser = serializer_.ViewTaskSerializer(task_details, many=False)
        return message.view_task_by_id(task_ser)

    @transaction.atomic()
    def update_task_status(self, task_id, task_status):
        """
        :param task_id:
        :param task_status:
        :return: update the task status
        """
        task_obj = task_model.Task.objects.get(id=task_id)
        if task_model.AssignTask.objects.select_related('task').filter(task_id=task_id).exists():
            if task_model.Status.objects.select_related('task').filter(task=task_obj.id).exists():
                task_model.Status.objects.get(task=task_obj).delete()
                task_status_ids = {'task': task_obj.id, 'task_status': task_status}
                task_data_ser = serializer_.StatusSerializer(data=task_status_ids, many=False)
                if task_data_ser.is_valid(raise_exception=True):
                    task_data_ser.save()
                    stat_obj = task_model.Task.objects.get(id=task_id)
                    serializer = serializer_.ViewTaskSerializer(stat_obj)
                    log_data.info(f'Task status updated successfully')
                    return message.update_task_status(serializer)
                else:
                    log_data.error(f'{task_data_ser.errors} update task_status operation failed')
                    return message.update_failed(task_data_ser)
        else:
            return message.id_does_not_exist()

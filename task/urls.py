from django.urls import path

from task import views as view

urlpatterns = [
    path('add-task/', view.AddNewTask.as_view(), name='add_task'),
    path('update-task/<int:pk>/', view.AddNewTask.as_view(), name='update_task'),
    path('view-task/', view.AddNewTask.as_view(), name='view_task'),
    path('delete-task/', view.AddNewTask.as_view(), name='delete_task'),
    path('delete-participant/', view.TaskParticipant.as_view(), name='delete_participant'),
    path('get-task-data/', view.ViewTask.as_view(), name='get_task_data'),
    path('update-task-status/', view.ViewTask.as_view(), name='update_status')
]

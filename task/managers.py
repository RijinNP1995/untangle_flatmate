from flatmates_backend.base_manager import BaseManager


class AssignManager(BaseManager):
    def get_assign_to(self, **filter_args):
        return self.get_all().filter(**filter_args)


class TaskManager(BaseManager):
    def get_tasks(self, **filter_args):
        return self.get_all().filter(**filter_args)


class TaskRatingManager(BaseManager):
    def get_badges(self, **filter_args):
        return self.get_all().filter(**filter_args)


class AssignToManager(BaseManager):
    def get_assign_to(self, **filter_args):
        return self.get_all().filter(**filter_args)


class TaskDataManager(BaseManager):
    def get_tasks(self, **filter_args):
        return self.get_all().filter(**filter_args)

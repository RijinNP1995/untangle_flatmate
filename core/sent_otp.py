import pyotp
from django.db.models import Q

from core import log_data
from flatmates_backend import responses as message
from user import models as model
from user import sent_otp as send_otp


def share_otp(user_id):
    """
    Sent user OTP service
    :param user_id:
    :return:
    """
    # get the phone number
    user_phone = model.User.objects.filter(Q(phone=user_id))
    # get the mail
    user_mail = model.User.objects.filter(Q(email=user_id))
    user_data = user_phone | user_mail
    if not user_data:
        log_data.error('user phone/email does not exist')
        return message.api_failed()
    # create the otp to send
    base32secret = pyotp.random_base32()
    time_otp = pyotp.TOTP(base32secret, interval=1000)
    time_otp = time_otp.now()
    user_verify = model.User.objects.get(Q(email=user_id) | Q(phone=user_id))
    user_verify.otp = time_otp
    user_verify.save()
    # check if user has phone number
    log_data.debug(f'data inserted successfully')
    if user_phone:
        # sent otp to phone
        send_otp.send_otp_to_phone(phone=user_id,
                                   otp=time_otp)
        log_data.info('one time password generated successfully and send to phone')
        return message.get_conflicts(time_otp)
    # check if the user has email id
    else:
        # send otp to email
        send_otp.send_otp_to_email(email=user_id, otp=time_otp)
    log_data.info('one time password generated successfully and send to email')
    return message.get_conflicts(time_otp)

# method to find the day of the week
import datetime


def find_day_of_week(dt):
    today = datetime.date.today()
    one_day = datetime.timedelta(days=1)

    if today == dt:
        return 'Today'
    elif dt == (today - one_day):
        return 'Yesterday'
    elif dt == (today + one_day):
        return 'Tomorrow'
    else:
        return dt.strftime("%A")

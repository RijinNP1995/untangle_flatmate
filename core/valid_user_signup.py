from rest_framework.authtoken.models import Token

from core import log_data
from core.profile_percentage import user_profile_percentage
from flat_api import models as flat_model
from flat_api.serializers import FlatDetailsSerializer
from flatmates_backend import responses as message
from user import models as model
from user import serializers as user_ser


def valid_user_signup(validate_user, user_id, fcm_token, password):
    if validate_user:

        if model.User.objects.filter(email=user_id).exists():
            user = model.User.objects.get(email=user_id)
            in_the_flat = True if flat_model.FlatTenant.objects.get_active_tenants(tenant=user) else False
            if not in_the_flat:
                flat_id = None
                flat_ser = user_ser.ReturnNullSerializer(flat_id)
            else:
                flat_data = flat_model.FlatTenant.objects.get(tenant=user, is_active=True)
                flat_id = flat_data.flat_id
                flat_obj = flat_model.Flat.objects.get_by_id(pk=flat_id)
                flat_ser = FlatDetailsSerializer(flat_obj)
            # check user password
            if user.check_password(password):
                # generating user token
                token, _ = Token.objects.get_or_create(user=user)
                user.fcm_token = fcm_token
                user.save()
                user_data = user_ser.ProfileDetailsSerializer(user)
                user_percentage = user_profile_percentage(user_data)
                log_data.info('user sign in completed with email id')
                return message.user_sign_in(token, user, in_the_flat, flat_id, user_data, flat_ser, user_percentage)
            else:
                return message.invalid_password()
        # checking for user phone condition
        elif model.User.objects.filter(phone=user_id).exists():
            user = model.User.objects.get(phone=user_id)
            user_data = user_ser.ProfileDetailsSerializer(user)
            user_percentage = user_profile_percentage(user_data)
            if user.check_password(password):
                # generating user token
                token, _ = Token.objects.get_or_create(user=user)
                user.fcm_token = fcm_token
                user.save()
                in_the_flat = True if flat_model.FlatTenant.objects.get_active_tenants(tenant=user) else False
                if not in_the_flat:
                    flat_id = None
                    flat_ser = user_ser.ReturnNullSerializer(flat_id)
                else:
                    flat_data = flat_model.FlatTenant.objects.get(tenant=user, is_active=True)
                    flat_id = flat_data.flat_id
                    flat_obj = flat_model.Flat.objects.get_by_id(pk=flat_id)
                    flat_ser = FlatDetailsSerializer(flat_obj)
                log_data.info('User log in successful with mobile number')
                return message.user_sign_in(token, user, in_the_flat, flat_id, user_data, flat_ser, user_percentage)
            else:
                return message.invalid_password()

    else:
        log_data.error('Please check the user_id')
        return message.check_the_user_id()

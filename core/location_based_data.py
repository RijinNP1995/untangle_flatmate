from django.db.models import Q, F

from service_provider import models as model


# get the list of services according to category_id
def get_service_list_by_category(filter_data, category_id):
    """
    :param filter_data:
    :param category_id:
    :return: list of services according to the category and location
    """
    service_list = model.ServiceProvider.objects.get_by_filter(service_category_id=category_id,
                                                               is_delete=False, private=False,
                                                               is_active=True)
    if filter_data.get('radius'):
        service_list = get_service_nearby_location(service_list=service_list,
                                                   radius_km=filter_data.get('radius'),
                                                   current_latitude=filter_data.get('current_lat'),
                                                   current_longitude=filter_data.get('current_lon'))
    return service_list.distinct()


# get service_list by location data
def get_service_list(filter_data):
    """
    :param filter_data:
    :return: get service_list by location data
    """
    service_list = model.ServiceProvider.objects.filter(private=False)
    if filter_data.get('radius'):
        service_list = get_service_nearby_location(service_list=service_list,
                                                   radius_km=filter_data.get('radius'),
                                                   current_latitude=filter_data.get('current_lat'),
                                                   current_longitude=filter_data.get('current_lon'))
    if filter_data.get('search_param'):
        qs = (Q(contact_name__icontains=filter_data.get('search_param')))
        service_list = service_list.filter(qs)
    return service_list.distinct()


def get_service_nearby_location(service_list, radius_km, current_latitude, current_longitude):
    """
    :param service_list:
    :param radius_km:
    :param current_latitude:
    :param current_longitude:
    :return: objects sorted by distance to specified coordinates which distance is less than max_distance given in kilometers
    """
    qs = service_list.annotate(
        radius_sqr=pow(F('lat') - current_latitude, 2) + pow(F('lon') - current_longitude, 2)
    ).filter(
        radius_sqr__lte=pow(radius_km / 11, 2)
    )
    return qs

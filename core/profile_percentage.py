from flatmates_backend.responses import get_profile_data


# get the profile percentage of the user details
def profile_percentage(user_info):
    percent = {'first_name': 10, 'last_name': 5, 'display_name': 5, 'gender': 5, 'profession': 5,
               'company_name': 5, 'age': 5, 'locality': 10, 'phone': 10, 'email': 10, 'description': 5, 'quote': 5,
               'photo': 30}
    total = 0
    if user_info.data.get('first_name'):
        total += percent.get('first_name', 0)
    if user_info.data.get('last_name'):
        total += percent.get('last_name', 0)
    if user_info.data.get('gender'):
        total += percent.get('gender', 0)
    if user_info.data.get('display_name'):
        total += percent.get('display_name', 0)
    if user_info.data.get('profession'):
        total += percent.get('profession', 0)
    if user_info.data.get('company_name'):
        total += percent.get('company_name', 0)
    if user_info.data.get('age'):
        total += percent.get('age', 0)
    if user_info.data.get('locality'):
        total += percent.get('locality', 0)
    if user_info.data.get('description'):
        total += percent.get('description', 0)
    if user_info.data.get('quote'):
        total += percent.get('quote', 0)
    if user_info.data.get('photo'):
        total += percent.get('photo', 0)
    if user_info.data.get('email'):
        total += percent.get('email', 0)
    elif user_info.data.get('phone'):
        total += percent.get('phone', 0)
    return get_profile_data(user_info, total)


# get the profile percentage of the user details
def user_percentage(user_info):
    percent = {'first_name': 10, 'last_name': 5, 'display_name': 5, 'gender': 5, 'profession': 5,
               'company_name': 5, 'age': 5, 'locality': 10, 'phone': 10, 'email': 10, 'description': 5, 'quote': 5,
               'photo': 30}
    total = 0
    if user_info.data.get('first_name'):
        total += percent.get('first_name', 0)
    if user_info.data.get('last_name'):
        total += percent.get('last_name', 0)
    if user_info.data.get('gender'):
        total += percent.get('gender', 0)
    if user_info.data.get('display_name'):
        total += percent.get('display_name', 0)
    if user_info.data.get('profession'):
        total += percent.get('profession', 0)
    if user_info.data.get('company_name'):
        total += percent.get('company_name', 0)
    if user_info.data.get('age'):
        total += percent.get('age', 0)
    if user_info.data.get('locality'):
        total += percent.get('locality', 0)
    if user_info.data.get('description'):
        total += percent.get('description', 0)
    if user_info.data.get('quote'):
        total += percent.get('quote', 0)
    if user_info.data.get('photo'):
        total += percent.get('photo', 0)
    if user_info.data.get('email'):
        total += percent.get('email', 0)
    elif user_info.data.get('phone'):
        total += percent.get('phone', 0)
    return get_profile_data(user_info, total)


def user_profile_percentage(user_info):
    percent = {'first_name': 10, 'last_name': 5, 'display_name': 5, 'gender': 5, 'profession': 5,
               'company_name': 5, 'age': 5, 'locality': 10, 'phone': 10, 'email': 10, 'description': 5, 'quote': 5,
               'photo': 30}
    total = 0
    if user_info.data.get('first_name'):
        total += percent.get('first_name', 0)
    if user_info.data.get('last_name'):
        total += percent.get('last_name', 0)
    if user_info.data.get('gender'):
        total += percent.get('gender', 0)
    if user_info.data.get('display_name'):
        total += percent.get('display_name', 0)
    if user_info.data.get('profession'):
        total += percent.get('profession', 0)
    if user_info.data.get('company_name'):
        total += percent.get('company_name', 0)
    if user_info.data.get('age'):
        total += percent.get('age', 0)
    if user_info.data.get('locality'):
        total += percent.get('locality', 0)
    if user_info.data.get('description'):
        total += percent.get('description', 0)
    if user_info.data.get('quote'):
        total += percent.get('quote', 0)
    if user_info.data.get('photo'):
        total += percent.get('photo', 0)
    if user_info.data.get('email'):
        total += percent.get('email', 0)
    elif user_info.data.get('phone'):
        total += percent.get('phone', 0)
    return total

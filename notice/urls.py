from django.urls import path

from . import views

urlpatterns = [
    # path('create-event/', views.CreateEvent.as_view(), name='create_event'),
    path('remove-event/<int:pk>/', views.RemoveEvent.as_view(), name='remove_event'),
    path('list-events/<int:flat_id>/', views.ListEvent.as_view(), name='list_events'),
    path('pin-unpin-events/<int:pk>/', views.PinUnpinEvent.as_view(), name='pin_unpin_event'),
    path('edit-event/<int:pk>/', views.EventView.as_view(), name='edit_events'),
    path('list-events/', views.EventView.as_view(), name='event_view'),
    path('add-event/', views.EventView.as_view(), name='add_event'),
    path('archive-event/<int:pk>/', views.ArchiveEventView.as_view(), name='archive_event'),
]

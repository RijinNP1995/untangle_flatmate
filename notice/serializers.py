from rest_framework import serializers

from core.utils import find_day_of_week
from flat_api import models as flat_models
from . import models as notice_model
from django.utils import timezone
from user import models as user_models
from user import serializers as user_serializers


class CreateEventSerializer(serializers.ModelSerializer):
    """
    Serializer class for flat
    """
    flat_id = serializers.IntegerField()

    class Meta:
        model = notice_model.Events
        fields = (
            'flat_id',
            'event_title',
            'event_description'
        )

    def validate_flat_id(self, flat_id):
        flat_obj = flat_models.Flat.objects.get_by_id(pk=flat_id)
        return flat_obj.id


class EventFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = notice_model.EventFile
        fields = '__all__'


class EventListSerializer(serializers.ModelSerializer):
    """
    Serializer class for flat
    """
    files = serializers.SerializerMethodField()
    created_day = serializers.SerializerMethodField()
    created_date = serializers.SerializerMethodField()
    created_user = serializers.SerializerMethodField()

    class Meta:
        model = notice_model.Events
        fields = (
            'id',
            'flat',
            'event_title',
            'event_description',
            'files',
            'created_day',
            'created_date',
            'created_on',
            'created_user'
        )

    def get_files(self, obj):
        event_files = notice_model.EventFile.objects.get_files(event=obj.id)
        serializer = EventFileSerializer(event_files, many=True)
        return serializer.data

    def get_created_day(self, obj):
        """
        :param obj: menu instance
        :return: day of menu date
        """
        return find_day_of_week(obj.created_on)

    def get_created_date(self, obj):
        return (timezone.now() - obj.created_on).days

    def get_created_user(self, obj):
        user_obj = user_models.User.objects.get(id=obj.created_by.id)
        return user_serializers.UserInfoSerializer(user_obj).data


class CreateEventByUserSerializer(serializers.ModelSerializer):
    """
    Serializer class for event without flat id
    """

    class Meta:
        model = notice_model.Events
        fields = (
            'event_title',
            'event_description'
        )

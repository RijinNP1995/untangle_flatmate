from flatmates_backend.base_manager import BaseManager


class EventManager(BaseManager):
    pass


class EventFileManager(BaseManager):
    def get_files(self, **filter_args):
        return self.get_all().filter(**filter_args)


class PinnedEventManager(BaseManager):
    pass


class ArchivedEventManager(BaseManager):
    pass

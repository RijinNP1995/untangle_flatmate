from django.db import models
from django.utils.translation import ugettext_lazy as _

from flat_api.models import Flat
from master.models import BaseModel, Amenity, Preferences, AgeGroup
from user.models import Gender, User
from . import managers as event_manager


class Events(BaseModel):
    flat = models.ForeignKey(Flat, on_delete=models.CASCADE, null=True, blank=True)
    event_title = models.CharField(_('Event Name'), max_length=50, null=True, blank=True)
    event_description = models.CharField(_('Event Description'), max_length=500, null=True, blank=True)
    event_date = models.DateField(_('Event Date'), null=True, blank=True)
    event_timings = models.TimeField(_('Event Time'), null=True, blank=True)
    event_venue = models.CharField(_('Event Venue'), max_length=75, null=True, blank=True)
    event_fees = models.IntegerField(_('Event Fee'), default=0, null=True, blank=True)
    event_organizer_details = models.CharField(_('Organizer Details'), max_length=140, null=True, blank=True)
    objects = event_manager.EventManager()

    def __str__(self):
        return f"{self.event_title} added by {self.created_by}"


class EventFile(models.Model):
    file = models.ImageField()
    event = models.ForeignKey(Events, on_delete=models.CASCADE)
    objects = event_manager.EventFileManager()

    def __str__(self):
        return self.event.__str__() + " " + self.file.__str__()


class PinnedEvents(models.Model):
    event = models.ForeignKey(Events, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    pinned = models.BooleanField(default=False, db_index=True)
    objects = event_manager.PinnedEventManager()

    def __str__(self):
        return self.event.__str__() + " " + self.user.__str__()


class ArchivedEvents(models.Model):
    event = models.ForeignKey(Events, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    archive = models.BooleanField(default=True)
    objects = event_manager.ArchivedEventManager()

    def __str__(self):
        return self.event.__str__() + " " + self.user.__str__()

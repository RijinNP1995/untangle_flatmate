from rest_framework import status
from rest_framework.response import Response

from core import log_data
from flat_api import models as flat_model
from flatmates_backend import responses as message
from notifications.services import multi_notify
from . import models as notice_model
from . import serializers as notice_serializer


class NoticeManagementServices:
    # def create_event(self, user, data, files):
    #     """
    #     Method to create events
    #     :param user: logged in user
    #     :param data: data to create an event
    #     :param files: images
    #     :return: status
    #     """
    #     if user:
    #         serializer = notice_serializer.CreateEventSerializer(data=data)
    #         if serializer.is_valid():
    #             event_obj = serializer.save(created_by=user)
    #             if files:
    #                 image_list = [{'event': event_obj.id,
    #                                'file': files['image[' + str(i) + ']']} for i in range(files.__len__())]
    #                 event_file_ser = notice_serializer.EventFileSerializer(data=image_list, many=True)
    #                 if event_file_ser.is_valid():
    #                     event_file_ser.save()
    #                     log_data.debug(f'{event_file_ser} New post added successfully')
    #             log_data.info(f'New post added successfully')
    #             event_list = notice_model.Events.objects.get_by_filter(flat=event_obj.flat).order_by('-created_on')
    #             return self.notice_response(message='New post added successfully', event_list=event_list, user=user,
    #                                         status_code=status.HTTP_201_CREATED)
    #         log_data.error(f'{serializer.errors} invalid data')
    #         return message.invalid_data()
    #     log_data.error(f'user does not exist')
    #     return message.invalid_user()

    def create_event_user_token(self, user, data, files):
        """
        Method to create events
        :param user: logged in user
        :param data: data to create an event
        :param files: images
        :return: status
        """
        if user:
            serializer = notice_serializer.CreateEventByUserSerializer(data=data)
            flat_obj = flat_model.FlatTenant.objects.get_tenant(tenant=user.id)[0]
            if serializer.is_valid():
                event_obj = serializer.save(created_by=user, flat=flat_obj.flat)
                if files:
                    image_list = [{'event': event_obj.id,
                                   'file': files['image[' + str(i) + ']']} for i in range(files.__len__())]
                    event_file_ser = notice_serializer.EventFileSerializer(data=image_list, many=True)
                    if event_file_ser.is_valid():
                        event_file_ser.save()
                        log_data.debug(f'{event_file_ser} New post added successfully')
                log_data.info(f'New post added successfully')
                user_list = list(
                    flat_model.FlatTenant.objects.get_active_tenants(flat=flat_obj.flat).values_list('tenant_id',
                                                                                                     flat=True).distinct())
                if user.id in user_list:
                    user_list.remove(user.id)
                multi_notify(users=user_list, actor=user.display_name,
                             verb='has added a new event',
                             action=event_obj.id,
                             notification_type='NEW_EVENT', description='',
                             icon='',
                             title="New Event")
                event_list = notice_model.Events.objects.get_by_filter(flat=event_obj.flat).order_by('-created_on')
                return self.notice_response(message='New post added successfully', event_list=event_list, user=user,
                                            status_code=status.HTTP_201_CREATED)
            log_data.error(f'{serializer.errors} invalid data')
            return message.invalid_data()
        log_data.error(f'user does not exist')
        return message.invalid_user()

    def edit_event_user_token(self, user, data, files, id):
        """
        Method to update events
        :param id:
        :param user: logged in user
        :param data: data to create an event
        :param files: images
        :return: status
        """
        if user:
            if notice_model.Events.objects.pk_does_exist(id=id):
                event_obj = notice_model.Events.objects.get_by_id(pk=id)
                if event_obj.created_by == user:
                    event_ser = notice_serializer.CreateEventByUserSerializer(data=data, partial=True)
                    if event_ser.is_valid():
                        event_new_obj = event_ser.update(instance=event_obj,
                                                         validated_data=event_ser.validated_data)
                        event_new_obj.modified_by = user
                        event_new_obj.save()
                        if files:
                            if notice_model.EventFile.objects.get_files(event=event_obj).exists():
                                notice_model.EventFile.objects.get_files(event=event_obj).delete()
                            image_list = [{'event': event_obj.id,
                                           'file': files['image[' + str(i) + ']']} for i in range(files.__len__())]
                            event_file_ser = notice_serializer.EventFileSerializer(data=image_list, many=True)
                            if event_file_ser.is_valid():
                                event_file_ser.save()
                        if data.get('clear') == "CLEAR":
                            if notice_model.EventFile.objects.get_files(event=event_obj).exists():
                                notice_model.EventFile.objects.get_files(event=event_obj).delete()
                        event_list = notice_model.Events.objects.get_by_filter(flat=event_obj.flat).order_by(
                            '-created_on')
                        return self.notice_response(message='Event updated successfully', event_list=event_list,
                                                    user=user,
                                                    status_code=status.HTTP_200_OK)
                    log_data.error(f'{event_ser.errors} invalid data')
                    return message.invalid_data()

                log_data.error(f'user does not have the permission to update details')
                return message.permission_denied()
            log_data.error(f'id {id} does not exist')
            return message.error_400('id does not exist')

        log_data.error(f'user does not exist')
        return message.invalid_user()

    def list_events_user_token(self, user):
        """
        get the list of events
        :param user: logged in user
        :return: status and events
        """
        if user:
            flat_obj = flat_model.FlatTenant.objects.get_tenant(tenant=user.id)[0]
            event_list = notice_model.Events.objects.get_by_filter(flat=flat_obj.flat.id).order_by('-created_on')
            msg = 'Events loaded successfully'
            log_data.info(msg)
            return self.notice_response(message=msg, event_list=event_list, user=user)
        log_data.error(f'user does not exist')
        return message.invalid_user()

    def remove_event(self, user, id):
        """
        Method to remove an item from the notice board
        :param user: logged in user
        :param id: event id
        :return: status and event list
        """
        if user:
            if notice_model.Events.objects.pk_does_exist(id=id):
                event_obj = notice_model.Events.objects.get_by_id(pk=id)
                if user == event_obj.created_by:
                    event_obj.modified_by = user
                    event_obj.is_delete = True
                    event_obj.is_active = False
                    event_obj.save()
                    log_data.info(f'remove the event successfully')
                    event_list = notice_model.Events.objects.get_by_filter(flat=event_obj.flat).order_by('-created_on')
                    return self.notice_response(message='Event removed.', event_list=event_list, user=user,
                                                status_code=status.HTTP_200_OK)
                log_data.error(f'user permission denied')
                return message.permission_denied()
            log_data.error(f'flat does not exist')
            return message.flat_does_not_exist()
        log_data.error(f'invalid user')
        return message.invalid_user()

    def get_pinned_event_list(self, event_list, user):
        """
        get pinned events of a user
        :param event_list:
        :param user:
        :return:
        """
        event_list = event_list.filter(pinnedevents__user=user, pinnedevents__pinned=True)
        event_list_ser = notice_serializer.EventListSerializer(event_list, many=True)
        log_data.info(f'get event list successfully')
        return event_list_ser

    def get_un_pinned_event_list(self, event_list, user):
        """
        get unpinned events of a user
        :param event_list:
        :param user:
        :return:
        """
        event_list = event_list.exclude(pinnedevents__user=user, pinnedevents__pinned=True)
        event_list = event_list.exclude(archivedevents__archive=True)
        event_list_ser = notice_serializer.EventListSerializer(event_list, many=True)
        log_data.info(f' get event list successfully')
        return event_list_ser

    def get_archived_event_list(self, event_list, user):
        """
        get archived events of a user
        :param event_list:
        :param user:
        :return:
        """
        event_list = event_list.filter(archivedevents__user=user, archivedevents__archive=True)
        event_list_ser = notice_serializer.EventListSerializer(event_list, many=True)
        log_data.info(f' get event list successfully')
        return event_list_ser

    def list_events(self, user, flat_id):
        """
        get the list of events
        :param user: logged in user
        :param flat_id: contains flat info
        :return: status and events
        """
        if user:
            event_list = notice_model.Events.objects.get_by_filter(flat=flat_id).order_by('-created_on')
            log_data.info(f'Events loaded successfully')
            return self.notice_response(message='Events loaded successfully.', event_list=event_list, user=user,
                                        status_code=status.HTTP_200_OK)

    def pin_unpin_events(self, user, id):
        if user:
            msg_object = "Pinned"
            if notice_model.Events.objects.pk_does_exist(id=id):
                event_obj = notice_model.Events.objects.get_by_id(pk=id)
                if notice_model.PinnedEvents.objects.filter(event=event_obj, user=user).exists():
                    notice_model.PinnedEvents.objects.filter(event=event_obj, user=user).delete()
                    msg_object = "Un-pinned "
                else:
                    notice_model.ArchivedEvents.objects.filter(event=event_obj, user=user).delete()
                    notice_model.PinnedEvents.objects.create(event=event_obj,
                                                             user=user,
                                                             pinned=True)
                event_list = notice_model.Events.objects.get_by_filter(flat=event_obj.flat).order_by('-created_on')
                return self.notice_response(message=msg_object + ' successfully', event_list=event_list, user=user,
                                            status_code=status.HTTP_200_OK)
            log_data.error(f'event id does not exist')
            message.id_does_not_exist()
        log_data.error(f'user does not exist')
        return message.invalid_user()

    def archive_notice_list(self, user, id):
        """
        :param id:
        :param user: logged in user[flat admin]
        :return: status
        """
        if user.id is not None:
            if notice_model.Events.objects.pk_does_exist(id=id):
                event_obj = notice_model.Events.objects.get_by_id(pk=id)
                if notice_model.ArchivedEvents.objects.filter(event=event_obj, user=user).exists():
                    notice_model.ArchivedEvents.objects.filter(event=event_obj, user=user).delete()
                    msg_object = 'Removed from archive list.'
                else:
                    notice_model.PinnedEvents.objects.filter(event=event_obj, user=user).delete()
                    notice_model.ArchivedEvents.objects.create(event=event_obj,
                                                               user=user,
                                                               archive=True)
                    msg_object = 'Added to archive list.'
                event_list = notice_model.Events.objects.get_by_filter(flat=event_obj.flat).order_by('-created_on')
                return self.notice_response(message=msg_object, event_list=event_list, user=user)
            log_data.error(f'event id does not exist')
            message.id_does_not_exist()
        log_data.error(f'{user} user does not exist')
        return message.invalid_user()

    def notice_response(self, message, event_list, user, status_code=status.HTTP_200_OK):
        """
        :param status_code:
        :param message: display message
        :param event_list: event-list
        :param user: logged in user
        :return: list of events
        """
        response_code = 200
        if status_code == status.HTTP_201_CREATED:
            response_code = 201
        return Response(
            {
                'status': response_code,
                'message': message,
                'pinned_events': self.get_pinned_event_list(event_list=event_list, user=user).data,
                'unpinned_events': self.get_un_pinned_event_list(event_list=event_list, user=user).data,
                'archived_events': self.get_archived_event_list(event_list=event_list, user=user).data,
            },
            status=status_code
        )

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.views import APIView

from core import log_data
from flatmates_backend import responses
from .services import NoticeManagementServices

notice_management_service = NoticeManagementServices()


# class CreateEvent(APIView):


# @swagger_auto_schema(request_body=serializers.CreateEventSerializer)
# @swagger_auto_schema(
#     operation_id='Create a new event on notice board',
#     request_body=openapi.Schema(type=openapi.TYPE_OBJECT,
#                                 properties={
#                                     'event_title': openapi.Schema(
#                                         type=openapi.TYPE_STRING,
#                                     ),
#                                     'event_description': openapi.Schema(
#                                         type=openapi.TYPE_STRING,
#                                     ),
#                                     'image[0]': openapi.Schema(
#                                         type=openapi.TYPE_FILE
#                                     )
#                                 }
#                                 ),
# )
# def post(self, request):
#     """
#     Create a new event on notice board
#     :param request:
#     :return:
#     """
#     try:
#         return notice_management_service.create_event(user=request.user,
#                                                       data=request.data,
#                                                       files=request.FILES)
#     except Exception as e:
#         log_data.error(f'{e.__str__()}')
#         responses.exception_response(e)

class EventView(APIView):
    @swagger_auto_schema(
        operation_id='Create a new event on notice board',
        request_body=openapi.Schema(type=openapi.TYPE_OBJECT,
                                    properties={
                                        'event_title': openapi.Schema(
                                            type=openapi.TYPE_STRING,
                                        ),
                                        'event_description': openapi.Schema(
                                            type=openapi.TYPE_STRING,
                                        ),
                                        'image[0]': openapi.Schema(
                                            type=openapi.TYPE_FILE
                                        )
                                    }
                                    ),
    )
    def post(self, request):
        """
        Create a new event on notice board
        :param request:
        :return:
        """
        try:
            return notice_management_service.create_event_user_token(user=request.user,
                                                                     data=request.data,
                                                                     files=request.FILES)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)

    # @swagger_auto_schema(request_body=serializers.CreateEventByUserSerializer)
    @swagger_auto_schema(
        operation_id='Update an event on notice board',
        request_body=openapi.Schema(type=openapi.TYPE_OBJECT,
                                    properties={
                                        'pk': openapi.Schema(
                                            type=openapi.TYPE_INTEGER
                                        ),
                                        'event_title': openapi.Schema(
                                            type=openapi.TYPE_STRING,
                                        ),
                                        'event_description': openapi.Schema(
                                            type=openapi.TYPE_STRING,
                                        ),
                                        'image[0]': openapi.Schema(
                                            type=openapi.TYPE_FILE
                                        )
                                    }
                                    ),
    )
    def patch(self, request, pk):
        """
        Edit an event on notice board
        :param pk:
        :param request:
        :return:
        """
        try:
            return notice_management_service.edit_event_user_token(user=request.user,
                                                                   data=request.data,
                                                                   files=request.FILES,
                                                                   id=pk)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)

    def get(self, request):
        """
        list the notice board items
        :param request:
        :return:
        """
        try:
            return notice_management_service.list_events_user_token(user=request.user)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)


class RemoveEvent(APIView):
    @swagger_auto_schema(
        operation_id='Delete Event',
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                'pk': openapi.Schema(
                    type=openapi.TYPE_INTEGER,
                )
            }
        ),
    )
    def delete(self, request, pk):
        """
        remove the events from notice board
        :param request:
        :param pk: id of event
        :return:
        """
        try:
            return notice_management_service.remove_event(user=request.user,
                                                          id=pk)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)


class ListEvent(APIView):
    def get(self, request, flat_id):
        """
        list the notice board items
        :param request:
        :param flat_id: flat id
        :return:
        """
        try:
            return notice_management_service.list_events(user=request.user, flat_id=flat_id)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)


class PinUnpinEvent(APIView):
    @swagger_auto_schema(
        operation_id='Pin-Unpin Events',
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                'pk': openapi.Schema(
                    type=openapi.TYPE_INTEGER,
                )
            }
        ),
    )
    def patch(self, request, pk):
        """
        pin and un-pin events
        :param request:
        :param pk: event id
        :return:
        """
        try:
            return notice_management_service.pin_unpin_events(user=request.user, id=pk)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)


class ArchiveEventView(APIView):
    @swagger_auto_schema(
        operation_id='Archive Events',
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                'pk': openapi.Schema(
                    type=openapi.TYPE_INTEGER,
                )
            }
        ),
    )
    def post(self, request, pk):
        """
        archive events
        :param request:
        :param pk: event id
        :return:
        """
        try:
            return notice_management_service.archive_notice_list(user=request.user, id=pk)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)

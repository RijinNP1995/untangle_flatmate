from django.contrib import admin

from . import models


@admin.register(models.Events)
class EventAdmin(admin.ModelAdmin):
    list_display = ('flat', 'event_title', 'is_active', 'created_by')


@admin.register(models.EventFile)
class EventFileAdmin(admin.ModelAdmin):
    list_display = ('event', 'file')


@admin.register(models.PinnedEvents)
class PinnedEventsAdmin(admin.ModelAdmin):
    list_display = ('event', 'user', 'pinned')


@admin.register(models.ArchivedEvents)
class ArchivedEventsAdmin(admin.ModelAdmin):
    list_display = ('event', 'user', 'archive')

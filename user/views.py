import requests
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import permission_classes
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.permissions import AllowAny
from rest_framework.utils import json
from rest_framework.views import APIView

from core import log_data
from flatmates_backend import responses as message
from user import serializers as serializer_
from user.services import UserManagementServices

user_management_service = UserManagementServices()


class UserSignUp(APIView):
    """ API view for user sign up """

    #
    @swagger_auto_schema(
        operation_id='User SignUp',
        request_body=serializer_.UserSignupSerializers)
    @permission_classes((AllowAny,))
    def post(self, request):
        try:
            # user sign up serializer
            serializer = serializer_.UserSignupSerializers(data=request.data)
            # user signup service
            return user_management_service.user_signup(serializer=serializer)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            return message.exception_response(e)


class SentUserOTP(APIView):
    """ API view to set user OTP"""

    def get(self, request):
        try:
            user = request.user
            # sent user OTP service
            return user_management_service.sent_otp(user=user)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            return message.exception_response(e)


class SetUserPassword(APIView):
    """ API view to set user password """

    @swagger_auto_schema(
        operation_id='Set Password',
        request_body=openapi.Schema(type=openapi.TYPE_OBJECT,
                                    properties={
                                        'password': openapi.Schema(
                                            type=openapi.TYPE_STRING,
                                        )
                                    }
                                    ),
    )
    def post(self, request):
        try:
            user = request.user
            # password
            password = request.data.get('password')
            # set user password service
            return user_management_service.set_user_password(user=user, password=password)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            return message.exception_response(e)


class ChangeUserPassword(APIView):
    """ API view to set user password """

    @swagger_auto_schema(
        operation_id='Change Password',
        request_body=openapi.Schema(type=openapi.TYPE_OBJECT,
                                    properties={
                                        'otp': openapi.Schema(type=openapi.TYPE_INTEGER,
                                                              ),
                                        'password': openapi.Schema(
                                            type=openapi.TYPE_STRING,
                                        )
                                    }
                                    ),
    )
    def post(self, request):
        try:
            otp = request.data.get('otp')
            # password
            password = request.data.get('password')
            # set user password service
            return user_management_service.change_password(otp=otp, password=password)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            return message.exception_response(e)


class VerifyUserOTP(APIView):
    """ API view to verify user OTP """

    @swagger_auto_schema(
        operation_id='Verify OTP',
        request_body=openapi.Schema(type=openapi.TYPE_OBJECT,
                                    properties={
                                        'otp': openapi.Schema(
                                            type=openapi.TYPE_INTEGER,
                                        )
                                    }
                                    ),
    )
    def post(self, request):
        try:
            user = request.user
            # get the otp
            otp = request.data.get('otp')
            # OTP verification service
            return user_management_service.otp_verification(user=user, otp=otp)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            return message.exception_response(e)


class UserSignIn(APIView):
    """ API view for user login """

    @swagger_auto_schema(
        operation_id='User SignIn',
        request_body=serializer_.UserLoginSerializer)
    @permission_classes((AllowAny,))
    def post(self, request):
        try:
            # user login serializer
            serializer = serializer_.UserLoginSerializer(data=request.data)
            # user sign_in service
            return user_management_service.user_sign_in(serializer=serializer)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            return message.exception_response(e)


class UpdateUserProfile(APIView):
    """ API view to update user profile """

    @swagger_auto_schema(
        operation_id='Update User Profile',
        request_body=serializer_.UpdateProfileSerializer)
    def post(self, request):
        try:
            # User profile update serializer
            serializer = serializer_.UpdateProfileSerializer(data=request.data, partial=True)
            user = request.user
            return user_management_service.update_user_profile(serializer=serializer, user=user)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            return message.exception_response(e)


class ProfileDetails(APIView):
    @swagger_auto_schema(
        operation_id='Profile Details',
        request_body=openapi.Schema(type=openapi.TYPE_OBJECT,
                                    properties={
                                        'user_id': openapi.Schema(
                                            type=openapi.TYPE_INTEGER,
                                        )
                                    }
                                    ),
    )
    def post(self, request):
        """
        API to fetch the profile details according to the user_id
        :param request: 
        :return: 
        """
        try:
            user_id = request.data.get('user_id')
            return user_management_service.profile_details(user_id=user_id)

        except Exception as e:
            log_data.error(f'{e.__str__()}')
            return message.exception_response(e)

    # Documenting the api to fetch the details according to the user_id
    @swagger_auto_schema(
        operation_id='Delete Profile',
        request_body=openapi.Schema(type=openapi.TYPE_OBJECT,
                                    properties={
                                        'id': openapi.Schema(
                                            type=openapi.TYPE_INTEGER,
                                        )
                                    }
                                    ),
    )
    def delete(self, request):
        """
        API to delete the profile photo
        :param request: 
        :return: 
        """
        try:
            id = request.data.get('id')
            return user_management_service.delete_user_data(id=id)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            return message.exception_response(e)


class UploadProfilePhoto(APIView):
    @swagger_auto_schema(
        operation_id='Upload Photo',
        request_body=openapi.Schema(type=openapi.TYPE_OBJECT,
                                    properties={
                                        'user_id': openapi.Schema(
                                            type=openapi.TYPE_INTEGER,
                                        ),
                                        'photo': openapi.Schema(
                                            type=openapi.TYPE_FILE,
                                        )
                                    }
                                    ),
    )
    def post(self, request):
        """
        API to upload the profile photo
        :param request:
        :return:
        """
        try:
            parser_classes = (MultiPartParser, FormParser)
            serializer = serializer_.ProfilePhotoSerializer(data=request.data)
            user = request.user
            user_id = request.data.get('user_id')
            return user_management_service.upload_photo_service(serializer=serializer, user=user, user_id=user_id)

        except Exception as e:
            log_data.error(f'{e.__str__()}')
            return message.exception_response(e)

    def get(self, request):
        """
        API to fetch the profile photo
        :param request:
        :return:
        """
        try:
            user_id = request.GET.get('user_id')
            return user_management_service.get_profile_photo(user_id=user_id)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            return message.exception_response(e)

    @swagger_auto_schema(
        operation_id='Delete Profile Photo',
        request_body=openapi.Schema(type=openapi.TYPE_OBJECT,
                                    properties={
                                        'user_id': openapi.Schema(
                                            type=openapi.TYPE_INTEGER,
                                        )
                                    }
                                    ),
    )
    def delete(self, request):
        """
        API to delete the profile photo
        :param request:
        :return:
        """
        try:
            id = request.data.get('id')
            return user_management_service.delete_user_photo(id=id)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            return message.exception_response(e)


class ForgotPassword(APIView):
    @swagger_auto_schema(
        operation_id='Forgot Password',
        request_body=openapi.Schema(type=openapi.TYPE_OBJECT,
                                    properties={
                                        'user_id': openapi.Schema(
                                            type=openapi.TYPE_STRING,
                                        )
                                    }
                                    ),
    )
    def post(self, request):
        """
        API to execute forgot password operation
        :param request:
        :return:
        """
        try:
            user_id = request.data.get('user_id')
            return user_management_service.send_forgot_password(user_id=user_id)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            return message.exception_response(e)


class OtpVerification(APIView):
    @swagger_auto_schema(
        operation_id='Verify W/O Token',
        request_body=openapi.Schema(type=openapi.TYPE_OBJECT,
                                    properties={
                                        'otp': openapi.Schema(
                                            type=openapi.TYPE_INTEGER,
                                        )
                                    }
                                    ),
    )
    def post(self, request):
        """
        API to verify the otp to perform forgot password
        :param request:
        :return:
        """
        try:
            user_otp = request.data.get('otp')
            return user_management_service.verify_otp_service(otp=user_otp)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            return message.exception_response(e)


class PeopleSearch(APIView):

    def get(self, request):
        """
        API to search with the name of users
        :param request:
        :return:
        """
        try:
            search_query = request.GET.get('search_query')
            return user_management_service.get_people_search(search_query=search_query)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            return message.exception_response(e)


class GoogleAuthentication(APIView):
    @swagger_auto_schema(
        operation_id='Google Auth',
        request_body=openapi.Schema(type=openapi.TYPE_OBJECT,
                                    properties={
                                        'access_token': openapi.Schema(
                                            type=openapi.TYPE_STRING,
                                        ),
                                        'fcm_token': openapi.Schema(
                                            type=openapi.TYPE_STRING,
                                        )
                                    }
                                    ),
    )
    def post(self, request):
        """
        API to perform the Google authentication
        :param request:
        :return:
        """
        try:
            fcm_token = request.data.get('fcm_token')
            payload = {'access_token': request.data.get("token")}  # validate the token
            r = requests.get('https://www.googleapis.com/oauth2/v2/userinfo', params=payload)
            data = json.loads(r.text)
            return user_management_service.google_auth_service(data=data, fcm_token=fcm_token)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            return message.exception_response(e)


class FacebookAuthentication(APIView):
    @swagger_auto_schema(
        operation_id='Facebook Auth',
        request_body=openapi.Schema(type=openapi.TYPE_OBJECT,
                                    properties={
                                        'access_token': openapi.Schema(
                                            type=openapi.TYPE_STRING,
                                        ),
                                        'fcm_token': openapi.Schema(
                                            type=openapi.TYPE_STRING,
                                        )
                                    }
                                    ),
    )
    def post(self, request):
        """
        API to Login with Facebook
        :param request:
        :return:
        """
        try:
            access_token = request.data.get('access_token')
            fcm_token = request.data.get('fcm_token')
            email_id = request.data.get('email_id')
            new_user = False
            return user_management_service.facebook_auth_service(access_token=access_token, fcm_token=fcm_token,
                                                                 email_id=email_id,
                                                                 new_user=new_user)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            return message.exception_response(e)


class UpdatePhoneDisplayName(APIView):
    def put(self, request):
        """
        :param request:
        :return:
        """
        user = request.user
        parser_classes = (MultiPartParser, FormParser)
        data = request.data
        try:
            return user_management_service.update_display_and_phone(data=data, user=user, file=request.FILES)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            return message.exception_response(e)


class UserFlats(APIView):
    def get(self, request):
        """
        :param request:
        :return:
        """
        try:
            return user_management_service.user_flat_history(user=request.user)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            return message.exception_response(e)


class UpdateUserPhoneNumber(APIView):
    def put(self, request):
        """
        :param request:data and user
        :return:
        """
        try:
            return user_management_service.update_user_phone_no(data=request.data, user=request.user)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            return message.exception_response(e)

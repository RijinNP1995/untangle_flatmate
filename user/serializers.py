from django.core.validators import validate_email
from phonenumber_field.phonenumber import PhoneNumber, to_python
from pyasn1.compat.octets import null
from rest_framework import serializers

from flat_api import models as flat_model
from user import models as user_model


class UserSignupSerializers(serializers.Serializer):
    user_id = serializers.CharField(required=True)
    display_name = serializers.CharField(required=True)
    fcm_token = serializers.CharField(required=False)

    def signup(self, user_id, display_name, fcm_token):
        if '@' in user_id:
            try:
                validate_email(user_id)
                return user_model.User.objects.email_signup(email=user_id,
                                                            display_name=display_name, fcm_token=fcm_token)
            except Exception as e:
                raise serializers.ValidationError(e.__str__())
        else:
            if PhoneNumber.is_valid(to_python(user_id)):
                return user_model.User.objects.phone_signup(phone=user_id,
                                                            display_name=display_name, fcm_token=fcm_token)
            raise serializers.ValidationError('Invalid Phone Number')


class UserLoginSerializer(serializers.Serializer):
    user_id = serializers.CharField(required=True)
    password = serializers.CharField(required=True)
    fcm_token = serializers.CharField(required=False)


class UpdateProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = user_model.User
        fields = (
            'first_name',
            'last_name',
            'display_name',
            'gender',
            'email',
            'profession',
            'company_name',
            'age',
            'locality',
            'description',
            'quote',
            'short_bio',
            'dob',
            'hobbies',
            'current_location',
            'home_town'
        )


class ViewUpdateProfileSerializer(serializers.Serializer):
    first_name = serializers.CharField(read_only=True)
    last_name = serializers.CharField(read_only=True)
    display_name = serializers.CharField(read_only=True)
    gender = serializers.PrimaryKeyRelatedField(read_only=True)
    email = serializers.EmailField(read_only=True)
    profession = serializers.CharField(read_only=True)
    company_name = serializers.CharField(read_only=True)
    age = serializers.IntegerField(read_only=True)
    locality = serializers.CharField(read_only=True)
    description = serializers.CharField(read_only=True)
    quote = serializers.CharField(read_only=True)


# class UpdateUserProfileSerializer(serializers.ModelSerializer):
#     first_name = serializers.CharField()
#     last_name = serializers.CharField()
#     display_name = serializers.CharField()
#     gender = serializers.IntegerField()
#     email = serializers.EmailField()
#     profession = serializers.CharField()
#     company_name = serializers.CharField()
#     age = serializers.IntegerField()
#     locality = serializers.CharField()
#     description = serializers.CharField()
#     quote = serializers.CharField()
#
#     class Meta:
#         model = user_model.User
#         fields = (
#             'id',
#             'first_name',
#             'last_name',
#             'display_name',
#             'gender',
#             'email'
#             'profession',
#             'company_name',
#             'age',
#             'locality',
#             'description',
#             'quote'
#         )
#
#     def update(self, instance, validated_data):
#         """
#         :param instance:
#         :param validated_data:
#         :return: updated user details
#         """
#         instance.first_name = validated_data.get('first_name')
#         instance.last_name = validated_data.get('last_name')
#         instance.display_name = validated_data.get('display_name')
#         instance.gender_id = validated_data.get('gender')
#         instance.email = validated_data.get('email')
#         instance.profession = validated_data.get('profession')
#         instance.company_name = validated_data.get('company_name')
#         instance.age = validated_data.get('age')
#         instance.locality = validated_data.get('locality')
#         instance.description = validated_data.get('description')
#         instance.quote = validated_data.get('quote')
#         instance.save()
#
#         return instance


class ProfileDetailsSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    first_name = serializers.CharField(read_only=True)
    last_name = serializers.CharField(read_only=True)
    display_name = serializers.CharField(read_only=True)
    gender = serializers.PrimaryKeyRelatedField(read_only=True)
    profession = serializers.CharField(read_only=True)
    company_name = serializers.CharField(read_only=True)
    age = serializers.IntegerField(read_only=True)
    residence = serializers.SerializerMethodField(read_only=True)
    locality = serializers.CharField(read_only=True)
    phone = serializers.CharField(read_only=True)
    email = serializers.EmailField(read_only=True)
    description = serializers.CharField(read_only=True)
    quote = serializers.CharField(read_only=True)
    photo = serializers.SerializerMethodField(read_only=True)
    dob = serializers.DateField(read_only=True)
    hobbies = serializers.CharField(read_only=True)
    short_bio = serializers.CharField(read_only=True)
    current_location = serializers.CharField(read_only=True)
    home_town = serializers.CharField(read_only=True)

    # badge_value = serializers.SerializerMethodField()

    # class Meta:
    #     model = user_model.User
    #     read_only_fields = (
    #         '',
    #         '',
    #         'last_name',
    #         'display_name',
    #         'gender',
    #         'profession',
    #         'company_name',
    #         'age',
    #         'residence',
    #         'locality',
    #         'phone',
    #         'email',
    #         'description',
    #         'quote',
    #         # 'badge_value',
    #         'photo'
    #     )

    def get_residence(self, obj):
        """
        :param obj:
        :return: residence details of user
        """
        if flat_model.FlatTenant.objects.get_active_tenants(tenant_id=obj.id).exists():
            flat_data = flat_model.FlatTenant.objects.get_active_tenants(tenant_id=obj.id)[0]
            res_obj = {}
            if flat_model.Flat.objects.pk_does_exist(id=flat_data.flat.id):
                flat_details = flat_model.Flat.objects.get(id=flat_data.flat.id)
                flat_ser = ResidenceSerializer(flat_details)
                res_obj = flat_ser.data
            return res_obj
        else:
            user_model.User.objects.filter(id=obj.id).exists()
            serializer = PeopleSearchDataSerializer(obj.id)
            return serializer.data

    # def get_badge_value(self, obj):
    #     num = badge_model.TaskRating.objects.get_badges(assigned_to_id=obj.id).__len__()
    #     if num == 0:
    #         User.objects.filter(id=obj.id).exists()
    #         serializer = PeopleSearchDataSerializer(obj.id)
    #         return serializer.data
    #     den = num * 100
    #     total = 0
    #     for n in range(num):
    #         badge_value = badge_model.TaskRating.objects.get_badges(assigned_to=obj.id)
    #         rate_id = badge_value[n].id
    #         badge = badge_model.RatingBadge.objects.get(rating_id=rate_id)
    #         total_sum = badge.badge.badge_value
    #         total += total_sum
    #     percentage = (total / den) * 100
    #     return percentage

    def get_photo(self, obj):
        """
        :param obj:
        :return: serializer to get the photo
        """
        if user_model.ProfilePhoto.objects.filter(user_id=obj.id).exists():
            profile_photo = user_model.ProfilePhoto.objects.get(user_id_id=obj.id)
            photo_ser = ProfilePhotoSerializer(profile_photo)
            return photo_ser.data
        else:
            user_model.User.objects.filter(id=obj.id).exists()
            serializer = PeopleSearchDataSerializer(obj.id)
            return serializer.data


class ProfilePhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = user_model.ProfilePhoto
        fields = '__all__'


class OtpSerializer(serializers.ModelSerializer):
    otp = serializers.IntegerField()

    class Meta:
        model = user_model.User
        fields = (
            'otp'
        )


class PeopleSearchDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = user_model.User
        fields = (
            'display_name',
            'profession'
        )


class PeopleSearchSerializer(serializers.ModelSerializer):
    photo = serializers.SerializerMethodField(required=False, default=null)

    class Meta:
        model = user_model.User
        fields = (
            'id',
            'display_name',
            'profession',
            'photo',
            'id'
        )

    def get_photo(self, obj):
        """
        :param obj:
        :return: serializer to get the photo
        """
        if user_model.ProfilePhoto.objects.filter(user_id=obj.id).exists():
            profile_photo = user_model.ProfilePhoto.objects.get(user_id_id=obj.id)
            photo_ser = ProfilePhotoSerializer(profile_photo)
            return photo_ser.data
        else:
            user_model.User.objects.filter(id=obj.id).exists()
            serializer = PeopleSearchDataSerializer(obj.id)
            return serializer.data


class ResidenceSerializer(serializers.Serializer):
    flat_no = serializers.CharField(read_only=True)
    flat_name = serializers.CharField(read_only=True)
    landmark = serializers.CharField(read_only=True)
    city = serializers.CharField(read_only=True)


class UserInfoSerializer(serializers.ModelSerializer):
    profile_photo = serializers.SerializerMethodField()

    class Meta:
        model = user_model.User
        fields = ('id',
                  'display_name',
                  'profile_photo')

    def get_profile_photo(self, obj):
        if user_model.ProfilePhoto.objects.filter(user_id=obj).exists():
            user_obj = user_model.ProfilePhoto.objects.get(user_id=obj)
            user_photo_data = ProfilePhotoSerializer(user_obj)
            return user_photo_data.data['photo']
        return


class ReturnNullSerializer(serializers.ModelSerializer):
    class Meta:
        model = user_model.User
        fields = (

        )


class UpdatePhoneSerializer(serializers.ModelSerializer):
    class Meta:
        model = user_model.User
        fields = (
            'display_name',
            'phone'
        )


class UpdatePhoneNumberSerializer(serializers.ModelSerializer):
    class Meta:
        model = user_model.User
        fields = (
            'otp',
            'phone'
        )

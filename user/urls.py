from django.urls import path

from user import views

urlpatterns = [
    # user signup url
    path('user-signup/', views.UserSignUp.as_view(), name='user_signup'),
    # send otp url
    path('send-user-otp', views.SentUserOTP.as_view(), name='send_user_otp'),
    # verify otp url
    path('verify-user-otp/', views.VerifyUserOTP.as_view(), name='verify_user_otp'),
    # set password url
    path('set-user-password/', views.SetUserPassword.as_view(), name='set_user_password'),
    # user login url
    path('user-login/', views.UserSignIn.as_view(), name='user_login'),
    # user profile update url
    path('update-user-profile/', views.UpdateUserProfile.as_view(), name='update_user_profile'),
    # user profile get url
    path('profile-details/', views.ProfileDetails.as_view(), name='get_profile_details'),
    # delete the profile photo
    path('delete-profile-details/', views.ProfileDetails.as_view(), name='delete_profile'),
    # upload the profile photo
    path('upload-profile-photo/', views.UploadProfilePhoto.as_view(), name='upload_profile_photo'),
    # get the profile photo
    path('get-the-profile-photo/', views.UploadProfilePhoto.as_view(), name='get_profile_photo'),
    # delete the profile photo
    path('delete-profile-photo/', views.UploadProfilePhoto.as_view(), name='delete_profile'),
    # api to execute forgot password
    path('forgot-password/', views.ForgotPassword.as_view(), name='forgot_password'),
    # verify the otp for forgot password
    path('verify_otp/', views.OtpVerification.as_view(), name='verify_otp'),
    # change password
    path('change_password/', views.ChangeUserPassword.as_view(), name='change_password'),
    # search user api
    path('search_user/', views.PeopleSearch.as_view()),
    # login using google
    path('google_auth/', views.GoogleAuthentication.as_view(), name='google'),  # add path for google authentication
    # login using facebook
    path('facebook_auth/', views.FacebookAuthentication.as_view(), name='facebook'),
    #
    path('update_phone/', views.UpdatePhoneDisplayName.as_view(), name='update_display_name_phone'),
    # path('facebook_authentication/', views.exchange_token(), name='facebook_auth'),
    # user-flat-history
    path('user-flat-history/', views.UserFlats.as_view(), name='user_flats'),
    # update phone number with otp
    path('update_phone_number/', views.UpdateUserPhoneNumber.as_view(), name='update_user_phone_no'),
]

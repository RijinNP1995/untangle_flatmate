from django.contrib import admin

from user import models


@admin.register(models.User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('display_name', 'email', 'phone', 'is_active')
    list_display_links = ('display_name', 'email', 'phone',)


@admin.register(models.Gender)
class GenderAdmin(admin.ModelAdmin):
    list_display = ('type',)


@admin.register(models.ProfilePhoto)
class ProfilePhotoAdmin(admin.ModelAdmin):
    list_display = ('user_id', 'photo')

import facebook
import pyotp
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.hashers import make_password
from django.db import transaction
from django.db.models import Q
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.response import Response

from core import log_data
from core.profile_percentage import profile_percentage, user_profile_percentage
from core.sent_otp import share_otp
from core.valid_user_signup import valid_user_signup
from flat_api import models as flat_model
from flat_api.serializers import FlatDetailsSerializer, TenantHistorySerializer
from flatmates_backend import responses as message
from user import models as model
from user import sent_otp as send_otp
from user import serializers as serializer_


class UserManagementServices:
    """
    User Management Services
    """

    @transaction.atomic
    def user_signup(self, serializer):
        """
        :param serializer:
        :return:
        """
        if serializer.is_valid():
            # user id
            user_id = serializer.data.get('user_id')
            # user display name
            display_name = serializer.data.get('display_name')
            # pass the fcm_token value
            fcm_token = serializer.data.get('fcm_token')
            validate_mail = model.User.objects.filter(email=user_id, valid_user=True)
            validate_phone = model.User.objects.filter(phone=user_id, valid_user=True)
            user_data = model.User.objects.filter(email=user_id)
            user_data2 = model.User.objects.filter(phone=user_id)
            valid_data = user_data | user_data2
            validate_user = validate_mail | validate_phone
            if validate_user:
                return message.get_conflicts_409(user_id)
            elif valid_data:
                return share_otp(user_id)
            # if not validate_user
            else:
                with transaction.atomic():
                    user = serializer.signup(user_id=user_id,
                                             display_name=display_name, fcm_token=fcm_token)
                # fetching or creating user token
                token = Token.objects.get_or_create(user=user)
                user.valid_user = False
                user_data = serializer_.ProfileDetailsSerializer(user)
                user_percentage = user_profile_percentage(user_data)
                log_data.info(f'Sign-up operation completed')
                return Response(
                    {
                        'status': 200,
                        'message': 'success',
                        'user_id': user.id,
                        'data': user.username.__str__(),
                        'token': token[0].key,
                        'user_data': user_data.data,
                        'percentage': user_percentage
                    },
                    status=status.HTTP_200_OK)
        else:
            log_data.error(f'{serializer.errors}')
            return message.invalid_serializer(serializer)

    def sent_otp(self, user):
        """
        Sent user OTP service
        :param user:
        :return:
        """
        if not user:
            log_data.error('invalid user')
            return message.api_failed()
        base32secret = pyotp.random_base32()
        time_otp = pyotp.TOTP(base32secret, interval=1000)
        time_otp = time_otp.now()
        user_data = model.User.objects.get_by_id(pk=user.id)
        user_data.otp = time_otp
        # save the otp into the model
        user.valid_user = False
        user_data.save()
        # check if user has phone number
        if user.phone:
            # sent otp to phone
            log_data.info('Send the one-time-password to the phone number')
            send_otp.send_otp_to_phone(phone=user.phone.national_number,
                                       otp=time_otp)
        else:
            # send otp to email
            log_data.info('Send the one-time-password to the email')
            send_otp.send_otp_to_email(email=user.email, otp=time_otp)
        log_data.info(f'User OTP: {time_otp} sent successfully')
        return message.get_otp(time_otp)

    @transaction.atomic
    def otp_verification(self, user, otp):
        """
        User OTP verification service
        :param user:
        :param otp:
        :return:
        """
        if not user:
            log_data.error('invalid user')
            return message.api_failed()
        # get the phone number
        user_otp = model.User.objects.prefetch_related('otp').get(otp=otp)
        if user_otp:
            # checking whether the user phone is verified or not
            if user_otp.phone and not user_otp.is_phone_verified:
                # setting user phone as verified
                user_otp.is_phone_verified = True
                user_otp.save()
                log_data.info('OTP verification successful')
            # checking whether the user email is verified or not
            if user_otp.email and not user_otp.is_email_verified:
                # setting user email as verified
                user_otp.is_email_verified = True
                user_otp.save()
                log_data.info('OTP verification successful')
            return message.verify_otp()
        log_data.error('OTP verification failed')
        return message.otp_verification_failed()

    @transaction.atomic
    def set_user_password(self, user, password):
        """
        Set user password service
        :param user:
        :param password:
        :return:
        """
        with transaction.atomic():
            if user:
                # checking whether phone/email is verified or not
                if user.phone and user.is_phone_verified or user.email and user.is_email_verified:
                    # setting user password
                    user.set_password(password)
                    valid_user = user.valid_user = True
                    user_id = user.email or user.phone
                    fcm_token = user.fcm_token
                    user.save()
                    if valid_user:
                        log_data.debug('set the password successfully')
                        return valid_user_signup(valid_user, user_id, fcm_token, password)
                    else:
                        return message.invalid_user()
                else:
                    log_data.error('User email/phone is not verified')
                    return message.unauthorized_user()
            log_data.error('User does not exists')
            return message.user_not_exist()

    @transaction.atomic
    def change_password(self, password, otp):
        """
        Set user password service
        :param password:
        :param otp:
        :return:
        """
        with transaction.atomic():
            # checking whether phone/email is verified or not
            user = model.User.objects.get(otp=otp)
            if user:
                # setting user password
                log_data.debug('set the password successfully')
                user.set_password(password)
                valid_user = user.valid_user = True
                user_id = user.email or user.phone
                fcm_token = user.fcm_token
                user.save()
                if valid_user:
                    log_data.debug('change the password successfully')
                    return valid_user_signup(valid_user, user_id, fcm_token, password)
                else:
                    return message.invalid_user()
            else:
                log_data.error('User email/phone is not verified')
                return message.unauthorized_user()

    def user_sign_in(self, serializer):
        """
        User sign in service
        :param serializer:
        :return:
        """
        if not serializer.is_valid():
            return
        user_id = serializer.data.get('user_id')
        password = serializer.data.get('password')
        fcm_token = serializer.data.get('fcm_token')
        active_request = False
        # checking condition for user email
        if '@' in user_id and model.User.objects.filter(email=user_id).exists():
            user = model.User.objects.get(email=user_id)
            in_the_flat = True if flat_model.FlatTenant.objects.get_active_tenants(
                tenant=user) else False
            if not in_the_flat:
                flat_id = None
                flat_ser = serializer_.ReturnNullSerializer(flat_id)
                active_request, flat_id = self.check_user_have_active_request(user=user.id)
            else:
                flat_data = flat_model.FlatTenant.objects.get(tenant=user, is_active=True)
                flat_id = flat_data.flat_id
                flat_obj = flat_model.Flat.objects.get_by_id(pk=flat_id)
                flat_ser = FlatDetailsSerializer(flat_obj)
            # check user password
            if user.check_password(password):
                # generating user token
                token, _ = Token.objects.get_or_create(user=user)
                user.fcm_token = fcm_token
                user.save()
                user_data = serializer_.ProfileDetailsSerializer(user)
                user_percentage = user_profile_percentage(user_data)
                log_data.info('user sign in completed with email id')
                return message.user_sign_in(token, user, in_the_flat, flat_id, user_data, flat_ser, user_percentage,
                                            active_request=active_request)
            else:
                log_data.error('Invalid user password')
                return message.invalid_password()
        # checking for user phone condition
        elif model.User.objects.filter(phone=user_id).exists():
            user = model.User.objects.get(phone=user_id)
            in_the_flat = True if flat_model.FlatTenant.objects.get_active_tenants(tenant=user) else False
            if not in_the_flat:
                flat_id = None
                flat_ser = serializer_.ReturnNullSerializer(flat_id)
            else:
                flat_data = flat_model.FlatTenant.objects.get(tenant=user, is_active=True)
                flat_id = flat_data.flat_id
                flat_obj = flat_model.Flat.objects.get_by_id(pk=flat_id)
                flat_ser = FlatDetailsSerializer(flat_obj)
            # check user password
            if user.check_password(password):
                # generating user token
                token, _ = Token.objects.get_or_create(user=user)
                user.fcm_token = fcm_token
                user.save()
                user_data = serializer_.ProfileDetailsSerializer(user)
                user_percentage = user_profile_percentage(user_data)

                log_data.info('User log in successful with mobile number')
                return message.user_sign_in(token, user, in_the_flat, flat_id, user_data, flat_ser, user_percentage,
                                            active_request=active_request)
            else:
                log_data.error('Invalid user password')
                return message.invalid_password()

        else:
            log_data.error('Please check the user_id')
            return message.check_the_user_id()

    # update the profile details
    def update_user_profile(self, serializer, user):
        """
        update the user detaials
        :param serializer:
        :param user:
        :return:
        """
        # checking whether serializer is valid or not
        if serializer.is_valid():
            # updating user data
            serializer.update(instance=user,
                              validated_data=serializer.validated_data)
            log_data.info('user profile updated successfully ')
            user_serializer = serializer_.ProfileDetailsSerializer(user)
            user_percentage = user_profile_percentage(user_serializer)
            return message.update_user_data(user_serializer, user_percentage=user_percentage)
        else:
            log_data.error(f'{serializer.errors}')
            return message.update_failed(serializer)

    def profile_details(self, user_id):
        """
        get the profile details
        :param user_id:
        :return:
        """
        user_details = model.User.objects.get(id=user_id)
        if user_details:
            user_info = serializer_.ProfileDetailsSerializer(user_details)
            return profile_percentage(user_info)
        else:
            return message.no_data_found()

    def delete_user_data(self, id):
        """
        Delete the user data
        :param id:
        :return:
        """
        if model.User.objects.filter(id=id).exists():
            log_data.warning('please enter the valid user_id to delete the user data')
            user_profile = model.User.objects.get(id=id)
            user_profile.is_delete = True
            user_profile.save()
            log_data.debug('Profile details deleted successfully')
            return message.delete_profile()
        else:
            log_data.error('user id does not exist')
            return message.not_found()

    def upload_photo_service(self, user, serializer, user_id):
        """
        upload the profile photo
        :param user:
        :param serializer:
        :param user_id:
        :return:
        """
        log_data.warning('please enter the valid user_id to upload the profile photo')
        if model.ProfilePhoto.objects.filter(user_id=user_id).exists():
            log_data.warning('please enter the valid user_id to delete the user photo')
            model.ProfilePhoto.objects.get(user_id=user_id).delete()
        if serializer.is_valid():
            serializer.update(instance=user,
                              validated_data=serializer.validated_data)
            serializer.save()
            log_data.debug(f'{serializer.data} upload the file_successfully')
            return message.upload_success()
        else:
            log_data.error(f'{serializer.errors}')
            return message.upload_failed(serializer)

    def get_profile_photo(self, user_id):
        """
        API to get the profile photo
        :param user_id:
        :return:
        """
        log_data.warning('please enter the valid user_id to get the profile photo')
        user_photo = model.ProfilePhoto.objects.get(user_id=user_id)
        serializer = serializer_.ProfilePhotoSerializer(user_photo)
        return message.view_photo(serializer)

    def delete_user_photo(self, id):
        """
        Delete the user profile photo
        :param id:
        :return:
        """
        if model.ProfilePhoto.objects.filter(user_id=id).exists():
            log_data.warning('please enter the valid user_id to delete the user photo')
            model.ProfilePhoto.objects.get(user_id=id).delete()
            log_data.debug('Profile details deleted successfully')
            return message.delete_photo()
        else:
            log_data.error('user id does not exist')
            return message.not_found()

    # send otp to the email or phone for particular user
    def send_forgot_password(self, user_id):
        """
        send otp to the email or phone for particular user
        :param user_id:
        :return:
        """
        # get the phone number
        user_phone = model.User.objects.filter(Q(phone=user_id))
        # get the mail
        user_mail = model.User.objects.filter(Q(email=user_id))
        user_data = user_phone | user_mail
        if not user_data:
            log_data.error('user phone/email does not exist')
            return message.api_failed()
        # create the otp to send
        base32secret = pyotp.random_base32()
        time_otp = pyotp.TOTP(base32secret, interval=1000)
        time_otp = time_otp.now()
        user_verify = model.User.objects.get(Q(email=user_id) | Q(phone=user_id))
        user_verify.otp = time_otp
        user_verify.save()
        # check if user has phone number
        log_data.debug(f'data inserted successfully')
        if user_phone:
            # sent otp to phone
            send_otp.send_otp_to_phone(phone=user_id,
                                       otp=time_otp)
            log_data.info('one time password generated successfully and send to phone')
            return message.get_otp(time_otp)
        # check if the user has email id
        else:
            # send otp to email
            send_otp.send_otp_to_email(email=user_id, otp=time_otp)
        log_data.info('one time password generated successfully and send to email')
        return message.get_otp(time_otp)

    # verify the otp for the forgot password operation
    def verify_otp_service(self, otp):
        """
        Verify the OTP for the user
        :param otp:
        :return:
        """
        log_data.warning('Enter the valid otp')
        user_otp = model.User.objects.get(otp=otp)
        if user_otp:
            # checking whether the user phone is verified or not
            if user_otp.phone and not user_otp.is_phone_verified:
                # setting user phone as verified
                user_otp.is_phone_verified = True
                user_otp.save()
                log_data.info('OTP verification successful')
            # checking whether the user email is verified or not
            if user_otp.email and not user_otp.is_email_verified:
                # setting user email as verified
                user_otp.is_email_verified = True
                user_otp.save()
                log_data.info('OTP verification successful')
            return message.verify_otp()
        log_data.error('OTP verification failed')
        return message.otp_verification_failed()

    # search with the user display name or first_name
    def get_people_search(self, search_query):
        """
        Get the search result according to the name of user
        :param search_query:
        :return:
        """
        if search_query != '':
            # search according to first_name and display name
            search = (Q(first_name__icontains=search_query) | Q(
                display_name__icontains=search_query))
            search_result = model.User.objects.filter(search,
                                                      is_delete=False,
                                                      valid_user=True).exclude(flattenant__is_active=True)
            queryset = search_result
        else:
            queryset = model.User.objects.get_by_mail(is_delete=False,
                                                      valid_user=True).exclude(flattenant__is_active=True)
        people_count = queryset.count()
        serializer = serializer_.PeopleSearchSerializer(queryset, many=True)
        log_data.info('people search result fetched successfully')
        return message.search_result(people_count, serializer)

    def google_auth_service(self, data, fcm_token):
        """
        Perform the google authentication
        :param data:
        :return:
        """
        if 'error' in data:
            log_data.error('wrong google token / this google token is already expired.')
            content = {'message': 'wrong google token / this google token is already expired.'}
            return Response(content)

        # create user if not exist
        try:
            user = model.User.objects.get(email=data['email'])
            log_data.warning('user already exist with same credentials')
        except model.User.DoesNotExist:
            user = model.User()
            user.username = data['email']
            # provider random default password
            user.password = make_password(BaseUserManager().make_random_password())
            user.email = data['email']
            user.display_name = user.email.split('@')[0]
            user.valid_user = True
            user.save()

            log_data.info('generate the user with his/her google account')
        active_request = False
        in_the_flat = True if flat_model.FlatTenant.objects.get_active_tenants(tenant=user) else False
        if not in_the_flat:
            flat_id = None
            flat_ser = serializer_.ReturnNullSerializer(flat_id)
            active_request, flat_id = self.check_user_have_active_request(user=user.id)
        else:
            flat_data = flat_model.FlatTenant.objects.get(tenant=user, is_active=True)
            flat_id = flat_data.flat_id
            flat_obj = flat_model.Flat.objects.get_by_id(pk=flat_id)
            flat_ser = FlatDetailsSerializer(flat_obj)
        token = Token.objects.get_or_create(user=user)
        user.fcm_token = fcm_token
        user.save()
        user_data = serializer_.ProfileDetailsSerializer(user)
        user_percentage = user_profile_percentage(user_data)
        log_data.info('user created successfully')
        return message.google_auth_success(user, token, in_the_flat, flat_id, user_data, flat_ser, user_percentage,
                                           active_request=active_request)

    @csrf_exempt
    def facebook_auth_service(self, access_token, fcm_token, email_id, new_user):
        """
        :param access_token:
        :param fcm_token:
        :param new_user:
        :return:
        """

        try:
            graph = facebook.GraphAPI(access_token=access_token)
            user_info = graph.get_object(
                id='me',
                fields='id, email, first_name,last_name')
        except facebook.GraphAPIError:
            log_data.error('invalid credentials of facebook')
            return JsonResponse({'error': 'Invalid data'}, safe=False)

        try:
            user = model.User.objects.get(username=user_info.get('id'))

        except model.User.DoesNotExist:
            password = model.User.objects.make_random_password()
            user = model.User(
                first_name=user_info.get('first_name'),
                display_name=user_info.get('first_name'),
                username=str(user_info.get('id')),
                email=email_id,
                is_active=1)
            user.set_password(password)
            user.valid_user = True
            user.save()
            new_user = True

        token = Token.objects.get_or_create(user=user)
        user.fcm_token = fcm_token
        user.save()
        user_data = serializer_.ProfileDetailsSerializer(user)
        user_percentage = user_profile_percentage(user_data)
        in_the_flat = True if flat_model.FlatTenant.objects.get_active_tenants(tenant=user) else False
        active_request = False
        if not in_the_flat:
            flat_id = None
            flat_ser = serializer_.ReturnNullSerializer(flat_id)
            active_request, flat_id = self.check_user_have_active_request(user=user.id)
        else:
            flat_data = flat_model.FlatTenant.objects.get(tenant=user, is_active=True)
            flat_id = flat_data.flat_id
            flat_obj = flat_model.Flat.objects.get_by_id(pk=flat_id)
            flat_ser = FlatDetailsSerializer(flat_obj)
        if token:
            log_data.info('facebook login successful')
            return message.fac_auth_success(token, new_user, user, in_the_flat, flat_id, user_data, flat_ser,
                                            user_percentage, active_request=active_request)
        else:
            log_data.error('Invalid data')
            return Response({'error': 'Invalid data'})

    @transaction.atomic
    def update_display_and_phone(self, data, user, file):
        """
        :param data:
        :param user:
        :param file:
        :return:
        """

        # updating user data
        if not user:
            log_data.error('user does not exist')
            return message.user_not_exist()
        user_data = model.User.objects.get(id=data.get('id'), is_delete=False)
        serializer = serializer_.UpdatePhoneSerializer(user_data, data=data)
        if serializer.is_valid():
            phone_no = data.get('phone')
            phone_exist = model.User.objects.filter(phone=phone_no)
            if not phone_exist:
                user_obj = serializer.update(instance=user,
                                             validated_data=serializer.validated_data)
                if file:
                    if model.ProfilePhoto.objects.filter(user_id=user_obj.id).exists():
                        model.ProfilePhoto.objects.get(user_id=user_obj.id).delete()
                    profile_photo = {'user_id': user_obj.id, 'photo': data.get('photo')}
                    profile_photo_ser = serializer_.ProfilePhotoSerializer(data=profile_photo)
                    if profile_photo_ser.is_valid(raise_exception=True):
                        profile_photo_ser.save()
                return message.update_success()
            else:
                return message.duplicate_entry()

        else:
            log_data.error(f'{serializer.errors}')
            return message.update_failed(serializer)

    def user_flat_history(self, user):
        if user:
            user_flat_list = flat_model.FlatTenant.objects.filter(tenant=user)
            user_flat_list_ser = TenantHistorySerializer(user_flat_list, many=True)
            return message.fetch_data_response(model_info='User history', data=user_flat_list_ser.data)
        log_data.error(f'user does not exist')
        return message.invalid_user()

    @transaction.atomic
    def update_user_phone_no(self, data, user):
        """
        :param data: contains otp and phone number
        :param user: logged in user
        :return:
        """
        # updating user data
        if user.is_anonymous:
            log_data.error('user does not exist')
            return message.user_not_exist()
        user_data = model.User.objects.get(id=user.id)
        if user_data.otp == int(data.get('otp')):
            serializer = serializer_.UpdatePhoneNumberSerializer(data=data)
            if serializer.is_valid(raise_exception=True):
                user_obj = serializer.update(instance=user,
                                             validated_data=serializer.validated_data)
                return message.update_success(data=serializer_.UpdatePhoneNumberSerializer(user_obj).data)

            else:
                log_data.error(f'{serializer.errors}')
                return message.update_failed(serializer)
        else:
            log_data.error('OTP verification failed')
            return message.otp_verification_failed()

    def check_user_have_active_request(self, user):
        """
        check user has already requested to join flat
        :param user: user_id
        :return: boolean value
        """
        active_request = False
        flat_id = None
        if flat_model.FlatmateCommunity.objects.get_active_pending_request(seeker__id=user,
                                                                           status='Pending',
                                                                           is_delete=False).exists():
            active_request = True
            flat_id = flat_model.FlatmateCommunity.objects.get_active_pending_request(seeker__id=user,
                                                                                      status='Pending',
                                                                                      is_delete=False)[0].flat.id

        return active_request, flat_id

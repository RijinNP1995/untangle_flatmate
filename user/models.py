import pyotp
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField

from master.constants import UserConstants
from user.managers import UserManager


class Gender(models.Model):
    type = models.CharField(_('gender'),
                            choices=UserConstants.GENDER_TYPES.value,
                            blank=False, null=False,
                            max_length=15)


class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(_('username'), max_length=35, blank=True,
                                unique=True)
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)
    display_name = models.CharField(_('display name'), max_length=20,
                                    blank=True)
    gender = models.ForeignKey(Gender, blank=True, null=True,
                               on_delete=models.DO_NOTHING)
    profession = models.CharField(_('profession'), max_length=160, blank=True)
    company_name = models.CharField(_('company name'), max_length=160,
                                    blank=True)
    age = models.IntegerField(_('age'), blank=True, null=True,
                              validators=[MinValueValidator(18),
                                          MaxValueValidator(100)])
    locality = models.CharField(_('locality'), blank=True, null=True, max_length=120)
    phone = PhoneNumberField(blank=True, null=True, unique=False)
    description = models.TextField(blank=True, null=True)
    quote = models.TextField(blank=True, null=True)
    email = models.EmailField(_('email address'), blank=True, null=True,
                              unique=True)
    otp = models.IntegerField(_('one_time_password'), blank=True, null=True)
    date_joined = models.DateTimeField(_('date joined'), auto_now_add=True)
    is_email_verified = models.BooleanField(_('email verification status'),
                                            default=False)
    is_phone_verified = models.BooleanField(_('phone verification status'),
                                            default=False)
    date_modified = models.DateTimeField(_('date modified'), blank=True,
                                         null=True)
    key = models.CharField(max_length=100, unique=True, blank=True, default='')
    fcm_token = models.TextField(_('FCM Token'), null=True, blank=True, default=None)
    dob = models.DateField(_('DOB'), blank=True, null=True, default=None)
    short_bio = models.TextField(_('Short Bio'), null=True, blank=True, default=None)
    hobbies = models.TextField(_('Hobbies'), null=True, blank=True, default=None)
    current_location = models.CharField(_('current city'), max_length=400, null=True, blank=True, default=None)
    home_town = models.CharField(_('home town'), max_length=400, null=True, blank=True, default=None)
    valid_user = models.BooleanField(_('valid_user'), default=False)
    is_staff = models.BooleanField(
        _('Staff status'),
        default=False,
        help_text=_(
            'Designates whether the user can log into this admin site.'),
    )
    is_superuser = models.BooleanField(
        _('Admin status'),
        default=False,
        help_text=_(
            'Designates that this user has all permissions without '
            'explicitly assigning them.'
        ),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting account.'
        ),
    )
    is_delete = models.BooleanField(_('delete'), default=False)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()

    class Meta:
        db_table = 'USER'
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def __str__(self):
        return f"{self.display_name} :: {self.username}"

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def set_username(self):
        if self.email:
            username = self.email.split('@')[0]
        else:
            username = self.phone.national_number
        return username

    def authenticate_otp(self, otp):
        """
        This method authenticates the given otp
        :param otp:
        :return:
        """
        try:
            provided_otp = int(otp)
        except:
            return False
        # Here we are using Time Based OTP. The interval is 60 seconds.
        # otp must be provided within this interval or it's invalid
        time_otp = pyotp.TOTP(self.key, interval=1000)
        return time_otp.verify(provided_otp)


class ProfilePhoto(models.Model):
    user_id = models.OneToOneField(User, on_delete=models.CASCADE)
    photo = models.ImageField(upload_to='profile_image')

from datetime import datetime

from django.test import TestCase

from user import models as user_model


class ModelTestCase(TestCase):
    """This class defines the test suite for the user model."""

    def setUp(self):
        """Define the test client and other test variables."""
        self.username = 'UserName'
        self.first_name = 'First_name'
        self.last_name = 'Last_name'
        self.display_name = 'Display_name'
        self.gender = user_model.Gender.objects.create(type='male')
        self.profession = 'Test Profession'
        self.company_name = 'Test Company'
        self.age = 12
        self.locality = 'Kochi'
        self.phone = '0987654321'
        self.description = '#This is a test description'
        self.quote = 'This is the quote'
        self.email = 'test@email.com'
        self.date_joined = datetime.now()
        self.is_email_verified = False
        self.is_phone_verified = False
        self.valid_user = False
        self.user = user_model.User(username=self.username, first_name=self.first_name,
                                    last_name=self.last_name, display_name=self.display_name,
                                    gender=self.gender,
                                    profession=self.profession, company_name=self.company_name,
                                    age=self.age,
                                    locality=self.locality, phone=self.phone,
                                    description=self.description,
                                    email=self.email,
                                    date_joined=self.date_joined,
                                    is_email_verified=self.is_email_verified,
                                    is_phone_verified=self.is_phone_verified, valid_user=self.valid_user)

    def test_model_can_create_a_user(self):
        old_count = user_model.User.objects.count()
        self.user.save()
        new_count = user_model.User.objects.count()
        self.assertNotEqual(old_count, new_count)

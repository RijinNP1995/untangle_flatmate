import json

from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from user import models as user_model


class TestUserSignUp(TestCase):
    def setUp(self):
        self.client = APIClient()

        self.valid_phone_payload = {
            "user_id": "+919895204120",
            "display_name": "Arjun"
        }

        self.valid_email_payload = {
            "user_id": "testmail@gmail.com",
            "display_name": "Arjun"
        }

        self.invalid_phone_payload = {
            "user_id": "+9195677",
            "display_name": "Arjun"
        }

        self.invalid_email_payload = {
            "user_id": "testmail@gmailcom",
            "display_name": "Arjun"
        }

    def test_user_valid_phone_signup(self):
        """
        :return: Pass the valid user phonenumber to signup
        """
        response = self.client.post(
            reverse('user_signup'),
            data=json.dumps(self.valid_phone_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['message'], 'success')

    def test_user_valid_email_signup(self):
        """
        :return: Pass the valid user email id to signup
        """
        response = self.client.post(
            reverse('user_signup'),
            data=json.dumps(self.valid_email_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['message'], 'success')

    def test_user_invalid_phone_signup(self):
        """
        :return: Pass a invalid phonenumber to signup
        """
        response = self.client.post(
            reverse('user_signup'),
            data=json.dumps(self.invalid_phone_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['error'], '[ErrorDetail(string=\'Invalid Phone Number\', code=\'invalid\')]')

    def test_user_invalid_email_signup(self):
        """
        :return: Pass a invalid email id to signup
        """
        response = self.client.post(
            reverse('user_signup'),
            data=json.dumps(self.invalid_email_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['error'],
                         '[ErrorDetail(string="[\'Enter a valid email address.\']", code=\'invalid\')]')


class TestSetUserPassword(TestCase):
    def setUp(self):
        self.client = APIClient()

        self.user1 = user_model.User(phone='+919895203121', is_phone_verified=True)
        self.user1.save()
        self.token1 = Token.objects.get_or_create(user=self.user1)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token1[0].key)

        self.user2 = user_model.User(email='testuser@example.com', is_email_verified=True)
        self.user2.save()
        self.token2 = Token.objects.get_or_create(user=self.user2)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token2[0].key)

        self.valid_password_payload_with_phone = {
            "password": "user@1234"
        }

        self.valid_password_payload_with_email = {
            "password": "user@1234"
        }

    def test_user_valid_set_password_with_phone(self):
        """
        :return: set a valid password with the phone number
        """
        response = self.client.post(
            reverse('set_user_password'),
            data=json.dumps(self.valid_password_payload_with_phone),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['message'], 'success')

    def test_user_valid_set_password_with_email(self):
        """
        :return: set a valid password with the mail id
        """
        response = self.client.post(
            reverse('set_user_password'),
            data=json.dumps(self.valid_password_payload_with_email),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['message'], 'success')


class TestUserLogin(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user1 = user_model.User(phone='+919895203121', is_phone_verified=True)
        self.user1.save()
        self.user2 = user_model.User(email='rijinswaminathan@gmail.com', is_email_verified=True)
        self.user2.save()
        self.token1 = Token.objects.get_or_create(user=self.user1)
        self.token2 = Token.objects.get_or_create(user=self.user2)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token1[0].key)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token2[0].key)
        self.user1.set_password('user@1234')
        self.user1.save()
        self.user2.set_password('user@1234')
        self.user2.save()

        self.valid_user_sign_in_payload = {
            "user_id": '+919895203121',
            "password": "user@1234"
        }
        self.valid_user_sign_in_payload_email = {
            "user_id": 'rijinswaminathan@gmail.com',
            "password": "user@1234"
        }
        # passing Wrong Password
        self.invalid_user_sign_in_payload1 = {
            "user_id": '+919895203121',
            "password": "user@12345"
        }
        # passing invalid mail id
        self.invalid_user_sign_in_payload_email = {
            "user_id": 'rijinswaminathan@gmailcom',
            "password": "user@1234"
        }
        # passing invalid password
        self.invalid_user_sign_in_payload_email01 = {
            "user_id": 'rijinswaminathan@gmail.com',
            "password": "user@1234eer"
        }

    def test_valid_user_sign_in_payload(self):
        """
        :return: Pass the valid login credentials with phone
        """
        response = self.client.post(
            reverse('user_login'),
            data=json.dumps(self.valid_user_sign_in_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['message'], 'success')

    def test_valid_user_sign_in_payload_mail(self):
        """
        :return: Pass the valid login credentials with the valid mail id
        """
        response = self.client.post(
            reverse('user_login'),
            data=json.dumps(self.valid_user_sign_in_payload_email),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['message'], 'success')

    def test_invalid_user_sign_in_payload(self):
        """
        :return: Pass the invalid Phone number
        """
        response = self.client.post(
            reverse('user_login'),
            data=json.dumps(self.invalid_user_sign_in_payload1),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['error'], 'Invalid user password')

    def test_invalid_user_sign_in_payload_email(self):
        """
        :return: Pass the invalid Email id
        """
        response = self.client.post(
            reverse('user_login'),
            data=json.dumps(self.invalid_user_sign_in_payload_email),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['message'], 'Please check your user_id')

    def test_invalid_user_sign_in_payload_email_02(self):
        """
        :return: Pass the invalid password
        """
        response = self.client.post(
            reverse('user_login'),
            data=json.dumps(self.invalid_user_sign_in_payload_email01),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['error'], 'Invalid user password')


class TestUpdateProfile(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.gender = user_model.Gender(1)
        self.locality = 'Kochi'
        self.gender.save()
        self.user = user_model.User(
            first_name='Rijin',
            last_name='NP',
            display_name='Rijin',
            gender=self.gender,
            phone='+919895203121',
            profession='Software engineer',
            company_name='Untangle',
            age=24,
            locality=self.locality,
            description='#test description',
            quote='test_quote'
        )
        self.user.save()
        self.token = Token.objects.get_or_create(user=self.user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token[0].key)
        self.valid_profile_details = {
            "first_name": "Rijin",
            "last_name": "NP",
            "display_name": "Rijin NP",
            "gender": 1,
            "profession": "Software Engineer",
            "company_name": "Untangle",
            "age": 24,
            "locality": "Kochi",
            "description": "Test Description",
            "quote": "test_quote"

        }
        self.invalid_gender = {
            "first_name": "Rijin",
            "last_name": "NP",
            "display_name": "Rijin NP",
            "gender": 'male',
            "profession": "Software Engineeer",
            "company_name": "Untangle",
            "age": 24,
            "locality": "Kochi",
            "description": "Test Discription",
            "quote": "test_quote"
        }

    def test_valid_user_profile_payload(self):
        """
        :return: pass the valid payload to pass the profile details
        """
        response = self.client.post(
            reverse('update_user_profile'),
            data=json.dumps(self.valid_profile_details),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['message'], 'success')

    def test_invalid_user_gender(self):
        """
        :return: Pass the invalid gender id to the payload
        """
        response = self.client.post(
            reverse('update_user_profile'),
            data=json.dumps(self.invalid_gender),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['error']),
                         '{\'gender\': [ErrorDetail(string=\'Incorrect type. Expected pk value, received str.\', code=\'incorrect_type\')]}')


class TestFetchProfileDetails(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.gender = user_model.Gender(1)
        self.locality = "Kochi"
        self.gender.save()
        self.user = user_model.User(
            first_name='Rijin',
            last_name='NP',
            display_name='Rijin',
            gender=self.gender,
            phone='+919895203121',
            profession='Software engineer',
            email='102408504735617@flatmates.com',
            company_name='Untangle',
            age=24,
            locality=self.locality,
            description='test description',
            quote='test_quote'
        )
        self.user.save()
        self.valid_user_id = {
            "user_id": 1
        }

        self.invalid_user_id = {
            "user_id": "364547363646473"
        }

    def test_fetch_user_profile_detail_valid(self):
        """
        :return: Pass the user id to fetch the profile details
        """
        response = self.client.post(
            reverse('get_profile_details'),
            data=json.dumps(self.valid_user_id),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['message'], 'success')

    def test_fetch_user_profile_detail_invalid_id(self):
        """
        :return: Pass the invalid user id to fetch the profile details
        """
        response = self.client.post(
            reverse('get_profile_details'),
            data=json.dumps(self.invalid_user_id),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['error'], 'User matching query does not exist.')


class TestUploadProfilePhoto(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.gender = user_model.Gender(1)
        self.locality = 'Kochi'
        self.gender.save()
        self.user = user_model.User(
            first_name='Rijin',
            last_name='NP',
            display_name='Rijin',
            gender=self.gender,
            phone='+919895203121',
            profession='Software engineer',
            email='102408504735617@flatmates.com',
            company_name='Untangle',
            age=24,
            locality=self.locality,
            description='test description',
            quote='test quote'
        )
        self.user.save()
        self.token = Token.objects.get_or_create(user=self.user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token[0].key)

        self.invalid_photo_upload = {
            "user_id": 1,
            "photo": "static/media/profile_image/facebook.png"
        }

    def test_invalid_user_profile_photo(self):
        """
        :return: Pass the valid payload to upload the profile photo
        """
        response = self.client.post(
            reverse('upload_profile_photo'),
            data=json.dumps(self.invalid_photo_upload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['error']),
                         '{\'photo\': [ErrorDetail(string=\'The submitted data was not a file. Check the encoding type on the form.\', code=\'invalid\')]}')


class TestForgotPassword(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.gender = user_model.Gender(1)
        self.locality = 'Kochi'
        self.gender.save()
        self.user = user_model.User(
            first_name='Rijin',
            last_name='NP',
            display_name='Rijin',
            gender=self.gender,
            phone='+919895203121',
            profession='Software engineer',
            email='102408504735617@flatmates.com',
            company_name='Untangle',
            age=24,
            locality=self.locality,
            description='test description',
            quote='test quote'
        )
        self.user.save()
        self.valid_user_id_email = {
            "user_id": "102408504735617@flatmates.com"
        }

        self.valid_user_id_phone = {
            "user_id": "+919895203121"
        }
        self.invalid_user_id_phone = {
            "user_id": "+91989ghggs5203121"
        }
        self.invalid_user_id_email = {
            "user_id": "102408504735617.flatmates.com"
        }

    def test_valid_forgot_password_email(self):
        """
        :return: Pass the valid payload to get the one time password with the email
        """
        response = self.client.post(
            reverse('forgot_password'),
            data=json.dumps(self.valid_user_id_email),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_valid_forgot_password_phone(self):
        """
        :return: Pass the valid payload get the otp with the phone
        """
        response = self.client.post(
            reverse('forgot_password'),
            data=json.dumps(self.valid_user_id_phone),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_invalid_forgot_password_email(self):
        """
        :return: Pass the invalid email id to the payload
        """
        response = self.client.post(
            reverse('forgot_password'),
            data=json.dumps(self.invalid_user_id_email),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['error'], 'Invalid user')

    def test_invalid_forgot_password_phone(self):
        """
        :return: Pass the invalid phone number to the payload
        """
        response = self.client.post(
            reverse('forgot_password'),
            data=json.dumps(self.invalid_user_id_phone),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['error'], 'Invalid user')


class TestOTP(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.gender = user_model.Gender(1)
        self.locality = 'Kochi'
        self.gender.save()
        self.user = user_model.User(
            first_name='Rijin',
            last_name='NP',
            display_name='Rijin',
            gender=self.gender,
            phone='+919895203121',
            profession='Software engineer',
            email='102408504735617@flatmates.com',
            otp='123456',
            company_name='Untangle',
            age=24,
            locality=self.locality,
            description='test description',
            quote='test quote'
        )
        self.user.save()
        self.valid_otp = {
            "otp": "123456"
        }
        self.invalid_otp_format = {
            "otp": "tre7wb"
        }
        self.invalid_otp = {
            "otp": "098765"
        }

    def test_valid_otp(self):
        """
        :return: Pass the valid onetime password
        """
        response = self.client.post(
            reverse('verify_otp'),
            data=json.dumps(self.valid_otp),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_202_ACCEPTED)
        self.assertEqual(response.data['message'], 'OTP verification successful')

    def test_invalid_otp_format(self):
        """
        :return: Pass the invalid otp format
        """
        response = self.client.post(
            reverse('verify_otp'),
            data=json.dumps(self.invalid_otp_format),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['error'], 'Field \'otp\' expected a number but got \'tre7wb\'.')

    def test_invalid_otp(self):
        """
        :return: Pass the invalid otp
        """
        response = self.client.post(
            reverse('verify_otp'),
            data=json.dumps(self.invalid_otp),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['error'], 'User matching query does not exist.')

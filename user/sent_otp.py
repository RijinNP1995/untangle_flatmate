import requests
from django.core.mail import send_mail

from flatmates_backend import settings


def send_otp_to_phone(phone, otp):
    """
    :param phone:
    :param otp:
    """

    url = 'https://www.fast2sms.com/dev/bulk'
    querystring = {"authorization": "xleKf1pMv4iXJcyuV3WzTm8PGBnbZAEgHhSto5DQ9kLRwFqI2a7LnhU46gMlbKrR0WuXIOqtZBGQwpV3",
                   "sender_id": "FSTSMS", "language": "english", "route": "qt", "numbers": {phone}, "message": "28083",
                   "variables": "{BB}", "variables_values": otp}
    headers = {
        'cache-control': "no-cache"
    }

    response = requests.request("GET", url, headers=headers, params=querystring)

    print(response.text)


def send_otp_to_email(email, otp):
    """
    :param email:
    :param otp:
    """
    from_email = settings.EMAIL_HOST_USER
    subject = 'Flatmates: OTP'
    message = f'Your Flatmates OTP is: {otp}'
    send_mail(subject=subject, message=message, from_email=from_email, recipient_list=[email])

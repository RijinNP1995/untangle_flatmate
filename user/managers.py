from django.contrib.auth.base_user import BaseUserManager

from flatmates_backend.base_manager import BaseManager


class UserManager(BaseUserManager,
                  BaseManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email number and password.
        """
        if not email:
            raise ValueError('The given email number must be set')
        # email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email=None, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """
        :param email:
        :param password:
        :param extra_fields:
        :return:
        """
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)

    def email_signup(self, email, display_name, fcm_token):
        """
        :param email:
        :param display_name:
        :return:
        """
        # user signup using email
        user = self.model(email=email, display_name=display_name, fcm_token=fcm_token, is_email_verified=True)
        user.save(using=self._db)
        return user

    def phone_signup(self, phone, display_name, fcm_token):
        """
        :param phone:
        :param display_name:
        :return:
        """
        # user signup using phone
        user = self.model(phone=phone, display_name=display_name, fcm_token=fcm_token)
        user.save(using=self._db)
        return user

    def get_by_mail(self, **filter_args):
        """
        :param filter_args:
        :return:
        """
        return self.get_all().filter(**filter_args)

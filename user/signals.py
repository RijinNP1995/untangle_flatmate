import pyotp
from django.db.models.signals import pre_save
from django.dispatch import receiver

from user.models import User


@receiver(pre_save, sender=User)
def create_username(sender, instance, **kwargs):
    """
    creates username for each users, whenever a user instance is created
    """
    if not instance.username or instance.username == '':
        # checking whether the user is a super user or not
        if instance.is_superuser:
            # assigns email as username
            instance.username = instance.email
        else:
            # generates username
            instance.username = instance.set_username()


def generate_key():
    """ User otp key generator """
    key = pyotp.random_base32()
    if is_unique(key):
        return key
    generate_key()


def is_unique(key):
    try:
        # checking whether the generated key already exists or not
        User.objects.get(key=key)
    except User.DoesNotExist:
        return True
    return False


@receiver(pre_save, sender=User)
def create_key(sender, instance, **kwargs):
    """
    Creates unique keys for user instances that dose'nt have a key
    """
    if not instance.key:
        instance.key = generate_key()

from django.contrib import admin

# Register your models here.
from task_badge.models import TaskRating

admin.site.register(TaskRating)
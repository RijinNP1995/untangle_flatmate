from django.db import transaction

from core import log_data
from flatmates_backend import responses as message
from task import models as task_model
from task_badge import models as rating_model
from task_badge import serializers as rating_ser


class TaskBadgeService:
    @transaction.atomic
    def add_badge(self, user, data):
        """
            :param user:
            :param data:
            :return: method to add the badge for tasks
        """
        with transaction.atomic():
            if not user:
                log_data.error(f'user does not exist')
                return message.user_not_exist()

            assigned_id = data.get('assigned_to')
            log_data.warning(f'enter the valid assigned_id and task status value')
            task_exist = task_model.AssignTask.objects.filter(assigned_to=assigned_id, task=data.get('task')).exists()
            if not task_exist:
                return message.invalid_id()
            log_data.warning(f'you cant rate your own task')
            if assigned_id == user.id:
                return message.cant_rate()
            log_data.warning(f'you can only rate a task if the task is completed')
            serializer = rating_ser.RateBadgeSerializer(data=data)
            if serializer.is_valid(raise_exception=True):
                rate_obj = serializer.save(rated_by=user, assigned_to_id=assigned_id)
                log_data.warning(f'enter the valid task status')
                if data.get('task_status'):
                    task_status_ids = {'rating': rate_obj.id, 'task_status': data.get('task_status')}
                    task_status_ser = rating_ser.RatingStatusSerializer(data=task_status_ids, many=False)
                    if task_status_ser.is_valid(raise_exception=True):
                        task_status_ser.save()
                if data.get('task'):
                    task_data = {'rating': rate_obj.id, 'task': data.get('task')}
                    task_data_ser = rating_ser.TasksSerializer(data=task_data, many=False)
                    if task_data_ser.is_valid(raise_exception=True):
                        task_data_ser.save()
                log_data.warning(f'enter the valid badge_id')
                if data.get('badge'):
                    task_badges = {'rating': rate_obj.id, 'badge': data.get('badge')}
                    task_badge_ser = rating_ser.RatingBadgeSerializer(data=task_badges, many=False)
                    if task_badge_ser.is_valid(raise_exception=True):
                        task_badge_ser.save()
                        log_data.info(f'add the badge for the particular task successfully')
                if data.get('assigned_to'):
                    task_assign = {'rating': rate_obj.id, 'assign_to': data.get('assigned_to')}
                    assign_badge_ser = rating_ser.AssignedSerializer(data=task_assign, many=False)
                    if assign_badge_ser.is_valid(raise_exception=True):
                        assign_badge_ser.save()
                        log_data.info(f'assigned to data inserted successfully')
                if user.id:
                    task_rated_by = {'rating': rate_obj.id, 'rated_by': user.id}
                    rated_by_ser = rating_ser.RatedBySerializer(data=task_rated_by, many=False)
                    if rated_by_ser.is_valid(raise_exception=True):
                        rated_by_ser.save()
                        log_data.info(f'rated by data inserted successfully')
                        return message.give_badge()
            else:
                log_data.error(f'{serializer.errors}')
                return message.invalid_serializer(serializer)

    @transaction.atomic()
    def update_badge(self, user, data):
        """
        :param user:
        :param data:
        :return:
        """
        if not user:
            log_data.error(f'user does not exist')
            return message.user_not_exist()

        assigned_id = data.get('assigned_to')
        log_data.warning(f'enter the valid assigned_id and task id')
        task_exist = task_model.AssignTask.objects.filter(assigned_to=assigned_id, task=data.get('task')).exists()
        if not task_exist:
            return message.invalid_id()
        log_data.warning(f'you cant rate your own task')
        if assigned_id == user.id:
            return message.cant_rate()
        serializer = rating_ser.UpdateRateBadgeSerializer(data=data)
        if serializer.is_valid(raise_exception=True):
            rate_obj = serializer.update(
                instance=rating_model.TaskRating.objects.get(pk=data.get('id')),
                validated_data=serializer.validated_data)
            rate_obj.rated_by = user
            rate_obj.save()
            # if rating_model.RatingStatus.objects.get(rating_id=rate_obj):
            #     rating_model.RatingStatus.objects.get(rating_id=rate_obj).delete()
            # if data.get('task_status'):
            #     task_status_ids = {'rating': rate_obj.id, 'task_status': data.get('task_status')}
            #     task_status_ser = rating_ser.RatingStatusSerializer(data=task_status_ids, many=False)
            #     if task_status_ser.is_valid(raise_exception=True):
            #         task_status_ser.save()
            if rating_model.TaskData.objects.get(rating_id=rate_obj):
                rating_model.TaskData.objects.get(rating_id=rate_obj).delete()
            if data.get('task'):
                task_data = {'rating': rate_obj.id, 'task': data.get('task')}
                task_data_ser = rating_ser.TasksSerializer(data=task_data, many=False)
                if task_data_ser.is_valid(raise_exception=True):
                    task_data_ser.save()
            if rating_model.AssignTo.objects.get(rating_id=rate_obj):
                rating_model.AssignTo.objects.get(rating_id=rate_obj).delete()
            if data.get('assigned_to'):
                task_assign = {'rating': rate_obj.id, 'assign_to': data.get('assigned_to')}
                assign_badge_ser = rating_ser.AssignedSerializer(data=task_assign, many=False)
                if assign_badge_ser.is_valid(raise_exception=True):
                    assign_badge_ser.save()
                    log_data.info(f'assigned to data inserted successfully')
            if rating_model.RatingBadge.objects.get(rating_id=rate_obj):
                rating_model.RatingBadge.objects.get(rating_id=rate_obj).delete()
                log_data.warning(f'enter the valid badge_id')
            if data.get('badge'):
                task_badges = {'rating': rate_obj.id, 'badge': data.get('badge')}
                task_badge_ser = rating_ser.RatingBadgeSerializer(data=task_badges, many=False)
                if task_badge_ser.is_valid(raise_exception=True):
                    task_badge_ser.save()
                    updated_data = rating_model.TaskRating.objects.get_badges(id=rate_obj.id)
                    task_ser = rating_ser.ViewBadgeSerializer(updated_data, many=True)
                    log_data.info(f'update the data successfully')
                    return message.update_badge(task_ser)
        else:
            log_data.error(f'{serializer.errors}')
            return message.invalid_serializer(serializer)

    @transaction.atomic()
    def view_badge_data(self, assigned_to):
        """
        :param assigned_to:
        :return: method to update the
        """
        badge_data = rating_model.TaskRating.objects.get_badges(assigned_to=assigned_to)
        num = rating_model.TaskRating.objects.get_badges(assigned_to_id=assigned_to).__len__()
        den = num * 100
        total = 0
        for n in range(num):
            badge_value = rating_model.TaskRating.objects.get_badges(assigned_to=assigned_to)
            rate_id = badge_value[n].id
            badge = rating_model.RatingBadge.objects.get(rating_id=rate_id)
            total_sum = badge.badge.badge_value
            total += total_sum
        percentage = (total / den) * 100
        serializer = rating_ser.ViewBadgeSerializer(badge_data, many=True)
        log_data.info(f'view the badge data with the overall percentage of data a user has got')
        return message.get_badge_data(serializer, percentage)

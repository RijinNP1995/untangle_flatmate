from rest_framework import serializers

from master.serializers import RatingsBadgeSerializer
from task import models as model
from task_badge import models as rating_model
from task_badge.models import RatingBadge, RatingStatus
from user import serializers as user_ser
from user.models import ProfilePhoto


class RatingStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = rating_model.RatingStatus
        fields = (
            '__all__'
        )


class RatingBadgeSerializer(serializers.ModelSerializer):
    class Meta:
        model = rating_model.RatingBadge
        fields = (
            '__all__'
        )


class AssignedSerializer(serializers.ModelSerializer):
    class Meta:
        model = rating_model.AssignTo
        fields = (
            '__all__'
        )


class RatedBySerializer(serializers.ModelSerializer):
    class Meta:
        model = rating_model.RatedBy
        fields = (
            '__all__'
        )


class RateBySerializer(serializers.ModelSerializer):
    display_name = serializers.SerializerMethodField()
    photo = serializers.SerializerMethodField()

    class Meta:
        model = rating_model.RatedBy
        fields = (
            'rated_by',
            'display_name',
            'photo',
        )

    def get_display_name(self, obj):
        return obj.rated_by.display_name

    def get_photo(self, obj):
        """
        :param obj:
        :return: serializer to get the photo
        """
        user_id = obj.rated_by.id
        if model.ProfilePhoto.objects.filter(user_id=user_id).exists():
            profile_photo = ProfilePhoto.objects.get(user_id_id=user_id)
            photo_ser = user_ser.ProfilePhotoSerializer(profile_photo)
            return photo_ser.data
        else:
            model.User.objects.filter(id=user_id).exists()
            serializer = user_ser.PeopleSearchDataSerializer(user_id)
            return serializer.data


class AssignedtoSerializer(serializers.ModelSerializer):
    display_name = serializers.SerializerMethodField()
    photo = serializers.SerializerMethodField()

    class Meta:
        model = rating_model.AssignTo
        fields = (
            'assign_to',
            'display_name',
            'photo',
        )

    def get_display_name(self, obj):
        return obj.assign_to.display_name

    def get_photo(self, obj):
        """
        :param obj:
        :return: serializer to get the photo
        """
        user_id = obj.assign_to.id
        if model.ProfilePhoto.objects.filter(user_id=user_id).exists():
            profile_photo = ProfilePhoto.objects.get(user_id_id=user_id)
            photo_ser = user_ser.ProfilePhotoSerializer(profile_photo)
            return photo_ser.data
        else:
            model.User.objects.filter(id=user_id).exists()
            serializer = user_ser.PeopleSearchDataSerializer(user_id)
            return serializer.data


class TasksSerializer(serializers.ModelSerializer):
    class Meta:
        model = rating_model.TaskData
        fields = (
            '__all__'
        )


class TaskDataSerializer(serializers.ModelSerializer):
    task_name = serializers.SerializerMethodField()
    task_description = serializers.SerializerMethodField()

    class Meta:
        model = rating_model.TaskData
        fields = (
            'task',
            'task_name',
            'task_description',
        )

    def get_task_name(self, obj):
        return obj.task.task_name

    def get_task_description(self, obj):
        return obj.task.task_description


class TaskBadgeSerializer(serializers.ModelSerializer):
    badge_label = serializers.SerializerMethodField()
    badges = serializers.SerializerMethodField()
    badge_value = serializers.SerializerMethodField()

    class Meta:
        model = RatingBadge
        fields = (
            'badge_label',
            'badges',
            'badge_value',
        )

    def get_badge_label(self, obj):
        return obj.badge.label

    def get_badges(self, obj):
        badge_obj = RatingsBadgeSerializer(obj.badge)
        return badge_obj.data['badges']

    def get_badge_value(self, obj):
        return obj.badge.badge_value


class RatingTaskStatusSerializer(serializers.ModelSerializer):
    status_label = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()

    class Meta:
        model = RatingStatus
        fields = (
            'status_label',
            'status'
        )

    def get_status_label(self, obj):
        return obj.task_status.label

    def get_status(self, obj):
        return obj.task_status.task_status


class RateBadgeSerializer(serializers.ModelSerializer):
    status = serializers.SerializerMethodField()
    badge = serializers.SerializerMethodField()
    assigned_to = serializers.SerializerMethodField()
    task = serializers.SerializerMethodField()
    rated_by = serializers.SerializerMethodField()

    class Meta:
        model = rating_model.TaskRating
        fields = (
            'assigned_to',
            'task',
            'status',
            'badge',
            'comments',
            'rated_by',
        )

    def get_assigned_to(self, obj):
        task_assign = rating_model.AssignTo.objects.get_assigned_to(rating_id=obj.id)
        task_assign_ser = AssignedtoSerializer(task_assign, many=True)
        return task_assign_ser.data

    def get_task(self, obj):
        task_data = rating_model.TaskData.objects.get_tasks(rating_id=obj.id)
        task_data_ser = TasksSerializer(task_data, many=True)
        return task_data_ser.data

    def get_rated_by(self, obj):
        task_assign = rating_model.RatedBy.objects.get_assigned_to(rating_id=obj.id)
        task_assign_ser = RatedBySerializer(task_assign, many=True)
        return task_assign_ser.data

    def get_status(self, obj):
        task_status = rating_model.RatingStatus.objects.get_tasks(rating_id=obj.id)
        task_status_ser = RatingTaskStatusSerializer(task_status, many=True)
        return task_status_ser.data

    def get_badge(self, obj):
        task_badge = rating_model.RatingBadge.objects.get_badges(rating_id=obj.id)
        task_badge_ser = TaskBadgeSerializer(task_badge, many=True)
        return task_badge_ser.data


class UpdateRateBadgeSerializer(serializers.ModelSerializer):
    badge = serializers.SerializerMethodField()
    task = serializers.SerializerMethodField()

    class Meta:
        model = rating_model.TaskRating
        fields = (
            'id',
            'assigned_to',
            'task',
            'badge',
            'rated_by',
            'comments',
        )

    def get_badge(self, obj):
        task_badge = rating_model.RatingBadge.objects.get_badges(rating_id=obj.id)
        task_badge_ser = TaskBadgeSerializer(task_badge, many=True)
        return task_badge_ser.data

    def get_task(self, obj):
        task_data = rating_model.TaskData.objects.get_tasks(rating_id=obj.id)
        task_data_ser = TasksSerializer(task_data, many=True)
        return task_data_ser.data

    def update(self, instance, validated_data):
        instance.task = validated_data.get('task')
        instance.badge = validated_data.get('badge')
        instance.comments = validated_data.get('comments')

        instance.save()

        return instance


class ViewBadgeSerializer(serializers.ModelSerializer):
    badge = serializers.SerializerMethodField()
    assigned_to = serializers.SerializerMethodField()
    task = serializers.SerializerMethodField()
    rated_by = serializers.SerializerMethodField()

    class Meta:
        model = rating_model.TaskRating
        fields = (
            'id',
            'assigned_to',
            'task',
            'rated_by',
            'badge',
            'comments',
        )

    def get_assigned_to(self, obj):
        task_assign = rating_model.AssignTo.objects.get_assign_to(rating_id=obj.id)
        task_assign_ser = AssignedtoSerializer(task_assign, many=True)
        return task_assign_ser.data

    def get_task(self, obj):
        task_data = rating_model.TaskData.objects.get_tasks(rating_id=obj.id)
        task_data_ser = TaskDataSerializer(task_data, many=True)
        return task_data_ser.data

    def get_rated_by(self, obj):
        task_assign = rating_model.RatedBy.objects.get_assign_to(rating_id=obj.id)
        task_assign_ser = RateBySerializer(task_assign, many=True)
        return task_assign_ser.data

    def get_badge(self, obj):
        task_badge = rating_model.RatingBadge.objects.get_badges(rating_id=obj.id)
        task_badge_ser = TaskBadgeSerializer(task_badge, many=True)
        return task_badge_ser.data

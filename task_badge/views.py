from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.views import APIView

from flatmates_backend import responses as message
from task_badge.services import TaskBadgeService

task_badge_service = TaskBadgeService()


# Create your views here.


class TaskBadge(APIView):
    @swagger_auto_schema(
        operation_id='Add Badge',
        request_body=openapi.Schema(type=openapi.TYPE_OBJECT,
                                    properties={
                                        'assigned_to': openapi.Schema(type=openapi.TYPE_INTEGER, ),
                                        'task': openapi.Schema(type=openapi.TYPE_INTEGER, ),
                                        'badge': openapi.Schema(type=openapi.TYPE_INTEGER, ),
                                        'task_status': openapi.Schema(type=openapi.TYPE_INTEGER, ),
                                        'comments': openapi.Schema(
                                            type=openapi.TYPE_STRING,
                                        )
                                    }
                                    ),
    )
    def post(self, request):
        """
        :param request:
        :return: API for add badge
        """
        try:
            return task_badge_service.add_badge(data=request.data, user=request.user)
        except Exception as e:
            return message.exception_response(e)

    @swagger_auto_schema(
        operation_id='Update Badge',
        request_body=openapi.Schema(type=openapi.TYPE_OBJECT,
                                    properties={
                                        'assigned_to': openapi.Schema(type=openapi.TYPE_INTEGER, ),
                                        'task': openapi.Schema(type=openapi.TYPE_INTEGER, ),
                                        'badge': openapi.Schema(type=openapi.TYPE_INTEGER, ),
                                        'task_status': openapi.Schema(type=openapi.TYPE_INTEGER, ),
                                        'comments': openapi.Schema(
                                            type=openapi.TYPE_STRING,
                                        )
                                    }
                                    ),
    )
    def patch(self, request):
        """
        :param request:
        :return: api to update the badge
        """
        try:
            return task_badge_service.update_badge(data=request.data, user=request.user)
        except Exception as e:
            return message.exception_response(e)

    def get(self, request):
        """
        :param request:
        :return: api to get the task badge rating details
        """
        try:
            assigned_to = request.GET.get('assigned_to')
            return task_badge_service.view_badge_data(assigned_to=assigned_to)
        except Exception as e:
            return message.exception_response(e)

from django.urls import path

from task_badge import views as view

urlpatterns = [
    path('add-badge/', view.TaskBadge.as_view(), name='rate_task'),
    path('get-badges/', view.TaskBadge.as_view(), name='get_the_badge'),
    path('update-badge/', view.TaskBadge.as_view(), name='update_the_badge'),
]

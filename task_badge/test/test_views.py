import json

from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from flat_api.models import Flat
from master import models as master_model
from task import models as task_model
from task_badge import models as rating_model
from user import models as user_model


class TestTaskBadge(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = user_model.User.objects.create(first_name='first_name', last_name='last_name', display_name='test',
                                                   profession='profession', company_name='company_name', age=20,
                                                   locality='locality', phone='9048893681', description='description',
                                                   date_joined='date_joined', is_email_verified=False,
                                                   is_phone_verified=False)
        self.user.save()
        self.token1 = Token.objects.get_or_create(user=self.user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token1[0].key)
        self.task_icon = master_model.TaskIcon.objects.create(icon='facebook.png')
        self.task_badge = master_model.TaskBadge.objects.create(label='gold', badges='facebook.png', badge_value=100)
        self.task_to_do = master_model.TaskStatus.objects.create(label='label1', task_status='Test Status')
        self.task_progress = master_model.TaskStatus.objects.create(label='label2', task_status='Test Status')
        self.task_completed = master_model.TaskStatus.objects.create(label='label3', task_status='Test Status')
        self.assign_to = user_model.User.objects.create(first_name='first_name', last_name='last_name',
                                                        display_name='test',
                                                        profession='profession', company_name='company_name', age=20,
                                                        locality='locality', email='rijin@gmail.com',
                                                        description='description',
                                                        date_joined='date_joined', is_email_verified=False,
                                                        is_phone_verified=False)
        self.flat = Flat.objects.create(flat_name='flat_name', flat_no='flat_no', landmark='landmark',
                                        rent_per_month=1500.50, num_of_beds=1,
                                        flat_description='flat_description',
                                        flatmate_description='flatmate_description',
                                        flatmate_min_age=20, flatmate_max_age=30,
                                        expected_gender='male')
        self.task = task_model.Task.objects.create(task_name='task_name', task_description='task_description',
                                                   due_date='2020-03-28',
                                                   due_time='12:12:12',
                                                   flat=self.flat,
                                                   created_by=self.user)
        self.assign_task = task_model.AssignTask.objects.create(assigned_to=self.assign_to, task=self.task)
        self.task_rating = rating_model.TaskRating.objects.create(assigned_to=self.user, rated_by=self.user)

        self.valid_payload = {

            "assigned_to": 2,
            "task": 1,
            "badge": 1,
            "task_status": 3,
            "comments": 'test_description'
        }
        self.invalid_payload1 = {

            "assigned_to": 1,
            "task": 1,
            "badge": 1,
            "task_status": 3,
            "comments": 'test_description'
        }
        self.invalid_payload2 = {

            "assigned_to": 2,
            "task": 3,
            "badge": 1,
            "task_status": 3,
            "comments": 'test_description'
        }
        self.invalid_payload3 = {

            "assigned_to": 2,
            "task": 1,
            "badge": 10,
            "task_status": 3,
            "comments": 'test_description'
        }
        self.invalid_payload4 = {

            "assigned_to": 2,
            "task": 1,
            "badge": 1,
            "task_status": 30,
            "comments": 'test_description'
        }

    def test_add_badge(self):
        """
        :return: Pass the values to add the badge
        """
        response = self.client.post(
            reverse('rate_task'),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['message'], 'Task badge given successfully')

    def test_invalid_assign_to(self):
        """
        :return: Pass a invalid assign_to id
        """
        response = self.client.post(
            reverse('rate_task'),
            data=json.dumps(self.invalid_payload1),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['message'], 'Invalid task_id or user_id.')

    def test_invalid_task(self):
        response = self.client.post(
            reverse('rate_task'),
            data=json.dumps(self.invalid_payload2),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['message'], 'Invalid task_id or user_id.')

    def test_invalid_badge(self):
        response = self.client.post(
            reverse('rate_task'),
            data=json.dumps(self.invalid_payload3),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['error'],
                         '{\'badge\': [ErrorDetail(string=\'Invalid pk "10" - object does not exist.\', code=\'does_not_exist\')]}')

    def test_invalid_status(self):
        response = self.client.post(
            reverse('rate_task'),
            data=json.dumps(self.invalid_payload4),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['error'],
                         '{\'task_status\': [ErrorDetail(string=\'Invalid pk "30" - object does not exist.\', code=\'does_not_exist\')]}')


class TestUpdateBadge(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = user_model.User.objects.create(first_name='first_name', last_name='last_name', display_name='test',
                                                   profession='profession', company_name='company_name', age=20,
                                                   locality='locality', phone='9048893681', description='description',
                                                   date_joined='date_joined', is_email_verified=False,
                                                   is_phone_verified=False)
        self.user.save()
        self.token1 = Token.objects.get_or_create(user=self.user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token1[0].key)
        self.task_icon = task_model.TaskIcon.objects.create(icon='facebook.png')
        self.task_badge = task_model.TaskBadge.objects.create(label='gold', badges='facebook.png', badge_value=100)
        self.task_to_do = task_model.TaskStatus.objects.create(label='label1', task_status='Test Status')
        self.task_progress = task_model.TaskStatus.objects.create(label='label2', task_status='Test Status')
        self.task_completed = task_model.TaskStatus.objects.create(label='label3', task_status='Test Status')
        self.assign_to = user_model.User.objects.create(first_name='first_name', last_name='last_name',
                                                        display_name='test',
                                                        profession='profession', company_name='company_name', age=20,
                                                        locality='locality', email='rijin@gmail.com',
                                                        description='description',
                                                        date_joined='date_joined', is_email_verified=False,
                                                        is_phone_verified=False)
        self.flat = Flat.objects.create(flat_name='flat_name', flat_no='flat_no', landmark='landmark',
                                        rent_per_month=1500.50, num_of_beds=1,
                                        flat_description='flat_description',
                                        flatmate_description='flatmate_description',
                                        flatmate_min_age=20, flatmate_max_age=30,
                                        expected_gender='male')
        self.task = task_model.Task.objects.create(task_name='task_name', task_description='task_description',
                                                   due_date='2020-03-28',
                                                   due_time='12:12:12',
                                                   flat=self.flat,
                                                   created_by=self.user)
        self.assign_task = task_model.AssignTask.objects.create(assigned_to=self.assign_to, task=self.task)
        self.task_rating = rating_model.TaskRating.objects.create(assigned_to=self.user, rated_by=self.user)
        self.rating_badge = rating_model.RatingBadge.objects.create(badge=self.task_badge, rating=self.task_rating)
        self.task_data = rating_model.TaskData.objects.create(task=self.task, rating=self.task_rating)
        self.assigned_to = rating_model.AssignTo.objects.create(assign_to=self.assign_to, rating=self.task_rating)
        self.valid_payload = {

            "id": 1,
            "assigned_to": 2,
            "task": 1,
            "badge": 1,
            "comments": "second update test_description"
        }
        self.invalid_payload1 = {

            "id": "10",
            "assigned_to": 2,
            "task": 1,
            "badge": 1,
            "comments": "second update test_description"
        }
        self.invalid_payload2 = {

            "id": 1,
            "assigned_to": 20,
            "task": 1,
            "badge": 1,
            "comments": "second update test_description"
        }
        self.invalid_payload3 = {

            "id": 1,
            "assigned_to": 2,
            "task": 10,
            "badge": 1,
            "comments": "second update test_description"
        }
        self.invalid_payload4 = {

            "id": 1,
            "assigned_to": 2,
            "task": 1,
            "badge": 10,
            "comments": "second update test_description"
        }

    def test_update_badge(self):
        """
        :return: Pass the valid payload to update the task badge
        """
        response = self.client.patch(
            reverse('update_the_badge'),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['message'], 'Update the badges successfully')

    def test_update_invalid_task_status(self):
        response = self.client.patch(
            reverse('update_the_badge'),
            data=json.dumps(self.invalid_payload1),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['error'], 'TaskRating matching query does not exist.')

    def test_update_invalid_assign_to(self):
        response = self.client.patch(
            reverse('update_the_badge'),
            data=json.dumps(self.invalid_payload2),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['message'], 'Invalid task_id or user_id.')

    def test_update_invalid_task(self):
        response = self.client.patch(
            reverse('update_the_badge'),
            data=json.dumps(self.invalid_payload3),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['message'], 'Invalid task_id or user_id.')

    def test_update_invalid_badge(self):
        response = self.client.patch(
            reverse('update_the_badge'),
            data=json.dumps(self.invalid_payload4),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.data['error'],
                         '{\'badge\': [ErrorDetail(string=\'Invalid pk "10" - object does not exist.\', code=\'does_not_exist\')]}')

from datetime import datetime

from django.test import TestCase

from flat_api.models import Flat
from master.models import TaskStatus
from task import models as task_model
from task_badge import models as rating_model
from user import models as user_model


class TaskBadgeModelTestCase(TestCase):
    def setUp(self):
        self.gender = user_model.Gender.objects.create(type='male')
        self.task_name = 'Task_name'
        self.task_description = 'Task_description'
        self.due_date = '2020-03-28'
        self.due_time = '12:12:12'
        self.task_status = TaskStatus(task_status='Completed')
        self.icon = task_model.TaskIcon(icon='facebook.png')
        self.first_name = 'First_name'
        self.last_name = 'Last_name'
        self.display_name = 'Display_name'
        self.gender = user_model.Gender.objects.create(type='male')
        self.profession = 'Test Profession'
        self.company_name = 'Test Company'
        self.age = 12
        self.locality = 'Kochi'
        self.phone = '0987654321'
        self.description = '#This is a test description'
        self.quote = 'This is the quote'
        self.email = 'test@email.com'
        self.date_joined = datetime.now()
        self.is_email_verified = False
        self.is_phone_verified = False
        self.valid_user = False
        self.user = user_model.User.objects.create(first_name=self.first_name,
                                                   last_name=self.last_name, display_name=self.display_name,
                                                   gender=self.gender,
                                                   profession=self.profession, company_name=self.company_name,
                                                   age=self.age,
                                                   locality=self.locality, phone=self.phone,
                                                   description=self.description,
                                                   email=self.email,
                                                   date_joined=self.date_joined,
                                                   is_email_verified=self.is_email_verified,
                                                   is_phone_verified=self.is_phone_verified, valid_user=self.valid_user)
        self.flat = Flat.objects.create(flat_no='flat_no', flat_name='flat_name', landmark='landmark',
                                        rent_per_month=1500.50, num_of_beds=1,
                                        flat_description='flat_description',
                                        flatmate_description='flatmate_description',
                                        flatmate_min_age=20, flatmate_max_age=30,
                                        expected_gender='male')
        self.task = task_model.Task.objects.create(task_name=self.task_name, task_description=self.task_description,
                                                   due_date=self.due_date,
                                                   due_time=self.due_time, flat=self.flat, created_by=self.user)
        self.task_rating = rating_model.TaskRating(assigned_to=self.user, rated_by=self.user,
                                                   comments='test_comments')

    def test_model_add_badge(self):
        old_count = rating_model.TaskRating.objects.count()
        self.task_rating.save()
        new_count = rating_model.TaskRating.objects.count()
        self.assertNotEqual(old_count, new_count)

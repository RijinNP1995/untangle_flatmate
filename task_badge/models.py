from django.db import models

from master.models import TaskBadge, TaskStatus
from task.managers import TaskRatingManager, TaskManager, AssignToManager, TaskDataManager
from task.models import Task
from user.models import User


# Create your models here.

class TaskRating(models.Model):
    assigned_to = models.ForeignKey(User, related_name='%(class)s_assigned_to', on_delete=models.CASCADE, null=False,
                                    editable=False, default=1)
    rated_by = models.ForeignKey(User, related_name='%(class)s_rated_by', on_delete=models.CASCADE, null=False,
                                 editable=False, default=1)
    comments = models.TextField('comments', max_length=30)
    objects = TaskRatingManager()


class RatingStatus(models.Model):
    task_status = models.ForeignKey(TaskStatus, on_delete=models.CASCADE)
    rating = models.ForeignKey(TaskRating, on_delete=models.CASCADE)
    objects = TaskManager()


class RatingBadge(models.Model):
    badge = models.ForeignKey(TaskBadge, on_delete=models.CASCADE)
    rating = models.ForeignKey(TaskRating, on_delete=models.CASCADE)
    objects = TaskRatingManager()


class AssignTo(models.Model):
    assign_to = models.ForeignKey(User, on_delete=models.CASCADE)
    rating = models.ForeignKey(TaskRating, on_delete=models.CASCADE)
    objects = AssignToManager()


class RatedBy(models.Model):
    rated_by = models.ForeignKey(User, on_delete=models.CASCADE)
    rating = models.ForeignKey(TaskRating, on_delete=models.CASCADE)
    objects = AssignToManager()


class TaskData(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    rating = models.ForeignKey(TaskRating, on_delete=models.CASCADE)
    objects = TaskDataManager()

from django.apps import AppConfig


class TaskBadgeConfig(AppConfig):
    name = 'task_badge'

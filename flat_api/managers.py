from flatmates_backend.base_manager import BaseManager


class FlatManager(BaseManager):
    # check user added flat exist or not
    def user_flat_exist(self, user):
        if self.get_by_filter(created_by=user).exists():
            return True
        return False


class FlatFileManager(BaseManager):
    #  to filter the files of the flat
    def get_files(self, **filter_args):
        return self.get_all().filter(**filter_args)


class FlatAmenityManager(BaseManager):
    # filter amenity
    def get_amenity(self, **filter_args):
        return self.get_all().filter(**filter_args)


class FlatmateCommunityManager(BaseManager):
    def get_active_pending_request(self, **filter_args):
        return self.get_all_active().filter(**filter_args)


class FlatPreferenceManager(BaseManager):
    # filter preferences
    def get_preference(self, **filter_args):
        return self.get_all().filter(**filter_args)


class FlatAgeGroupManager(BaseManager):
    # to filter age group
    def get_age_group(self, **filter_args):
        return self.get_all().filter(**filter_args)


class FlatTenantManager(BaseManager):
    # to filter flatmates
    def get_tenant(self, **filter_args):
        return self.get_all_active().filter(**filter_args)

    def get_staying_tenant(self):
        return self.get_all_active().filter(stay_in_flat=True)

    def get_active_tenants(self, **filter_args):
        return self.get_staying_tenant().filter(**filter_args)


class FavouriteFlatManager(BaseManager):
    pass


class FlatCurrencyManager(BaseManager):
    def get_currency(self, **filter_args):
        return self.get_all().filter(**filter_args)

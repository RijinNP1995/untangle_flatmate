import json

from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from flat_api.models import Amenity
from user.models import User


class TestAddFlat(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user1 = User(phone='+919895203121', is_phone_verified=True)
        self.user1.save()
        self.token1 = Token.objects.get_or_create(user=self.user1)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token1[0].key)
        self.user1.set_password('user@1234')
        self.user1.save()
        self.label = "wifi"
        self.description = "24*7 free wifi available"
        self.created_by = User.objects.create(email="ss@example.com")
        self.amenities = Amenity.objects.create(label=self.label, description=self.description,
                                                created_by=self.created_by)
        # enter the valid payload
        self.valid_payload = {
            "flat_name": "my flat",
            "flat_no": "house no",
            "landmark": "landmark",
            "city": "kochi",
            "rent_per_month": 2000,
            "num_of_beds": 3,
            "flat_description": "short description about flat/house",
            "flatmate_description": "short description about expected flatmate",
            "flatmate_min_age": 18,
            "flatmate_max_age": 40,
            "expected_gender": "male",
            "lat": 10.0014,
            "lon": 76.3101,
            "amenities": [1],
            "images": ""
        }

    # validate the data is valid or not
    def test_add_flat(self):
        response = self.client.post(
            reverse('add_flat'),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertAlmostEqual(response.status_code, status.HTTP_201_CREATED)

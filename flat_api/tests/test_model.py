from django.test import TestCase

from flat_api.models import Flat, Amenity
from user.models import User


class TestFlat(TestCase):
    """This class defines the test suite for the Flat model."""

    def setUp(self):
        """Define the test client and other test variables."""
        self.label = "wifi"
        self.description = "24*7 free wifi available"
        self.created_by = User.objects.create(email="ss@example.com")
        self.amenities = Amenity.objects.create(label=self.label, description=self.description,
                                                created_by=self.created_by)
        self.flat_name = "my flat"
        self.flat_no = "house no"
        self.landmark = "landmark"
        self.city = "kochi"
        self.rent_per_month = 2000
        self.num_of_beds = 3
        self.flat_description = "short description about flat/house"
        self.flatmate_description = "short description about expected flatmate"
        self.flatmate_min_age = 18
        self.flatmate_max_age = 40
        self.expected_gender = "male"
        self.lat = 10.0014
        self.lon = 76.3101
        self.flat = Flat(flat_name=self.flat_name, flat_no=self.flat_no, landmark=self.landmark,
                         rent_per_month=self.rent_per_month, num_of_beds=self.num_of_beds,
                         flat_description=self.flat_description, flatmate_description=self.flatmate_description,
                         flatmate_min_age=self.flatmate_min_age, flatmate_max_age=self.flatmate_max_age,
                         expected_gender=self.expected_gender)

    def test_create_a_flat(self):
        old_count = Flat.objects.count()
        self.flat.save()
        new_count = Flat.objects.count()
        self.assertNotEqual(old_count, new_count)

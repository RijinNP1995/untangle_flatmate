from rest_framework import serializers

from expenses.serializers import ViewCurrencySerializer
from flat_api import models
from user import models as user_models
from user import serializers as user_serializers


class AddFlatSerializer(serializers.ModelSerializer):
    """
    Serializer class for flat
    """

    class Meta:
        model = models.Flat
        fields = (
            'flat_no',
            'flat_name',
            'landmark',
            'city',
            'rent_per_month',
            'num_of_beds',
            'flat_description',
            'flatmate_description',
            'expected_gender',
            'lat',
            'lon',
            'zip_code',
            'available_from'
        )


class FlatAmenityListSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.FlatAmenity
        fields = '__all__'


class FlatFileListSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.FlatFile
        fields = '__all__'


class FlatCurrencySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.FlatCurrency
        fields = '__all__'


class FlatDetailsSerializer(serializers.ModelSerializer):
    """
    Serializer class for flat
    """
    # id = serializers.IntegerField()
    # flat_no = serializers.CharField()
    # flat_name = serializers.CharField()
    # landmark = serializers.CharField()
    # city = serializers.CharField()
    # rent_per_month = serializers.CharField()
    # num_of_beds = serializers.IntegerField()
    # flat_description = serializers.CharField()
    # flatmate_description = serializers.CharField()
    # flatmate_min_age = serializers.IntegerField()
    # flatmate_max_age = serializers.IntegerField()
    # expected_gender = serializers.CharField()
    # lat = serializers.DecimalField(decimal_places=17, max_digits=20)
    # lon = serializers.DecimalField(decimal_places=17, max_digits=20)
    amenities = serializers.SerializerMethodField()
    files = serializers.SerializerMethodField()
    preferences = serializers.SerializerMethodField()
    age_group = serializers.SerializerMethodField()
    tenants = serializers.SerializerMethodField()
    owner_details = serializers.SerializerMethodField()
    currency = serializers.SerializerMethodField()

    class Meta:
        model = models.Flat
        fields = (
            'id',
            'flat_no',
            'flat_name',
            'landmark',
            'city',
            'rent_per_month',
            'currency',
            'num_of_beds',
            'flat_description',
            'flatmate_description',
            'expected_gender',
            'lat',
            'lon',
            'amenities',
            'files',
            'preferences',
            'age_group',
            'tenants',
            'created_by',
            'zip_code',
            'owner_details',
            'available_from'

        )

    def get_currency(self, obj):
        if models.FlatCurrency.objects.filter(flat=obj.id):
            currency_data = models.FlatCurrency.objects.get(flat=obj.id)
            currency_ser = ViewCurrencySerializer(currency_data, many=False)
            return currency_ser.data
        return

    def get_files(self, obj):
        flat_files = models.FlatFile.objects.get_files(flat=obj.id)
        serializer = FlatFileListSerializer(flat_files, many=True)
        return serializer.data

    def get_amenities(self, obj):
        flat_aminities = models.FlatAmenity.objects.get_amenity(flat=obj.id)
        aminity_ser = ViewAmenityDetailSerializer(flat_aminities, many=True)
        return aminity_ser.data

    def get_preferences(self, obj):
        flat_preferences = models.FlatPreference.objects.get_preference(flat=obj.id)
        flat_preferences_ser = ViewPreferenceDetailSerializer(flat_preferences, many=True)
        return flat_preferences_ser.data

    def get_age_group(self, obj):
        flatmates_age_group = models.FlatAgeGroup.objects.get_age_group(flat=obj.id)
        flatmates_age_group_ser = ViewAgeGroupDetailSerializer(flatmates_age_group, many=True)
        return flatmates_age_group_ser.data

    def get_tenants(self, obj):
        flat_tenants_list = models.FlatTenant.objects.get_tenant(flat=obj.id)
        flat_tenants_list_ser = ViewTenantDetailSerializer(flat_tenants_list, many=True)
        return flat_tenants_list_ser.data

    def get_owner_details(self, obj):
        user_obj = user_models.User.objects.get_by_id(pk=obj.created_by.id)
        user_obj_ser = user_serializers.ProfileDetailsSerializer(user_obj)
        return user_obj_ser.data


class UpdateFlatDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Flat
        fields = "__all__"


class UpdateFlatSerializer(serializers.ModelSerializer):
    """
    Serializer class for flat
    """
    flat_no = serializers.CharField()
    flat_name = serializers.CharField()
    landmark = serializers.CharField()
    city = serializers.CharField()
    rent_per_month = serializers.FloatField()
    num_of_beds = serializers.IntegerField()
    flat_description = serializers.CharField()
    flatmate_description = serializers.CharField()
    expected_gender = serializers.CharField()
    lat = serializers.CharField()
    lon = serializers.CharField()
    available_from = serializers.DateField()

    class Meta:
        model = models.Flat
        fields = (
            'flat_no',
            'flat_name',
            'landmark',
            'city',
            'rent_per_month',
            'num_of_beds',
            'flat_description',
            'flatmate_description',
            'expected_gender',
            'lat',
            'lon',
            'available_from'
        )

    def update(self, instance, validated_data):
        instance.flat_no = validated_data.get('flat_no')
        instance.flat_name = validated_data.get('flat_name')
        instance.landmark = validated_data.get('landmark')
        instance.city = validated_data.get('city')
        instance.rent_per_month = validated_data.get('rent_per_month')
        instance.num_of_beds = validated_data.get('num_of_beds')
        instance.flat_description = validated_data.get('flat_description')
        instance.flatmate_description = validated_data.get('flatmate_description')
        instance.expected_gender = validated_data.get('expected_gender')
        instance.lat = validated_data.get('lat')
        instance.lon = validated_data.get('lon')
        instance.available_from = validated_data.get('available_from')
        instance.save()

        return instance


class FlatListSerializer(serializers.ModelSerializer):
    """
    Serializer class for flat
    """
    amenities = serializers.SerializerMethodField()
    files = serializers.SerializerMethodField()

    class Meta:
        model = models.Flat
        fields = (
            'id',
            'flat_no',
            'flat_name',
            'landmark',
            'city',
            'rent_per_month',
            'amenities',
            'files',
            'available_from'
        )

    def get_files(self, obj):
        flat_files = models.FlatFile.objects.get_files(flat=obj.id)
        serializer = FlatFileListSerializer(flat_files, many=True)
        return serializer.data

    def get_amenities(self, obj):
        flat_amenities = models.FlatAmenity.objects.get_amenity(flat=obj.id)
        amenity_ser = ViewAmenityDetailSerializer(flat_amenities, many=True)
        return amenity_ser.data


class CreateSeekerRequestSerializer(serializers.ModelSerializer):
    flat = serializers.IntegerField()
    seeker = serializers.IntegerField()

    class Meta:
        model = models.FlatmateCommunity
        fields = ('flat',
                  'seeker')

    def validate_flat(self, flat_id):
        if models.Flat.objects.pk_does_exist(id=flat_id):
            flat = models.Flat.objects.get_by_id(pk=flat_id)
            return flat
        raise serializers.ValidationError("This flat does not exist")

    def validate_seeker(self, seeker):
        user = user_models.User.objects.get_by_id(pk=seeker)
        return user

    def validate(self, data):
        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user
            if user == data['seeker']:
                raise serializers.ValidationError("Requested user and seeker must be same. ")

        # if data['flat'].created_by == data['seeker']:
        #     raise serializers.ValidationError("Flat owner and seeker can not be same. ")

        if models.FlatmateCommunity.objects.get_by_filter(seeker=data['seeker'], is_active=True):
            raise serializers.ValidationError("This user has another active request")

        if models.FlatTenant.objects.get_active_tenants(tenant=user):
            raise serializers.ValidationError("This user has already joined in another flat")
        return data


class SeekerRequestListSerializer(serializers.ModelSerializer):
    flat_details = serializers.SerializerMethodField()
    seeker_details = serializers.SerializerMethodField()

    class Meta:
        model = models.FlatmateCommunity
        fields = ('id',
                  'status',
                  'flat_details',
                  'seeker_details')

    def get_seeker_details(self, obj):
        seeker_data = user_serializers.ProfileDetailsSerializer(obj.seeker)
        return seeker_data.data

    def get_flat_details(self, obj):
        flat_data = FlatDetailsSerializer(obj.flat)
        return flat_data.data


class AcceptSeekerRequestSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField()

    class Meta:
        model = models.FlatmateCommunity
        fields = ('pk',)

    def validate_pk(self, pk):
        flatmate_community = models.FlatmateCommunity.objects.get_by_id(pk=pk)
        return flatmate_community

    def validate(self, data):
        flatmate_community_obj = models.FlatmateCommunity.objects.get_by_id(pk=data['pk'])
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            if request.user != flatmate_community_obj.flat.created_by:
                raise serializers.ValidationError("Requested user and dweller must be same. ")
        return flatmate_community_obj


class FlatPreferenceListSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.FlatPreference
        fields = '__all__'


class FlatAgeGroupListSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.FlatAgeGroup
        fields = '__all__'


class FlatTenantListSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.FlatTenant
        fields = '__all__'


class ViewTenantDetailSerializer(serializers.ModelSerializer):
    tenant_name = serializers.SerializerMethodField()
    tenant_profile_photo = serializers.SerializerMethodField()

    class Meta:
        model = models.FlatTenant
        fields = ('id',
                  'tenant',
                  'tenant_name',
                  'tenant_profile_photo')

    def get_tenant_name(self, obj):
        if obj.tenant.display_name:
            return obj.tenant.display_name
        return obj.tenant.username

    def get_tenant_profile_photo(self, obj):
        if user_models.ProfilePhoto.objects.filter(user_id=obj.tenant).exists():
            user_obj = user_models.ProfilePhoto.objects.get(user_id=obj.tenant)
            user_photo_data = user_serializers.ProfilePhotoSerializer(user_obj)
            return user_photo_data.data['photo']
        return


class ViewAgeGroupDetailSerializer(serializers.ModelSerializer):
    age_group_label = serializers.SerializerMethodField()
    age_group_desc = serializers.SerializerMethodField()
    age_group_icon = serializers.SerializerMethodField()

    class Meta:
        model = models.FlatAgeGroup
        fields = ('age_group', 'age_group_label', 'age_group_desc', 'age_group_icon')

    def get_age_group_label(self, obj):
        return obj.age_group.label

    def get_age_group_desc(self, obj):
        return obj.age_group.description

    def get_age_group_icon(self, obj):
        return str(obj.age_group.icon)


class ViewPreferenceDetailSerializer(serializers.ModelSerializer):
    preference_label = serializers.SerializerMethodField()
    preference_desc = serializers.SerializerMethodField()
    preference_icon = serializers.SerializerMethodField()

    class Meta:
        model = models.FlatPreference
        fields = ('preference', 'preference_label', 'preference_desc', 'preference_icon')

    def get_preference_label(self, obj):
        return obj.preference.label

    def get_preference_desc(self, obj):
        return obj.preference.description

    def get_preference_icon(self, obj):
        return str(obj.preference.icon)


class ViewAmenityDetailSerializer(serializers.ModelSerializer):
    amenity_label = serializers.SerializerMethodField()
    amenity_desc = serializers.SerializerMethodField()
    amenity_icon = serializers.SerializerMethodField()

    class Meta:
        model = models.FlatAmenity
        fields = ('amenity', 'amenity_label', 'amenity_desc', 'amenity_icon')

    def get_amenity_label(self, obj):
        return obj.amenity.label

    def get_amenity_desc(self, obj):
        return obj.amenity.description

    def get_amenity_icon(self, obj):
        # amenity_obj = master_serializers.AmenityListSerializer(obj.amenity)
        # return amenity_obj.data['icon']
        return str(obj.amenity.icon)


class CreateDwellerRequestSerializer(serializers.ModelSerializer):
    flat = serializers.IntegerField()
    seeker = serializers.IntegerField()

    class Meta:
        model = models.FlatmateCommunity
        fields = ('flat',
                  'seeker')

    def validate_flat(self, flat_id):
        if models.Flat.objects.pk_does_exist(id=flat_id):
            flat = models.Flat.objects.get_by_id(pk=flat_id)
            return flat
        raise serializers.ValidationError("This flat does not exist")

    def validate_seeker(self, seeker):
        user = user_models.User.objects.get_by_id(pk=seeker)
        return user

    def validate(self, data):
        # user = None
        # request = self.context.get("request")
        # if request and hasattr(request, "user"):
        #     user = request.user
        #     if data['flat'].created_by == user:
        #         raise serializers.ValidationError("Requested user and flat owner must be same. ")
        #
        # if data['flat'].created_by == data['seeker']:
        #     raise serializers.ValidationError("Flat owner and seeker can not be same. ")

        if models.FlatmateCommunity.objects.get_by_filter(seeker=data['seeker'], is_active=True):
            raise serializers.ValidationError("This user has another active request")

        if models.FlatTenant.objects.get_active_tenants(tenant=data['seeker']):
            raise serializers.ValidationError("This user has already joined in another flat")
        return data


class FavouriteFlatSerializer(serializers.ModelSerializer):
    flat = serializers.SerializerMethodField()

    class Meta:
        model = models.FavouriteFlat
        fields = ('id', 'flat', 'favourite', 'user')

    def get_flat(self, obj):
        return FlatDetailsSerializer(obj.flat).data


class TenantHistorySerializer(serializers.ModelSerializer):
    flat_name = serializers.SerializerMethodField()

    class Meta:
        model = models.FlatTenant
        fields = ('id',
                  'tenant',
                  'flat_name')

    def get_flat_name(self, obj):
        return obj.flat.flat_name

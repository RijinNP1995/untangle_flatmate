import datetime

from django.db import models
from django.utils.translation import ugettext_lazy as _

from flat_api import managers
from flat_api.managers import FlatManager, FlatFileManager, FlatAmenityManager, FlatPreferenceManager, \
    FlatAgeGroupManager, FlatTenantManager, FavouriteFlatManager, FlatCurrencyManager
from master.models import BaseModel, Amenity, Preferences, AgeGroup, Currency
from user.models import Gender, User


class Flat(BaseModel):
    flat_no = models.CharField(_('flat number'), max_length=20, blank=True, null=True)
    flat_name = models.CharField(_('flat name'), max_length=160, blank=True, null=True)
    landmark = models.CharField(_('landmark'), max_length=160, blank=True, null=True)
    city = models.CharField(_('city'), max_length=160, blank=True, null=True)
    rent_per_month = models.FloatField(_('rent per month'), default=0)
    num_of_beds = models.IntegerField(_('number of beds'), blank=True, null=True)
    flat_description = models.TextField(_('flat description'), blank=True, null=True)
    flatmate_description = models.TextField(_('flatmate description'), blank=True, null=True)
    flatmate_min_age = models.IntegerField(_("flatmate's min age"), blank=True, null=True)
    flatmate_max_age = models.IntegerField(_("flatmate's max age"), blank=True, null=True)
    expected_gender = models.CharField(_('expected gender'), max_length=32, blank=True, null=True)
    lat = models.DecimalField(decimal_places=17, max_digits=20, null=True, blank=True)
    lon = models.DecimalField(decimal_places=17, max_digits=20, null=True, blank=True)
    zip_code = models.CharField(_("zip code"), max_length=8, null=True, blank=True)
    display_public = models.BooleanField(_('display public'), default=True)
    available_from = models.DateField(_('available_from'), default=datetime.date.today)
    objects = FlatManager()

    def __str__(self):
        return self.flat_no.__str__() + "-" + self.flat_name.__str__()


class FlatFile(models.Model):
    file = models.ImageField()
    flat = models.ForeignKey(Flat, on_delete=models.CASCADE)
    objects = FlatFileManager()

    def __str__(self):
        return self.flat.__str__() + " " + self.file.__str__()


class FlatAmenity(models.Model):
    amenity = models.ForeignKey(Amenity, on_delete=models.CASCADE)
    flat = models.ForeignKey(Flat, on_delete=models.CASCADE)
    objects = FlatAmenityManager()

    def __str__(self):
        return self.flat.__str__() + " " + self.amenity.__str__()


class FlatmateCommunity(BaseModel):
    flat = models.ForeignKey(Flat, on_delete=models.CASCADE)
    seeker = models.ForeignKey(User, on_delete=models.CASCADE)
    status = models.CharField(_('request status'), max_length=15)
    comments = models.TextField(_('comments'), blank=True, null=True)
    objects = managers.FlatmateCommunityManager()

    def __str__(self):
        return self.flat.__str__() + " " + self.seeker.__str__()


class FlatPreference(models.Model):
    preference = models.ForeignKey(Preferences, on_delete=models.CASCADE)
    flat = models.ForeignKey(Flat, on_delete=models.CASCADE)
    objects = FlatPreferenceManager()

    def __str__(self):
        return self.flat.__str__() + " " + self.preference.__str__()


class FlatAgeGroup(models.Model):
    age_group = models.ForeignKey(AgeGroup, on_delete=models.CASCADE)
    flat = models.ForeignKey(Flat, on_delete=models.CASCADE)
    objects = FlatAgeGroupManager()

    def __str__(self):
        return self.flat.__str__() + " " + self.age_group.__str__()


class FlatCurrency(models.Model):
    currency = models.ForeignKey(Currency, on_delete=models.CASCADE)
    flat = models.ForeignKey(Flat, on_delete=models.CASCADE)
    objects = FlatCurrencyManager()

    def __str__(self):
        return self.flat.__str__() + " " + self.currency.__str__()


class FlatTenant(models.Model):
    flat = models.ForeignKey(Flat, on_delete=models.CASCADE)
    tenant = models.ForeignKey(User, on_delete=models.CASCADE)
    stay_in_flat = models.BooleanField(_('Stay on this flat'), default=True)
    is_admin = models.BooleanField(_('Admin of this flat'), default=False)
    is_active = models.BooleanField(_('active'), default=True)
    is_delete = models.BooleanField(_('delete'), default=False)
    added_on = models.DateTimeField(auto_now_add=True, editable=False, blank=True, null=True)
    added_by = models.ForeignKey(User, on_delete=models.CASCADE, editable=False, null=True, blank=True,
                                 related_name='%(class)s_added_by')
    exit_on = models.DateTimeField(blank=True, null=True, editable=False, )
    exit_by = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True, editable=False,
                                related_name='%(class)s_exit_by')
    objects = FlatTenantManager()

    def __str__(self):
        return self.flat.__str__() + " " + self.tenant.__str__()


class FavouriteFlat(models.Model):
    flat = models.ForeignKey(Flat, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    favourite = models.BooleanField(default=False, db_index=True)
    objects = FavouriteFlatManager()

    def __str__(self):
        return self.flat.__str__() + " " + self.user.__str__()

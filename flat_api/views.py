# Views file for flat_api app
import json

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.views import APIView

from core import log_data
from flat_api import serializers
from flat_api.services import FlatManagementServices
from flatmates_backend import responses

flat_management_service = FlatManagementServices()



class AddFlat(APIView):
    """
    For adding flat
    """

    @swagger_auto_schema(request_body=serializers.AddFlatSerializer)
    def post(self, request):
        try:
            return flat_management_service.add_flat(user=request.user,
                                                    data=request.data,
                                                    files=request.FILES)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)


class ViewFlat(APIView):
    """
    For detailed view of a single flat
    """

    def get(self, request, pk):
        try:
            log_data.warning(f'enter the valid flat id')
            return flat_management_service.view_flat(id=pk)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)


class EditFlat(APIView):
    """
    For updating the existing flat
    """

    @swagger_auto_schema(request_body=serializers.UpdateFlatDetailsSerializer)
    def patch(self, request, pk):
        try:
            return flat_management_service.update_flat(user=request.user,
                                                       data=request.data,
                                                       id=pk,
                                                       files=request.FILES)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)


class RemoveFlat(APIView):
    """
    For removing the existing flat
    """

    # API Documentation for Flat DELETE API
    @swagger_auto_schema(
        operation_id='Delete Flat',
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                'pk': openapi.Schema(
                    type=openapi.TYPE_INTEGER,
                )
            }
        ),
    )
    def delete(self, request, pk):
        try:
            return flat_management_service.remove_flat(user=request.user,
                                                       id=pk)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)

    # API Documentation for remove ad from public
    @swagger_auto_schema(
        operation_id='Remove Flat Ad',
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                'pk': openapi.Schema(
                    type=openapi.TYPE_INTEGER,
                )
            }
        ),
    )
    def patch(self, request, pk):
        try:
            return flat_management_service.remove_ad(user=request.user,
                                                     id=pk)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)


class ListFlat(APIView):
    """
    List down all the flats based on search data
    """

    def get(self, request):
        try:
            filter_data = {}
            if 'data' in request.GET:
                filter_data = json.loads(request.GET.get('data'))
            return flat_management_service.list_flat(
                data=filter_data)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)


class MyFlatList(APIView):
    """
    For list the flats that are added by the logged in user
    """

    def get(self, request):
        try:
            return flat_management_service.my_flat_list(user=request.user)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)


class SeekerRequestToJoin(APIView):
    """
    For request from seeker to join the flat
    """

    @swagger_auto_schema(request_body=serializers.CreateSeekerRequestSerializer)
    def post(self, request):
        try:
            return flat_management_service.seeker_request_for_flat(user=request.user,
                                                                   data=request.data)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)


class SeekerRequestList(APIView):
    """
    To get the seeker request list to the flat owner
    """

    def get(self, request):
        try:
            return flat_management_service.get_request_list(user=request.user)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)


class AcceptSeekerRequest(APIView):
    """
    For accepting seeker request by flat ad owner
    """

    @swagger_auto_schema(
        operation_id='Accept Request',
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                'pk': openapi.Schema(
                    type=openapi.TYPE_INTEGER,
                )
            }
        ),
    )
    def post(self, request, pk):
        try:
            return flat_management_service.accept_seeker_request(user=request.user, id=pk)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)


class RejectSeekerRequest(APIView):
    """
    For rejecting seeker request by flat ad owner
    """
    @swagger_auto_schema(
        operation_id='Reject Request',
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                'pk': openapi.Schema(
                    type=openapi.TYPE_INTEGER,
                )
            }
        ),
    )
    def post(self, request, pk):
        try:
            return flat_management_service.reject_seeker_request(user=request.user, id=pk)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)


class CancelSeekerRequest(APIView):
    """
    For cancelling the join flat request by seeker
    """

    @swagger_auto_schema(
        operation_id='Accept Request',
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
            }
        ),
    )
    def post(self, request):
        try:
            return flat_management_service.cancel_seeker_request(user=request.user)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)


class ExitFlat(APIView):
    """
    Exit from a flat
    """

    @swagger_auto_schema(
        operation_id='Exit Flat',
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
            }
        ),
    )
    def delete(self, request):
        try:
            return flat_management_service.exit_flat(user=request.user)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)


class MyFlatDetails(APIView):
    """
    For details of the flats that are added by the logged in user
    """

    def get(self, request):
        try:
            return flat_management_service.my_flat_details(user=request.user)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)


class DwellerAddSeeker(APIView):
    """
    For request from dweller to join the flat
    """
    @swagger_auto_schema(request_body=serializers.CreateDwellerRequestSerializer)
    def post(self, request):
        try:
            return flat_management_service.dweller_add_seeker(user=request.user,
                                                              data=request.data)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)


class FlatmateList(APIView):
    """
    List down all the flats based on search data
    """

    def get(self, request):
        try:
            return flat_management_service.list_flatmates(user=request.user,
                                                          search_param=request.GET.get('search_param'))
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)


class RemoveTenant(APIView):
    """
    For removing tenant from the flat by admin
    """

    @swagger_auto_schema(
        operation_id='Remove Tenant From Flat',
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                'pk': openapi.Schema(
                    type=openapi.TYPE_INTEGER,
                )
            }
        ),
    )
    def delete(self, request, pk):
        try:
            return flat_management_service.remove_tenant(user=request.user,
                                                         id=pk)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)


class CheckFlat(APIView):
    """
    check a user have active flat
    """

    def get(self, request):
        try:
            return flat_management_service.user_have_flat(user=request.user)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)


class FavouriteFlat(APIView):
    """
    for shortlisting flats
    """

    def post(self, request, flat_pk):
        try:
            return flat_management_service.set_favourite_flat(user=request.user,
                                                              id=flat_pk)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)

    def get(self, request):
        try:
            return flat_management_service.favourite_flat_list(user=request.user)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)


class ChangeFlatAdmin(APIView):

    @swagger_auto_schema(
        operation_id='Change flat admin',
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                'pk': openapi.Schema(
                    type=openapi.TYPE_INTEGER,
                )
            }
        ),
    )
    def post(self, request, pk):
        try:
            return flat_management_service.change_flat_admin(user=request.user,
                                                             flat_id=pk,
                                                             data=request.data)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)

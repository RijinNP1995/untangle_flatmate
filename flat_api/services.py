from django.db.models import Q
from django.db.models.expressions import F

from core import log_data
from flat_api import models as flat_model
from flat_api import serializers
from flatmates_backend import responses
from flatmates_backend import responses as message
from master import models as master_model
from notifications.services import notify
from user.services import UserManagementServices

user_management_service = UserManagementServices()


class FlatManagementServices:
    def add_flat(self, user, data, files):
        """
        method to add flat
        :param user: logged in user info
        :param data: data to create an ad for flatmates
        :param files: images of flat
        :return: status
        """
        if user:
            if flat_model.FlatTenant.objects.get_active_tenants(tenant=user):
                log_data.error(f'{user} user has already joined in another flat')
                return message.error_409('This user has already joined in another flat')
            else:
                serializer = serializers.AddFlatSerializer(data=data)
                # log_data.warning('enter the valid serializer payload')
                if serializer.is_valid():
                    flat_obj = serializer.save(created_by=user)
                    if data.get('currency'):
                        currency_data = {'flat': flat_obj.id, 'currency': data.get('currency')}
                        currency_data_ser = serializers.FlatCurrencySerializer(data=currency_data, many=False)
                        if currency_data_ser.is_valid(raise_exception=True):
                            currency_data_ser.save()
                    if data.get('preference_length'):
                        preference_length = int(data.get('preference_length'))
                        if preference_length > 0:
                            preference_list = [
                                {'flat': flat_obj.id, 'preference': data.get('preference[' + str(i) + ']')}
                                for i in range(preference_length)]
                            flat_preference_ser = serializers.FlatPreferenceListSerializer(data=preference_list,
                                                                                           many=True)
                            if flat_preference_ser.is_valid():
                                flat_preference_ser.save()
                                # log_data.debug(f'{flat_preference_ser} preferences set successfully')
                    if data.get('amenities_length'):
                        amenities_length = int(data.get('amenities_length'))
                        if amenities_length > 0:
                            amenity_list = [{'flat': flat_obj.id, 'amenity': data.get('amenity[' + str(i) + ']')}
                                            for i in range(amenities_length)]
                            flat_amenity_ser = serializers.FlatAmenityListSerializer(data=amenity_list, many=True)
                            if flat_amenity_ser.is_valid():
                                flat_amenity_ser.save()
                                # log_data.debug(f'{flat_amenity_ser} amenities added successfully')
                    if files:
                        image_list = [{'flat': flat_obj.id,
                                       'file': files['image[' + str(i) + ']']} for i in range(files.__len__())]
                        flat_file_ser = serializers.FlatFileListSerializer(data=image_list, many=True)
                        if flat_file_ser.is_valid():
                            flat_file_ser.save()
                            log_data.debug(f'{flat_file_ser} flat images added successfully')
                    if data.get('age_group_length'):
                        age_group_length = int(data.get('age_group_length'))
                        if age_group_length > 0:
                            age_group_list = [{'flat': flat_obj.id, 'age_group': data.get('age_group[' + str(i) + ']')}
                                              for i in range(age_group_length)]
                            age_group_list_ser = serializers.FlatAgeGroupListSerializer(data=age_group_list, many=True)
                            if age_group_list_ser.is_valid():
                                age_group_list_ser.save()
                                # log_data.debug(f'{age_group_list_ser} add the age group data successfully')
                    flat_model.FlatTenant.objects.create(flat=flat_obj,
                                                         tenant=user,
                                                         is_admin=True)
                    flat = flat_model.Flat.objects.get_by_id(pk=flat_obj.id)
                    flat_ser = serializers.FlatDetailsSerializer(flat)
                    # log_data.info(f'add the flat details successfully')
                    return message.add_flat(flat_id=flat_obj.id, data=flat_ser.data)
                log_data.error(f'{serializer.errors} invalid data')
                return message.invalid_data(serializer)
        log_data.error(f'user does not exist')
        return message.invalid_user()

    def view_flat(self, id):
        """
        To get the details of a single flat
        :param id: flat ad id
        :return: flat complete details
        """
        if flat_model.Flat.objects.pk_does_exist(id=id):
            flat = flat_model.Flat.objects.get_by_id(pk=id)
            flat_ser = serializers.FlatDetailsSerializer(flat)
            log_data.info(f'flat details fetched successfully')
            return responses.fetch_data_response(model_info='Flat', data=flat_ser.data)
        log_data.error(f'flat does not exist')
        return message.flat_does_not_exist()

    def update_flat(self, user, data, id, files):
        """
        Edit the existing ad of flat
        :param user: logged in user
        :param data: data to update
        :param id: id of flat ad
        :param files: images of files
        :return: data  and status
        """
        if user:
            flat_obj = flat_model.Flat.objects.pk_does_exist(id=id)
            # log_data.warning(f'enter the valid flat id')
            if flat_obj:
                flat_obj = flat_model.Flat.objects.get_by_id(pk=id)
                if user == flat_obj.created_by:
                    serializer = serializers.UpdateFlatDetailsSerializer(data=data, partial=True)
                    if serializer.is_valid():
                        flat_obj = serializer.update(instance=flat_model.Flat.objects.get_by_id(pk=id),
                                                     validated_data=serializer.validated_data)
                        flat_obj.modified_by = user
                        flat_obj.save()
                        log_data.debug(f'{serializer.data}')
                        if data.get('currency'):
                            if flat_model.FlatCurrency.objects.get_currency(flat=flat_obj).exists():
                                flat_model.FlatCurrency.objects.get_currency(flat=flat_obj).delete()
                                currency_data = {'flat': flat_obj.id, 'currency': data.get('currency')}
                                currency_data_ser = serializers.FlatCurrencySerializer(data=currency_data, many=False)
                                if currency_data_ser.is_valid(raise_exception=True):
                                    currency_data_ser.save()
                        if data.get('amenities_length'):
                            amenities_length = int(data.get('amenities_length'))
                            if amenities_length > 0:
                                if flat_model.FlatAmenity.objects.get_amenity(flat=flat_obj).exists():
                                    flat_model.FlatAmenity.objects.get_amenity(flat=flat_obj).delete()
                                amenity_list = [
                                    {'flat': flat_obj.id, 'amenity': data.get('amenity[' + str(i) + ']')}
                                    for i in range(amenities_length)]
                                flat_amenity_ser = serializers.FlatAmenityListSerializer(data=amenity_list,
                                                                                         many=True)
                                if flat_amenity_ser.is_valid():
                                    flat_amenity_ser.save()
                                log_data.debug(f'{flat_amenity_ser.data} flat amenities added successfully')
                        if data.get('age_group_length'):
                            age_group_length = int(data.get('age_group_length'))
                            if age_group_length > 0:
                                if flat_model.FlatAgeGroup.objects.get_age_group(flat=flat_obj).exists():
                                    flat_model.FlatAgeGroup.objects.get_age_group(flat=flat_obj).delete()
                                age_group_list = [
                                    {'flat': flat_obj.id, 'age_group': data.get('age_group[' + str(i) + ']')}
                                    for i in range(age_group_length)]
                                age_group_list_ser = serializers.FlatAgeGroupListSerializer(data=age_group_list,
                                                                                            many=True)
                                if age_group_list_ser.is_valid():
                                    age_group_list_ser.save()
                                    # log_data.debug(f'{age_group_list_ser} add the age group data successfully')
                        if data.get('preference_length'):
                            preference_length = int(data.get('preference_length'))
                            if preference_length > 0:
                                if flat_model.FlatPreference.objects.get_preference(flat=flat_obj).exists():
                                    flat_model.FlatPreference.objects.get_preference(flat=flat_obj).delete()
                                preference_list = [
                                    {'flat': flat_obj.id, 'preference': data.get('preference[' + str(i) + ']')}
                                    for i in range(preference_length)]
                                flat_preference_ser = serializers.FlatPreferenceListSerializer(data=preference_list,
                                                                                               many=True)
                                if flat_preference_ser.is_valid():
                                    flat_preference_ser.save()
                                    # log_data.debug(f'{flat_preference_ser} preferences set successfully')
                        if files:
                            if flat_model.FlatFile.objects.get_files(flat=flat_obj).exists():
                                flat_model.FlatFile.objects.get_files(flat=flat_obj).delete()
                            image_list = [{'flat': flat_obj.id,
                                           'file': files['image[' + str(i) + ']']} for i in range(files.__len__())]
                            flat_file_ser = serializers.FlatFileListSerializer(data=image_list, many=True)
                            if flat_file_ser.is_valid():
                                flat_file_ser.save()
                                log_data.debug(f'{flat_file_ser.data} flat images added successfully')
                        flat_ser = serializers.FlatDetailsSerializer(flat_obj)
                        log_data.info(f'{flat_ser.data} updated the flat data successfully')
                        # notify(user=user, actor=flat_obj, verb='updated successfully', action='',
                        #        target='', description='')
                        return message.update_flat_data(flat_ser)
                    log_data.error(f'{serializer.errors} invalid serializer')
                    return message.invalid_data(serializer)
                log_data.error(f'user does not have the permission to update details')
                return message.permission_denied()
            log_data.error(f'flat does not exist')
            return message.flat_does_not_exist()
        log_data.error(f'user does not exist')
        return message.invalid_user()

    def remove_flat(self, user, id):
        """
        For removing an existing flat
        :param user: logged in user
        :param id: flat id
        :return: status
        """
        if user:
            if flat_model.Flat.objects.pk_does_exist(id=id):
                flat_obj = flat_model.Flat.objects.get_by_id(pk=id)
                if user == flat_obj.created_by:
                    if flat_model.FlatTenant.objects.get_active_tenants(flat=flat_obj).__len__() > 1:
                        log_data.error(f'{flat_obj} can not remove now - more active tenants on this flat')
                        return message.error_400(message='More than one active tenants on this flat')
                    if flat_model.FlatmateCommunity.objects.get_active_pending_request(flat=flat_obj,
                                                                                       status='Pending').__len__() > 0:
                        log_data.error(f'{flat_obj} can not remove now - active requests are pending')
                        return message.error_400(message='Active request is pending.')
                    if flat_model.FlatTenant.objects.get_active_tenants(tenant=user).exists():
                        tenant_obj = flat_model.FlatTenant.objects.get_active_tenants(tenant=user)[0]
                        tenant_obj.is_active = False
                        tenant_obj.is_delete = True
                        tenant_obj.save()
                    flat_obj.modified_by = user
                    flat_obj.is_delete = True
                    flat_obj.is_active = False
                    flat_obj.save()
                    log_data.info(f'flat data removed successfully')
                    return message.remove_flat()
                log_data.error(f'permission to remove the flat denied')
                return message.permission_denied()
            log_data.error(f'flat does not exist')
            return message.flat_does_not_exist
        log_data.error(f'user does not exist')
        return message.invalid_user

    def list_flat(self, data):
        """
        List the flat based on search params
        :param data: search params
        :return: list of flats
        """
        flat_list = self.get_flat_list(filter_data=data)
        if data.get('screen'):
            if data.get('screen') == 'HOME':
                flat_list = flat_list[:4]
        flat_ser = serializers.FlatDetailsSerializer(flat_list, many=True)
        log_data.info(f'flat list fetched successfully')
        return message.load_flat(flat_ser)

    def my_flat_list(self, user):
        """
        To list the flats that are added by logged in user
        :param user: logged in user
        :return: list of flats
        """
        flat_list = flat_model.Flat.objects.get_by_filter(created_by=user)
        flat_ser = serializers.FlatListSerializer(flat_list, many=True)
        log_data.info(f'{flat_ser.data}listed the flats that are added by logged in user successfully')
        return message.load_flat(flat_ser)

    def get_flat_list(self, filter_data):
        """
        Apply search params for the flats
        :param filter_data: search params
        :return: filtered query set of flat
        """
        flat_list = flat_model.Flat.objects.get_all_active()
        if filter_data.get('radius'):
            flat_list = self.get_flat_nearby_location(flat_list=flat_list,
                                                      radius_km=filter_data.get('radius'),
                                                      current_latitude=filter_data.get('current_lat'),
                                                      current_longitude=filter_data.get('current_lon'))

        if filter_data.get('search_param'):
            qs = (Q(flat_no__icontains=filter_data.get('search_param'))
                  | Q(flat_name__icontains=filter_data.get('search_param'))
                  | Q(landmark__icontains=filter_data.get('search_param'))
                  | Q(city__icontains=filter_data.get('search_param'))
                  | Q(rent_per_month__icontains=filter_data.get('search_param'))
                  | Q(expected_gender__icontains=filter_data.get('search_param'))
                  | Q(flatamenity__amenity__label__icontains=filter_data.get('search_param'))
                  | Q(flatpreference__preference__label__icontains=filter_data.get('search_param'))
                  | Q(flatagegroup__age_group__label__icontains=filter_data.get('search_param')))
            flat_list = flat_list.filter(qs)
        if filter_data.get('min_rent'):
            flat_list = flat_list.filter(rent_per_month__gte=filter_data.get('min_rent'))
        if filter_data.get('max_rent'):
            flat_list = flat_list.filter(rent_per_month__lte=filter_data.get('max_rent'))
        if filter_data.get('gender'):
            flat_list = flat_list.filter(expected_gender=filter_data.get('gender'))
        if filter_data.get('preferences'):
            flat_list = flat_list.filter(flatpreference__preference__id__in=filter_data.get('preferences'))
        if filter_data.get('amenities'):
            flat_list = flat_list.filter(flatamenity__amenity__id__in=filter_data.get('amenities'))
        if filter_data.get('age_group'):
            flat_list = flat_list.filter(flatagegroup__age_group__id__in=filter_data.get('age_group'))
        return flat_list.distinct()

    def get_flat_nearby_location(self, flat_list, radius_km, current_latitude, current_longitude):
        """
        Return objects sorted by distance to specified coordinates
        which distance is less than max_distance given in kilometers
        """
        qs = flat_list.annotate(
            radius_sqr=pow(F('lat') - current_latitude, 2) + pow(F('lon') - current_longitude, 2)
        ).filter(
            radius_sqr__lte=pow(radius_km / 11, 2)
        )
        return qs

    def seeker_request_for_flat(self, user, data):
        """
        seeker request to join a flat
        :param user: logged in user
        :param data: flat details
        :return: status
        """
        if user:
            serializer = serializers.CreateSeekerRequestSerializer(data=data)
            if serializer.is_valid():
                request_obj = serializer.save(status="Pending", created_by=user)
                # log_data.debug(f'{serializer.data}')
                notify(user=request_obj.flat.created_by, actor=user.display_name,
                       verb='has sent you a flatmate request',
                       action=request_obj.flat.id,
                       notification_type='NEW_REQ', description='',
                       icon=master_model.NotificationIcon.objects.get(label='FLAT'),
                       title='New Request')
                log_data.info(f'send the request for the flat access')
                return message.sent_request()
            log_data.error(f'{serializer.errors} invalid data entered')
            return message.invalid_data(serializer)
        log_data.error(f'user does not exist')
        return message.invalid_user()

    def get_request_list(self, user):
        """
        get the request list to dweller
        :param user: logged in user
        :return: request list
        """
        if user:
            if flat_model.FlatTenant.objects.get_active_tenants(tenant=user, is_admin=True).exists():
                flat_admin = flat_model.FlatTenant.objects.get_active_tenants(tenant=user, is_admin=True)[0]
                request_list = flat_model.FlatmateCommunity.objects.get_by_filter(flat__id=flat_admin.flat.id,
                                                                                  status="Pending",
                                                                                  is_delete=False)
                request_list_ser = serializers.SeekerRequestListSerializer(request_list, many=True)
                log_data.info(f'{request_list_ser.data} request list loaded successfully')
                return message.load_request_list(request_list_ser)
        log_data.error(f'user does not exist')
        return message.invalid_user()

    def accept_seeker_request(self, user, id):
        """
        accept the seeker request
        :param user: logged in user
        :param id: request id
        :return: status
        """
        if user:
            if flat_model.FlatmateCommunity.objects.pk_does_exist(id=id):
                request_obj = flat_model.FlatmateCommunity.objects.get_by_id(pk=id)
                log_data.warning(f'user and dweller must be same.')
                # if user != request_obj.flat.created_by:
                #     raise serializers.ValidationError("User and dweller must be same. ")
                request_obj.modified_by = user
                request_obj.status = "Accept"
                request_obj.is_active = False
                request_obj.is_delete = True
                request_obj.save()
                flat_model.FlatTenant.objects.create(flat=request_obj.flat,
                                                     tenant=request_obj.seeker)
                flat = flat_model.Flat.objects.get_by_id(pk=request_obj.flat.pk)
                flat_ser = serializers.FlatDetailsSerializer(flat)
                notify(user=request_obj.seeker, actor=user.display_name,
                       verb='has accepted your flatmate request ',
                       action=request_obj.flat.id,
                       notification_type='REQ_RES_ACCEPT', description='',
                       icon=master_model.NotificationIcon.objects.get(label='FLAT'),
                       title="Request Accept",
                       details=flat_ser.data)
                log_data.info(f'request accepted successfully')
                return message.accept_request()
            log_data.error(f'invalid request id')
            return message.invalid_request_id()
        log_data.error(f'user does not exist')
        return message.invalid_user()

    def reject_seeker_request(self, user, id):
        """
        reject the seeker request
        :param user: logged in user
        :param id: request id
        :return: status
        """
        if user:
            if flat_model.FlatmateCommunity.objects.pk_does_exist(id=id):
                request_obj = flat_model.FlatmateCommunity.objects.get_by_id(pk=id)
                log_data.warning(f'User and dweller must be same.')
                # if user != request_obj.flat.created_by:
                #     raise serializers.ValidationError("User and dweller must be same. ")
                request_obj.modified_by = user
                request_obj.status = "Reject"
                request_obj.is_active = False
                request_obj.is_delete = True
                request_obj.save()
                flat = flat_model.Flat.objects.get_by_id(pk=request_obj.flat.pk)
                flat_ser = serializers.FlatDetailsSerializer(flat)
                notify(user=request_obj.seeker, actor=user.display_name,
                       verb='has rejected your flatmate request ',
                       action=request_obj.flat.id,
                       notification_type='REQ_RES_REJECT', description='',
                       icon=master_model.NotificationIcon.objects.get(label='FLAT'),
                       title="Request Reject",
                       details=flat_ser.data)
                log_data.info(f'reject request successfully')
                return message.reject_request()
            log_data.error(f'invalid request id')
            return message.invalid_request_id()
        log_data.error(f'user does not exist')
        return message.invalid_user()

    def cancel_seeker_request(self, user):
        """
        cancel the request by seeker
        :param user: logged in user
        :param id: request id
        :return: status
        """
        if user:
            if flat_model.FlatmateCommunity.objects. \
                    get_active_pending_request(created_by=user, status='Pending').exists():
                request_obj = flat_model.FlatmateCommunity.objects. \
                    get_active_pending_request(created_by=user, status='Pending')[0]
                if user != request_obj.created_by:
                    log_data.error(f'user does not exist')
                    return message.invalid_user()
                request_obj.modified_by = user
                request_obj.status = "Cancel"
                request_obj.is_active = False
                request_obj.is_delete = True
                request_obj.save()
                log_data.info(f'seeker request cancelled successfully')
                return message.cancel_request()
            log_data.error(f'invalid request id')
            return message.invalid_request_id()
        log_data.error(f'user does not exist')
        return message.invalid_user()

    def exit_flat(self, user):
        """
        exit from a joined flat
        :param user: logged in user
        :return: status
        """
        if user:
            if flat_model.FlatTenant.objects.get_active_tenants(tenant=user).exists():
                tenant_obj = flat_model.FlatTenant.objects.get_active_tenants(tenant=user)[0]
                tenant_obj.is_active = False
                tenant_obj.is_delete = True
                tenant_obj.save()
                log_data.info(f'exit flat successfully')
                flat = flat_model.Flat.objects.get_by_id(pk=tenant_obj.flat.pk)
                flat_ser = serializers.FlatDetailsSerializer(flat)
                if user != tenant_obj.flat.created_by:
                    notify(user=tenant_obj.flat.created_by, actor=user.display_name,
                           verb='has exited from your flat. ',
                           action=tenant_obj.flat.id,
                           notification_type='EXIT_FLAT', description='',
                           icon='',
                           title="Exit Flat",
                           details=flat_ser.data)
                return message.exit_flat()
            log_data.error(f'invalid user')
            return message.invalid_request_id()
        log_data.error(f'user does not exist')
        return message.invalid_user()

    def my_flat_details(self, user):
        """
        For the details of the flats that are added by logged in user
        :param user: logged in user
        :return: flat details
        """
        if user:
            if flat_model.FlatTenant.objects.get_active_tenants(tenant=user).exists():
                flat_tenant_obj = flat_model.FlatTenant.objects.get_active_tenants(tenant=user)[0]
                if flat_model.Flat.objects.pk_does_exist(id=flat_tenant_obj.flat.id):
                    flat_obj = flat_model.Flat.objects.get_by_id(pk=flat_tenant_obj.flat.id)
                    flat_ser = serializers.FlatDetailsSerializer(flat_obj)
                    log_data.info(f'My flats details fetched by logged in user {user} successfully')
                    return message.fetch_data_response(model_info='Flat', data=flat_ser.data)
                log_data.error(f'flat does not exist')
                return message.error_400(f'flat does not exist')
            log_data.error(f'flat does not added by this user {user}')
            return message.error_400(f'flat does not added by this user {user}')
        log_data.error(f'user does not exist')
        return message.invalid_user()

    def dweller_add_seeker(self, user, data):
        """
        dweller request add to a flat
        :param user: logged in user
        :param data: flat details
        :return: status
        """
        if user:
            serializer = serializers.CreateDwellerRequestSerializer(data=data)
            if serializer.is_valid():
                request_obj = serializer.save(status="Accept",
                                              is_active=False,
                                              is_delete=True,
                                              created_by=user)
                flat_model.FlatTenant.objects.create(flat=request_obj.flat,
                                                     tenant=request_obj.seeker)
                flat = flat_model.Flat.objects.get_by_id(pk=data.get('flat'))
                flat_ser = serializers.FlatDetailsSerializer(flat)
                notify(user=request_obj.seeker, actor=user.display_name,
                       verb='has added you as flatmate ',
                       action=request_obj.flat.id,
                       notification_type='REQ_RES_ACCEPT', description='',
                       icon=master_model.NotificationIcon.objects.get(label='FLAT'),
                       title="Added as Flatmate",
                       details=flat_ser.data)
                log_data.debug(f'flat owner send request ')
                log_data.info(f'send the request to the flat access')
                return message.success_message(
                    message='You have added ' + request_obj.seeker.display_name + ' as flatmate.',
                    data=flat_ser.data)
            log_data.error(f'{serializer.errors} invalid data entered')
            return message.invalid_data(serializer)
        log_data.error(f'user does not exist')
        return message.invalid_user()

    def list_flatmates(self, user, search_param):
        """
        List the flatmates based on search params
        :param user: user
        :param search_param: search params
        :return: list of flatmates
        """
        if user:
            if flat_model.FlatTenant.objects.get_tenant(tenant=user).exists():
                tenant_obj = flat_model.FlatTenant.objects.get_active_tenants(tenant=user)[0]
                flatmates_list = flat_model.FlatTenant.objects.get_active_tenants(flat=tenant_obj.flat)
                if search_param:
                    flatmates_list = flatmates_list.filter(tenant__display_name__icontains=search_param)
                flatmate_list_ser = serializers.ViewTenantDetailSerializer(flatmates_list, many=True)
                log_data.info(f'flatmate details fetched successfully')
                return responses.fetch_data_response(model_info='Flatmates', data=flatmate_list_ser.data)
            log_data.error(f'invalid user')
            return message.invalid_request_id()
        log_data.error(f'user does not exist')
        return message.invalid_user()

    def remove_tenant(self, user, id):
        """
        remove tenant from flat by admin
        :param id: flat-tenant model id
        :param user: logged in user[flat admin]
        :return: status
        """
        if user:
            if flat_model.FlatTenant.objects.get_active_tenants(id=id).exists():
                tenant_obj = flat_model.FlatTenant.objects.get(id=id)
                if user == tenant_obj.flat.created_by:
                    tenant_obj.is_active = False
                    tenant_obj.is_delete = True
                    tenant_obj.save()
                    flat = flat_model.Flat.objects.get_by_id(pk=tenant_obj.flat.id)
                    flat_ser = serializers.FlatDetailsSerializer(flat)
                    notify(user=tenant_obj.tenant, actor=user.display_name,
                           verb='has removed from flat ',
                           action=tenant_obj.flat.id,
                           notification_type='REMOVED_TENANT', description='',
                           icon='',
                           title="Removed from flat",
                           details=flat_ser.data)
                    log_data.info(f'Removed from flat successfully')
                    return message.success_message(message='Removed from flat successfully',
                                                   data=flat_ser.data)
                log_data.error(f'Flat owner and user must be same')
                return message.error_400('Flat owner and user must be same')
            log_data.error(f'invalid user')
            return message.invalid_request_id()
        log_data.error(f'user does not exist')
        return message.invalid_user()

    def user_have_flat(self, user):
        """
        check user  have flat or active flat request
        :param user:
        :return:
        """
        if user:
            have_flat = False
            if flat_model.FlatTenant.objects.get_active_tenants(tenant=user).exists():
                have_flat = True
                flat_tenant_obj = flat_model.FlatTenant.objects.get_active_tenants(tenant=user)[0]
                flat_ser = serializers.FlatDetailsSerializer(flat_tenant_obj.flat)
                return responses.check_user_have_flat(message="data fetched", flat_value=have_flat, data=flat_ser.data)
            active_request, flat_id = user_management_service.check_user_have_active_request(user=user.id)
            if active_request:
                return responses.check_user_have_flat(message="No data fetched", flat_value=have_flat, data={},
                                                      active_request=active_request, flat_id=flat_id)
            return responses.check_user_have_flat(message="No data fetched", flat_value=have_flat, data={})
        log_data.error(f'user does not exist')
        return message.invalid_user()

    def set_favourite_flat(self, user, id):
        """

        :param id: flat-tenant model id
        :param user: logged in user[flat admin]
        :return: status
        """
        if user.id is not None:
            flat_obj = flat_model.Flat.objects.get_by_id(pk=id)
            if flat_model.FavouriteFlat.objects.filter(flat=flat_obj, user=user).exists():
                flat_model.FavouriteFlat.objects.filter(flat=flat_obj, user=user).delete()
                msg_content = 'Flat removed from favourites.'
            else:
                flat_model.FavouriteFlat.objects.create(flat=flat_obj,
                                                        user=user,
                                                        favourite=True)
                msg_content = 'Flat added to favourites.'
            log_data.info(msg_content)
            return message.success_message(message=msg_content)
        log_data.error(f'user does not exist')
        return message.invalid_user()

    def favourite_flat_list(self, user):
        """
        :param user: logged in user[flat admin]
        :return: status
        """
        if user.id is not None:
            fav_flat_list = flat_model.FavouriteFlat.objects.filter(user=user)
            fav_flat_list_ser = serializers.FavouriteFlatSerializer(fav_flat_list, many=True)
            log_data.info('favourite flat list fetched')
            return message.fetch_data_response(model_info='Favourite flat',
                                               data=fav_flat_list_ser.data)
        log_data.error(f'{user} user does not exist')
        return message.invalid_user()

    def remove_ad(self, user, id):
        """
        :param user: logged in user
        :param id: flat id
        :return: status
        """
        if user.id is not None:
            if flat_model.Flat.objects.pk_does_exist(id=id):
                flat_obj = flat_model.Flat.objects.get_by_id(pk=id)
                if flat_model.FlatTenant.objects.get_active_tenants(tenant=user,
                                                                    flat=flat_obj,
                                                                    is_admin=True).exists():
                    flat_obj.display_public = not flat_obj.display_public
                    flat_obj.modified_by = user
                    flat_obj.save()
                    log_data.info('successfully changed the public status.')
                    return message.success_message(message='successfully changed the public status.')
                log_data.error(f'{user}  does not have permission.')
                return message.permission_denied()
            log_data.error(f'flat {id} does not exist')
            return message.flat_does_not_exist
        log_data.error(f'{user} user does not exist')
        return message.invalid_user()

    def change_flat_admin(self, user, flat_id, data):
        """
        :param data:
        :param flat_id:
        :param user: logged in user[flat admin]
        :return: status
        """
        if user.id is not None:
            if flat_model.Flat.objects.pk_does_exist(id=flat_id):
                flat = flat_model.Flat.objects.get_by_id(pk=flat_id)
                if flat_model.FlatTenant.objects.get_active_tenants(tenant=user, is_admin=True, flat=flat).exists():
                    new_admin_obj = flat_model.FlatTenant.objects.get(tenant__id=data.get('user_id'))
                    old_admin_obj = flat_model.FlatTenant.objects.get(tenant=user)
                    old_admin_obj.is_admin = False
                    new_admin_obj.is_admin = True
                    old_admin_obj.save()
                    new_admin_obj.save()
                    log_data.info(f'{user} changed admin')
                    return message.success_message('Success fully changed admin.')
                log_data.error(f'user does not have the permission to update details')
                return message.permission_denied()
            log_data.error(f'flat does not exist')
            return message.flat_does_not_exist()
        log_data.error(f'{user} user does not exist')
        return message.invalid_user()

# URL for flat_api app
from django.urls import path

from flat_api import views

urlpatterns = [
    path('add-flat/', views.AddFlat.as_view(), name='add_flat'),
    path('view-flat/<int:pk>/', views.ViewFlat.as_view(), name="view-flat"),
    path('edit-flat/<int:pk>/', views.EditFlat.as_view(), name="edit-flat"),
    path('remove-flat/<int:pk>/', views.RemoveFlat.as_view(), name="remove-flat"),
    path('list-flat/', views.ListFlat.as_view(), name="list-flat"),
    path('my-flat-list/', views.MyFlatList.as_view(), name="my-flat-list"),
    path('seeker-request-to-join/', views.SeekerRequestToJoin.as_view(), name="request-to-join"),
    path('view-seeker-request/', views.SeekerRequestList.as_view(), name="seeker-request-list"),
    path('accept-seeker-request/<int:pk>/', views.AcceptSeekerRequest.as_view(), name="accept-seeker-request"),
    path('reject-seeker-request/<int:pk>/', views.RejectSeekerRequest.as_view(), name="reject-seeker-request"),
    path('cancel-seeker-request/', views.CancelSeekerRequest.as_view(), name="cancel-seeker-request"),
    path('exit-flat/', views.ExitFlat.as_view(), name="exit-flat"),
    path('my-flat-details/', views.MyFlatDetails.as_view(), name="my-flat-list"),
    path('dweller-add-seeker/', views.DwellerAddSeeker.as_view(), name="dweller-add-seeker"),
    path('flatmate-list/', views.FlatmateList.as_view(), name="get-flatmate-list"),
    path('remove-tenant/<int:pk>/', views.RemoveTenant.as_view(), name="remove-tenant"),
    path('check-flat/', views.CheckFlat.as_view(), name="check-flat"),

    # favourite-flats
    path('favourite-flat/<int:flat_pk>/', views.FavouriteFlat.as_view(), name='favourite-flat'),
    path('favourite-flat-list/', views.FavouriteFlat.as_view(), name='favourite-flat-list'),
    path('remove-ad/<int:pk>/', views.RemoveFlat.as_view(), name='remove-ad'),
    path('change-admin/<int:pk>/', views.ChangeFlatAdmin.as_view(), name='change-flat-admin'),
]

from django.contrib import admin

from flat_api import models


@admin.register(models.Flat)
class FlatAdmin(admin.ModelAdmin):
    list_display = ('flat_no', 'flat_name', 'is_active', 'created_by', 'created_on')
    list_display_links = ('flat_no', 'flat_name')


@admin.register(models.FlatAmenity)
class FlatAmenityAdmin(admin.ModelAdmin):
    list_display = ('flat', 'amenity')
    list_display_links = ('flat', 'amenity')


@admin.register(models.FlatFile)
class FlatFileAdmin(admin.ModelAdmin):
    list_display = ('flat', 'file')
    list_display_links = ('flat', 'file')


@admin.register(models.FlatmateCommunity)
class FlatmateCommunityAdmin(admin.ModelAdmin):
    list_display = ('flat', 'seeker', 'status')
    list_display_links = ('flat', 'seeker')


@admin.register(models.FlatPreference)
class FlatPreferenceAdmin(admin.ModelAdmin):
    list_display = ('flat', 'preference')
    list_display_links = ('flat', 'preference')


@admin.register(models.FlatAgeGroup)
class FlatAgeGroupAdmin(admin.ModelAdmin):
    list_display = ('flat', 'age_group')
    list_display_links = ('flat', 'age_group')


@admin.register(models.FlatTenant)
class FlatTenantAdmin(admin.ModelAdmin):
    list_display = ('flat', 'tenant', 'is_active')
    list_display_links = ('flat', 'tenant')


@admin.register(models.FavouriteFlat)
class FavouriteFlatAdmin(admin.ModelAdmin):
    list_display = ('flat', 'user', 'favourite')
    list_display_links = ('flat', 'user')

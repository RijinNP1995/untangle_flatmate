# Views file for feedback app
from drf_yasg.utils import swagger_auto_schema
from rest_framework.views import APIView

from core import log_data
from flatmates_backend import responses
from . import serializers
from .services import FeedbackManagementServices

feedback_management_service = FeedbackManagementServices()


class CreateFeedback(APIView):
    """
    For adding feedback
    """

    @swagger_auto_schema(request_body=serializers.FeedbackSerializer)
    def post(self, request):
        try:
            return feedback_management_service.create_feedback(user=request.user,
                                                               data=request.data)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)

    def get(self, request):
        """
        To get all the feedback
        :param request:
        :return:
        """
        try:
            return feedback_management_service.view_all()
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)

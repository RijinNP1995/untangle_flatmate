from rest_framework import serializers

from . import models as model


class FeedbackSerializer(serializers.ModelSerializer):

    class Meta:
        model = model.Feedback
        fields = '__all__'

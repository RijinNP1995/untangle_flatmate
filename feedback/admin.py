from django.contrib import admin
from django.contrib.admin import DateFieldListFilter

from . import models


@admin.register(models.Feedback)
class FeedbackAdmin(admin.ModelAdmin):
    list_display = ('screen_name', 'device_info', 'feedback_title', 'created_by', 'created_on')
    list_filter = (('created_on', DateFieldListFilter),)


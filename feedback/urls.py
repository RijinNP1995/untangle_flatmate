from django.urls import path

from . import views


urlpatterns = [
    path('create-feedback/', views.CreateFeedback.as_view(), name='create_feedback'),
    path('view-all-feedback/', views.CreateFeedback.as_view(), name='view_all_feedback'),
]

from core import log_data
from flatmates_backend import responses as message
from . import models
from . import serializers


class FeedbackManagementServices:
    def create_feedback(self, user, data):
        ser_obj = serializers.FeedbackSerializer(data=data, )
        if ser_obj.is_valid(raise_exception=True):
            if user.is_anonymous:
                ser_obj.save()
            else:
                ser_obj.save(created_by=user)
            log_data.info(f'Feedback added successfully')
            return message.success_message(message='Thank you for your feedback')
        log_data.error(f'{ser_obj.errors} invalid data')
        return message.error_403(message="Something went wrong")

    def view_all(self):
        """
        view all feedback data
        :return:
        """
        return message.fetch_data_response(model_info='feedback', data=self.get_all_feedback().data)

    def get_all_feedback(self):
        """
        get all feedback data
        :return:
        """
        data = models.Feedback.objects.all().order_by('-created_on')
        ser_obj = serializers.FeedbackSerializer(data, many=True)
        log_data.info(f'Feedback added successfully')
        return ser_obj



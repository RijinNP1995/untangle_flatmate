from django.db import models
from django.utils.translation import ugettext_lazy as _

from feedback.managers import FeedbackManager
from master.models import BaseModel
from user.models import User


class Feedback(models.Model):
    device_info = models.CharField(_('Device Info'), max_length=300, blank=True, null=True)
    feedback_description = models.TextField(_('Feedback Description'), blank=True, null=True)
    screenshot = models.ImageField(blank=True, null=True, upload_to="feedback")
    feedback_title = models.CharField(_('Feedback Title'), max_length=300, blank=True, null=True)
    screen_name = models.CharField(_('Screen Name'), max_length=300, blank=True, null=True)
    created_on = models.DateTimeField(auto_now_add=True, editable=False)
    created_by = models.ForeignKey(User, related_name='%(class)s_createdby', on_delete=models.CASCADE, null=True,
                                   blank=True, editable=False)
    objects = FeedbackManager()

    def __str__(self):
        return f"{self.feedback_title} {self.screen_name} at {self.created_on}"

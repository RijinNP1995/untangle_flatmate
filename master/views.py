from rest_framework.views import APIView

from flatmates_backend import responses
from . import services as master_service


class PreferenceList(APIView):
    def get(self, request):
        try:
            return master_service.preferences_list(self)
        except Exception as e:
            responses.exception_response(e)


class AgeGroupList(APIView):
    def get(self, request):
        try:
            return master_service.age_group_list(self)
        except Exception as e:
            responses.exception_response(e)


class AmenityList(APIView):
    def get(self, request):
        try:
            return master_service.view_amenity_list(self)
        except Exception as e:
            responses.exception_response(e)


class TaskIconList(APIView):
    def get(self, request):
        try:
            return master_service.view_task_icon(self)
        except Exception as e:
            responses.exception_response(e)


class TaskStatusList(APIView):
    def get(self, request):
        try:
            return master_service.view_task_status(self)
        except Exception as e:
            responses.exception_response(e)


class MasterList(APIView):
    def get(self, request):
        try:
            return master_service.master_filter_list(self)
        except Exception as e:
            responses.exception_response(e)


class ServiceCategoryList(APIView):
    def get(self, request):
        try:
            return master_service.service_category_list(self)
        except Exception as e:
            responses.exception_response(e)


class ExpenseCategoryList(APIView):
    def get(self, request):
        try:
            return master_service.expense_category_list(self)
        except Exception as e:
            responses.exception_response(e)


class ExpenseCurrencyList(APIView):
    def get(self, request):
        try:
            return master_service.expense_currency_list(self)
        except Exception as e:
            responses.exception_response(e)

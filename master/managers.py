from flatmates_backend.base_manager import BaseManager


class PreferencesManager(BaseManager):
    pass


class AmenityManager(BaseManager):
    pass


class AgeGroupManager(BaseManager):
    pass


class IconManager(BaseManager):
    pass


class StatusManager(BaseManager):
    pass


class NotificationIconManager(BaseManager):
    pass


class ServiceCategoryManager(BaseManager):
    pass


class ExpenseCategoryManager(BaseManager):
    pass

class CurrencyManager(BaseManager):
    pass
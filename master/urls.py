from django.urls import path

from master import views

urlpatterns = [
    path('view-preference-list/', views.PreferenceList.as_view(), name='preference_list_view'),
    path('view-age-group-list/', views.AgeGroupList.as_view(), name='age_group_list_view'),
    path('view-amenity-list/', views.AmenityList.as_view(), name='amenity_list_view'),
    path('view-task-icon/', views.TaskIconList.as_view(), name='task_icon_view'),
    path('view-task-status/', views.TaskStatusList.as_view(), name='task_status_view'),

    # master-data-list
    path('filter/', views.MasterList.as_view(), name='master_data'),
    path('service-category-list/', views.ServiceCategoryList.as_view(), name='service_category_list_view'),
    path('expense-category-list/', views.ExpenseCategoryList.as_view(), name='expense_category_list'),
    path('currency-list/', views.ExpenseCurrencyList.as_view(), name='expense_currency_list')
]

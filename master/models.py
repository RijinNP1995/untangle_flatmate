from django.db import models
from django.utils.translation import ugettext_lazy as _

from task import managers as task_manager
from user.models import User
from . import managers as master_manager


class BaseModel(models.Model):
    """
    model that will be inherited by all models to add common fields.
    """
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this should be treated as active. '
            'Unselect this instead of deleting.'
        ),
    )
    is_delete = models.BooleanField(_('delete'), default=False)
    modified_on = models.DateTimeField(auto_now=True, blank=True)
    modified_by = models.ForeignKey(User,
                                    related_name='%(class)s_modifiedby', on_delete=models.CASCADE, null=True,
                                    blank=True)
    created_on = models.DateTimeField(auto_now_add=True, editable=False)
    created_by = models.ForeignKey(User, related_name='%(class)s_createdby', on_delete=models.CASCADE, null=False,
                                   editable=False, default=1)

    class Meta:
        abstract = True


class Amenity(BaseModel):
    label = models.CharField(_('label'), max_length=15, unique=True)
    description = models.TextField(_('flatmate description'), blank=True, null=True)
    icon = models.ImageField(upload_to='amenity_icons', default='/icons/amenities-icon.jpg')

    objects = master_manager.AmenityManager()

    def __str__(self):
        return self.label


class Preferences(BaseModel):
    label = models.CharField(_('label'), max_length=20, unique=True)
    description = models.TextField(_('Preference description'), blank=True, null=True)
    icon = models.ImageField(upload_to='preferences_icons', default='/icons/amenities-icon.jpg')

    objects = master_manager.PreferencesManager()

    def __str__(self):
        return self.label


class AgeGroup(BaseModel):
    label = models.CharField(_('label'), max_length=15, unique=True)
    description = models.TextField(_('Age group description'), blank=True, null=True)
    icon = models.ImageField(upload_to='age_group_icons', default='/icons/amenities-icon.jpg')

    objects = master_manager.AgeGroupManager()

    def __str__(self):
        return self.label


class TaskStatus(BaseModel):
    label = models.CharField(_('label'), max_length=15, unique=True)
    task_status = models.CharField(_('task'), max_length=10)
    objects = master_manager.StatusManager()


class TaskIcon(BaseModel):
    label = models.CharField(_('label'), max_length=15, unique=True)
    icon = models.ImageField(upload_to='task_icons')
    objects = master_manager.IconManager()


class TaskBadge(models.Model):
    label = models.CharField(_('badge_name'), max_length=20, unique=True)
    badges = models.ImageField(upload_to='badges')
    badge_value = models.IntegerField('badge_value')
    objects = task_manager.TaskRatingManager()


class NotificationIcon(BaseModel):
    label = models.CharField(_('label'), max_length=25, unique=True)
    icon = models.ImageField(upload_to='notification_icons')
    objects = master_manager.NotificationIconManager()

    def __str__(self):
        if self.label:
            return self.label


class ServiceCategory(BaseModel):
    label = models.CharField(_('label'), max_length=25, unique=True)
    icon = models.ImageField(upload_to='service_category')
    objects = master_manager.ServiceCategoryManager()


class ExpenseCategory(BaseModel):
    name = models.CharField(_('category_name'), max_length=25, unique=True)
    icon = models.ImageField(upload_to='expense_cat_icon', default='icons/bill-icon.png')
    objects = master_manager.ExpenseCategoryManager()


class Currency(BaseModel):
    name = models.CharField(_('currency_name'), max_length=10, unique=True)
    objects = master_manager.CurrencyManager()

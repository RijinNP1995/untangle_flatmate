from rest_framework import serializers

from . import models as master_models


class PreferenceListSerializer(serializers.ModelSerializer):
    class Meta:
        model = master_models.Preferences
        fields = '__all__'


class AgeGroupListSerializer(serializers.ModelSerializer):
    class Meta:
        model = master_models.AgeGroup
        fields = '__all__'


class AmenityListSerializer(serializers.ModelSerializer):
    class Meta:
        model = master_models.Amenity
        fields = '__all__'


class TaskIconsSerializer(serializers.ModelSerializer):
    class Meta:
        model = master_models.TaskIcon
        fields = '__all__'


class TaskStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = master_models.TaskStatus
        fields = '__all__'


class RatingsBadgeSerializer(serializers.ModelSerializer):
    class Meta:
        model = master_models.TaskBadge
        fields = (
            '__all__'
        )


class NotificationIconSerializer(serializers.ModelSerializer):
    class Meta:
        model = master_models.NotificationIcon
        fields = ['label', 'icon']


class ServiceCategoryListSerializer(serializers.ModelSerializer):
    class Meta:
        model = master_models.ServiceCategory
        fields = '__all__'


class ExpenseCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = master_models.ExpenseCategory
        fields = '__all__'


class CurrencyListSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField(max_length=10)


class ExpenseCategoryListSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField(max_length=25)
    icon = serializers.ImageField()

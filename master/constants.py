from enum import Enum


class UserConstants(Enum):
    GENDER_TYPES = [('male', 'MALE'), ('female', 'FEMALE'), ('other', 'OTHER')]

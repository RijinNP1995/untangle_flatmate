from rest_framework import status
from rest_framework.response import Response

from flatmates_backend import responses
from master import models as master_model
from . import serializers as master_serializers


def preferences_list(self):
    return responses.fetch_data_response(model_info='Preference', data=get_preferences_list())


def age_group_list(self):
    return responses.fetch_data_response(model_info='AgeGroup', data=get_age_group_list())


def view_amenity_list(self):
    return responses.fetch_data_response(model_info='Amenity', data=get_amenity_list())


def view_task_icon(self):
    task_icon_data = master_model.TaskIcon.objects.get_all_active()
    task_icon_ser = master_serializers.TaskIconsSerializer(task_icon_data, many=True)
    return responses.fetch_data_response(model_info='TaskIcon', data=task_icon_ser.data)


def view_task_status(self):
    task_status_data = master_model.TaskStatus.objects.get_all_active()
    task_status_ser = master_serializers.TaskStatusSerializer(task_status_data, many=True)
    return responses.fetch_data_response(model_info='TaskStatus', data=task_status_ser.data)


def get_amenity_list():
    amenity_data = master_model.Amenity.objects.get_all_active()
    amenity_list_ser = master_serializers.AmenityListSerializer(amenity_data, many=True)
    return amenity_list_ser.data


def get_preferences_list():
    preference_data = master_model.Preferences.objects.get_all_active()
    preference_list_ser = master_serializers.PreferenceListSerializer(preference_data, many=True)
    return preference_list_ser.data


def get_age_group_list():
    age_group_data = master_model.AgeGroup.objects.get_all_active()
    age_group_list_ser = master_serializers.AgeGroupListSerializer(age_group_data, many=True)
    return age_group_list_ser.data


def master_filter_list(self):
    return Response(
        {
            'status': 200,
            'message': 'Data fetched successfully',
            'amenities': get_amenity_list(),
            'preferences': get_preferences_list(),
            'age_group': get_age_group_list()
        },
        status=status.HTTP_200_OK
    )


def service_category_list(self):
    return responses.fetch_data_response(model_info='Service category', data=get_service_category_list())


def get_service_category_list():
    service_category_data = master_model.ServiceCategory.objects.get_all_active()
    service_category_list_ser = master_serializers.ServiceCategoryListSerializer(service_category_data, many=True)
    return service_category_list_ser.data


def expense_category_list(self):
    return responses.fetch_data_response(model_info='Expense category', data=get_expense_category_list())


def get_expense_category_list():
    service_category_data = master_model.ExpenseCategory.objects.get_all_active()
    expense_category_list_ser = master_serializers.ExpenseCategoryListSerializer(service_category_data, many=True)
    return expense_category_list_ser.data


def expense_currency_list(self):
    return responses.fetch_data_response(model_info='Currency', data=get_expense_currency_list())


def get_expense_currency_list():
    currency_data = master_model.Currency.objects.get_all_active()
    currency_list_ser = master_serializers.CurrencyListSerializer(currency_data, many=True)
    return currency_list_ser.data

from django.contrib import admin

from master import models


@admin.register(models.Amenity)
class AmenityAdmin(admin.ModelAdmin):
    list_display = ('label', 'icon', 'is_active')


@admin.register(models.AgeGroup)
class AgeGroupAdmin(admin.ModelAdmin):
    list_display = ('label', 'is_active')


@admin.register(models.Preferences)
class PreferencesAdmin(admin.ModelAdmin):
    list_display = ('label', 'is_active')


@admin.register(models.TaskIcon)
class TaskIconAdmin(admin.ModelAdmin):
    list_display = ('label', 'icon', 'is_active')


@admin.register(models.TaskStatus)
class TaskStatusAdmin(admin.ModelAdmin):
    list_display = ('label', 'task_status', 'is_active')


@admin.register(models.TaskBadge)
class TaskBadgeAdmin(admin.ModelAdmin):
    list_display = ('label', 'badge_value')


@admin.register(models.NotificationIcon)
class NotificationIconAdmin(admin.ModelAdmin):
    list_display = ('label', 'icon', 'is_active')


@admin.register(models.ServiceCategory)
class ServiceCategoryAdmin(admin.ModelAdmin):
    list_display = ('label', 'icon', 'is_active')


@admin.register(models.ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'icon', 'is_active')


@admin.register(models.Currency)
class CurrencyAdmin(admin.ModelAdmin):
    list_display = ('name', 'is_active')

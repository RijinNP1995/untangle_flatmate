# Create your views here.
from drf_yasg.utils import swagger_auto_schema
from rest_framework.views import APIView

from core import log_data
from expenses import serializers as expense_ser
from expenses.service import ExpenseManagementService
from flatmates_backend import responses

expense_management_service = ExpenseManagementService()


class CreateExpense(APIView):
    """
    create the expense and the following operations
    """

    @swagger_auto_schema(request_body=expense_ser.AddExpensesSerializer)
    def post(self, request):
        """ API to create the group and add the group members"""
        try:
            return expense_management_service.create_expenses_service(user=request.user, data=request.data)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)

    def get(self, request, flat_id):
        """ API to view the expense details of a group member"""
        try:
            return expense_management_service.get_expense_details(user=request.user, flat=flat_id)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)

    @swagger_auto_schema(request_body=expense_ser.UpdateExpensesSerializer)
    def patch(self, request, pk):
        """ API to update the group and group members"""
        try:
            return expense_management_service.update_expense_service(user=request.user, data=request.data,
                                                                     expense_id=pk)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)

    def delete(self, request, pk):
        """API to delete the expense and details"""
        try:
            flat = request.data.get('flat_id')
            return expense_management_service.delete_expense_service(expense_id=pk, user=request.user, flat_id=flat)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)


class RemoveUsers(APIView):
    def delete(self, request):
        """
        :param request:
        :return: Remove the particular participant from particular expense
        """
        try:
            user_id = request.data.get('user_id')
            expense_id = request.data.get('expense_id')
            flat = request.data.get('flat_id')
            return expense_management_service.remove_user_from_expense(user_id=user_id,
                                                                       expense_id=expense_id, flat_id=flat)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)

    def put(self, request):
        try:
            expense_id = request.data.get('expense_id')
            flat_id = request.data.get('flat_id')
            return expense_management_service.settle_expenses(user=request.user, expense_id=expense_id, flat_id=flat_id)
        except Exception as e:
            log_data.error(f'{e.__str__()}')
            responses.exception_response(e)

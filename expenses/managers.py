from flatmates_backend.base_manager import BaseManager


class ExpenseManager(BaseManager):
    # to filter expenses
    def get_expense(self, **filter_args):
        return self.get_all_active().filter(**filter_args)


class CategoryManager(BaseManager):
    def get_category(self, **filter_args):
        return self.get_all().filter(**filter_args)


class ExpenseDetailsManager(BaseManager):
    def get_detail(self, **filter_args):
        return self.get_all().filter(**filter_args)


class CurrencyManager(BaseManager):
    def get_currency(self, **filter_args):
        return self.get_all().filter(**filter_args)





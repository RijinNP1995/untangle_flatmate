# Create your models here.
from django.db import models
from django.utils.translation import ugettext_lazy as _

from expenses import managers as expense_manager
from flat_api.models import Flat
from master import models as master_models
from user.models import User


class Expenses(master_models.BaseModel):
    expense_name = models.CharField(_('expense_title'), max_length=30, blank=True, null=True)
    description = models.CharField(_('expense_description'), max_length=50, blank=True, null=True)
    total_amount = models.DecimalField(_('total_amount'), max_digits=10, decimal_places=3, blank=True, null=True)
    flat_id = models.ForeignKey(Flat, on_delete=models.CASCADE)

    objects = expense_manager.ExpenseManager()

    def __str__(self):
        return self.expense_name.__str__()


class ExpenseDetails(models.Model):
    expense = models.ForeignKey(Expenses, on_delete=models.CASCADE)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    spent_amount = models.DecimalField(_('spent_amount'), max_digits=10, decimal_places=3, blank=True, null=True)
    owe_amount = models.DecimalField(_('owe_amount'), max_digits=10, decimal_places=3, blank=True, null=True)
    settle = models.BooleanField(_('settle_or_not'), default=False)

    objects = expense_manager.ExpenseDetailsManager()

    def __str__(self):
        return self.id.__str__()


class Category(models.Model):
    expense_category = models.ForeignKey(master_models.ExpenseCategory, on_delete=models.CASCADE)
    expense = models.ForeignKey(Expenses, on_delete=models.CASCADE)
    objects = expense_manager.CategoryManager()


class ExpenseCurrency(models.Model):
    currency = models.ForeignKey(master_models.Currency, on_delete=models.CASCADE)
    expense = models.ForeignKey(Expenses, on_delete=models.CASCADE)
    objects = expense_manager.CurrencyManager()

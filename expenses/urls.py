from django.urls import path

from expenses import views

urlpatterns = [
    path('create-expense/', views.CreateExpense().as_view(), name='create_expense'),
    path('get-expenses/<int:flat_id>/', views.CreateExpense().as_view(), name='get_expenses'),
    path('update-expense/<int:pk>/', views.CreateExpense().as_view(), name='update_expense'),
    path('delete-expense/<int:pk>/', views.CreateExpense().as_view(), name='delete_expense'),
    path('remove_user/', views.RemoveUsers().as_view(), name='remove_user_from_expense'),
    path('settle_expense/', views.RemoveUsers().as_view(), name='settle_the_expense')
]

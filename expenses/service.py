from django.db import transaction

from core import log_data
from expenses import models as expense_model
from expenses import serializers as serializer_
from flat_api.models import FlatTenant
from flatmates_backend import responses as message
from master import models as master_model
from notifications.services import multi_notify


class ExpenseManagementService:
    @transaction.atomic
    def create_expenses_service(self, user, data):
        """
        :param user:
        :param data:
        :return: api to add expenses for expense management
        """
        if not user:
            return message.user_not_exist()
        if FlatTenant.objects.filter(tenant=user.id, flat_id=data.get('flat_id')):
            serializer = serializer_.AddExpensesSerializer(data=data)
            if serializer.is_valid(raise_exception=True):
                if data.get('total_amount') == data.get('total_spent_amount'):
                    ser_obj = serializer.save(created_by=user)
                    log_data.warning(f'enter the valid expense_details as a list')
                    if data.get('no_of_users'):
                        no_of_users = int(data.get('no_of_users'))
                        if no_of_users > 0:
                            expense_data_list = [
                                {'expense': ser_obj.id, 'spent_amount': data.get('spent_amount[' + str(i) + ']'),
                                 'owe_amount': data.get('owe_amount[' + str(i) + ']'),
                                 'user_id': data.get('user_id[' + str(i) + ']')} for i in range(no_of_users)]
                            expense_ser = serializer_.AddExpenseDetailsSerializer(data=expense_data_list, many=True)
                            if expense_ser.is_valid(raise_exception=True):
                                expense_ser.save()
                                log_data.warning(f'enter the valid currency')
                    if data.get('currency'):
                        currency_data = {'expense': ser_obj.id, 'currency': data.get('currency')}
                        currency_ser = serializer_.CurrencySerializer(data=currency_data, many=False)
                        if currency_ser.is_valid(raise_exception=True):
                            currency_ser.save()
                        log_data.warning(f'enter the valid category_id ')
                    if data.get('category_id'):
                        expense_category = {'expense_category': data.get('category_id'),
                                            'expense': ser_obj.id, }
                        category_ser = serializer_.CategorySerializer(data=expense_category, many=False)
                        if category_ser.is_valid(raise_exception=True):
                            category_ser.save()
                    user_info = expense_model.ExpenseDetails.objects.filter(expense_id=ser_obj.id)
                    user_list = user_info
                    if user.id in user_list:
                        user_list.remove(user.id)
                    multi_notify(users=user_list, actor=user.display_name,
                                 verb='a new expenses added with you',
                                 action="",
                                 notification_type='NEW_EXPENSE', description='',
                                 icon=master_model.NotificationIcon.objects.get(label='FLAT'),
                                 title="New Expense")
                    expense_id = ser_obj.id
                    return message.create_expense(user, expense_id)
                else:
                    log_data.error(f'spent_amount and total amount is not match')
                    return message.amount_not_match()

            else:
                log_data.error(f'{serializer.errors}')
                return message.invalid_serializer(serializer)
        else:
            log_data.error(f'invalid user user does not exist in this flat')
            return message.invalid_user()

    @transaction.atomic()
    def update_expense_service(self, user, data, expense_id):
        """
        :param expense_id:
        :param user:
        :param data:
        :return: update the expense data
        """
        if FlatTenant.objects.filter(tenant=user.id, flat_id=data.get('flat_id')):
            # get the values according to expense_id
            exp_obj = expense_model.Expenses.objects.filter(pk=expense_id, is_delete=False, is_active=True)
            if exp_obj.exists():
                # serializer to update the group data
                update_ser = serializer_.UpdateExpensesSerializer(data=data, partial=True)
                if update_ser.is_valid(raise_exception=True):
                    if data.get('total_amount') == data.get('total_spent_amount'):
                        exp_obj = update_ser.update(
                            instance=expense_model.Expenses.objects.get(pk=expense_id, is_delete=False),
                            validated_data=update_ser.validated_data)
                        exp_obj.modified_by = user
                        exp_obj.save()
                        log_data.warning(f'enter the valid expense_details as a list')
                        expense_detail = expense_model.ExpenseDetails.objects.filter(expense=exp_obj)
                        if expense_detail:
                            expense_model.ExpenseDetails.objects.get_detail(expense=exp_obj).delete()
                            if data.get('no_of_users'):
                                no_of_users = int(data.get('no_of_users'))
                                if no_of_users > 0:
                                    expense_data_list = [
                                        {'expense': exp_obj.id,
                                         'spent_amount': data.get('spent_amount[' + str(i) + ']'),
                                         'owe_amount': data.get('owe_amount[' + str(i) + ']'),
                                         'user_id': data.get('user_id[' + str(i) + ']')} for i in range(no_of_users)]
                                    expense_ser = serializer_.AddExpenseDetailsSerializer(data=expense_data_list,
                                                                                          many=True)
                                    if expense_ser.is_valid(raise_exception=True):
                                        expense_ser.save()
                            if data.get('currency'):
                                if expense_model.ExpenseCurrency.objects.get_currency(
                                        expense=exp_obj).exists():
                                    expense_model.ExpenseCurrency.objects.get_currency(
                                        expense=exp_obj).delete()
                                currency_data = {'expense': exp_obj.id, 'currency': data.get('currency')}
                                currency_ser = serializer_.CurrencySerializer(data=currency_data,
                                                                              many=False)
                                if currency_ser.is_valid(raise_exception=True):
                                    currency_ser.save()
                                    log_data.warning(f'enter the valid category_id')
                            if data.get('category_id'):
                                if expense_model.Category.objects.get_category(expense=exp_obj).exists():
                                    expense_model.Category.objects.get_category(expense=exp_obj).delete()
                                expense_category = {'expense_category': data.get('category_id'),
                                                    'expense': exp_obj.id, }
                                category_ser = serializer_.CategorySerializer(data=expense_category,
                                                                              many=False)
                                if category_ser.is_valid(raise_exception=True):
                                    category_ser.save()
                            user_info = expense_model.ExpenseDetails.objects.filter(expense_id=exp_obj.id)
                            user_list = user_info
                            if user.id in user_list:
                                user_list.remove(user.id)
                            multi_notify(users=user_list, actor=user.display_name,
                                         verb='Expense data updated with you',
                                         action="",
                                         notification_type='UPDATE_EXPENSE', description='',
                                         icon=master_model.NotificationIcon.objects.get(label='FLAT'),
                                         title="Update Expense")
                            return message.update_group(user)
                    else:
                        log_data.error(f'spent_amount and total amount is not match')
                        return message.amount_not_match()
                else:
                    log_data.error(f'{update_ser.errors} update operation failed')
                    return message.update_failed(update_ser)

            else:
                log_data.error(f'expense id not found')
                return message.no_data_found()

        else:
            log_data.error(f'invalid user user does not exist in this flat')
            return message.invalid_user()

    @transaction.atomic()
    def delete_expense_service(self, expense_id, user, flat_id):
        """
        :param flat_id:
        :param user:
        :param expense_id:
        :return: delete the groups
        """
        if not user:
            return message.user_not_exist()
        if FlatTenant.objects.filter(tenant=user.id, flat_id=flat_id):

            if expense_model.Expenses.objects.filter(pk=expense_id, flat_id=flat_id).exists():
                expense_data = expense_model.Expenses.objects.get(pk=expense_id, flat_id=flat_id)
                expense_data.is_delete = True
                expense_data.save()
                expense_details = expense_model.Expenses.objects.get_expense(flat_id=flat_id, is_delete=False)
                expense_owed_ser = serializer_.ViewExpenseDetailsSerializer(expense_details, many=True)
                return message.delete_group(expense_owed_ser)
            else:
                log_data.error(f' no data found')
                return message.no_data_found()
        else:
            return message.invalid_user()

    def get_expense_details(self, user, flat):
        if FlatTenant.objects.filter(tenant=user.id, flat_id=flat):
            expense_details = expense_model.Expenses.objects.get_expense(flat_id=flat, is_delete=False)
            expense_ser = serializer_.ViewExpenseDetailsSerializer(expense_details, many=True)
            return message.view_expense_details(expense_ser)
        else:
            return message.invalid_user()

    @transaction.atomic()
    def remove_user_from_expense(self, expense_id, user_id, flat_id):
        if expense_model.ExpenseDetails.objects.select_related('expense').filter(expense_id=expense_id).exists():
            if expense_model.ExpenseDetails.objects.select_related('user_id').filter(user_id=user_id).exists():
                expense_model.ExpenseDetails.objects.get_detail(user_id=user_id, expense_id=expense_id).delete()
                expense_details = expense_model.Expenses.objects.get_expense(flat_id=flat_id, is_delete=False,
                                                                             settle=False)
                expense_data_ser = serializer_.ViewExpenseDetailsSerializer(expense_details, many=True)
                log_data.info(f'remove the user from the particular expense')
                return message.remover_user_from_expense(expense_data_ser)
            else:
                log_data.error(f'user id does not exist')
                return message.no_data_found()
        else:
            log_data.error(f'expense id does not exist')
            return message.no_data_found()

    @transaction.atomic()
    def settle_expenses(self, user, expense_id, flat_id):
        if not user:
            return message.user_not_exist()
        if expense_model.Expenses.objects.filter(pk=expense_id).exists():
            expense_settle = expense_model.ExpenseDetails.objects.get(user_id=user.id, expense_id=expense_id)
            if expense_settle:
                expense_settle.settle = True
                expense_settle.save()
                expense_details = expense_model.Expenses.objects.get_expense(flat_id=flat_id, is_delete=False)
                settled_data = serializer_.ViewExpenseDetailsSerializer(expense_details, many=True)
                return message.settle_expense(settled_data)
            else:
                log_data.error(f' no data found')
                return message.no_data_found()
        else:
            log_data.error(f' no data found')
            return message.no_data_found()

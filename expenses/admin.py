from django.contrib import admin

from expenses import models


# Register your models here.


@admin.register(models.Expenses)
class ExpenseAdmin(admin.ModelAdmin):
    list_display = ('expense_name', 'description', 'total_amount',)

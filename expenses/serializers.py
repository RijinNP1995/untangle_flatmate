from rest_framework import serializers

from expenses import models as expense_model
from expenses.models import Category
from master import serializers as master_ser
from user import serializers as user_ser
from user.models import User


class AddExpensesSerializer(serializers.ModelSerializer):
    """
    Serializer class to add the expense
    """

    class Meta:
        model = expense_model.Expenses
        fields = (
            'expense_name',
            'description',
            'total_amount',
            'flat_id'
        )


class AddExpenseDetailsSerializer(serializers.ModelSerializer):
    """
    Serializer class to add the details of expense
    """

    class Meta:
        model = expense_model.ExpenseDetails
        fields = (
            'expense',
            'user_id',
            'spent_amount',
            'owe_amount',
            'settle'
        )


class CurrencySerializer(serializers.ModelSerializer):
    """
    Serializer to add the currency data
    """

    class Meta:
        model = expense_model.ExpenseCurrency
        fields = (
            '__all__'
        )


class CategorySerializer(serializers.ModelSerializer):
    """
    Serializer to add the category data
    """

    class Meta:
        model = expense_model.Category
        fields = (
            '__all__'
        )


class ViewCategorySerializer(serializers.Serializer):
    id = serializers.SerializerMethodField()
    category_name = serializers.SerializerMethodField()
    category_icon = serializers.SerializerMethodField()

    def get_id(self, obj):
        return obj.expense_category.id

    def get_category_name(self, obj):
        return obj.expense_category.name

    def get_category_icon(self, obj):
        icon_obj = master_ser.ExpenseCategorySerializer(obj.expense_category)
        return icon_obj.data['icon']


class ViewCurrencySerializer(serializers.Serializer):
    id = serializers.SerializerMethodField()
    currency_name = serializers.SerializerMethodField()

    def get_id(self, obj):
        return obj.currency.id

    def get_currency_name(self, obj):
        return obj.currency.name


class UpdateExpensesSerializer(serializers.ModelSerializer):
    """
    Serializer class to update the expense
    """

    class Meta:
        model = expense_model.Expenses
        fields = (
            'expense_name',
            'description',
            'total_amount'
        )


class UpdateExpenseDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = expense_model.ExpenseDetails
        fields = (
            'expense',
            'user_id',
            'spent_amount',
            'owe_amount',
            'settle'
        )


class ViewExpenseSerializer(serializers.Serializer):
    user_data = serializers.SerializerMethodField()

    spent_amount = serializers.DecimalField(max_digits=10, decimal_places=3)
    owe_amount = serializers.DecimalField(max_digits=10, decimal_places=3)
    settle = serializers.BooleanField()

    def get_user_data(self, obj):
        if User.objects.filter(id=obj.user_id.id).exists():
            user_info = User.objects.get(id=obj.user_id.id)
            photo_ser = user_ser.UserInfoSerializer(user_info)
            return photo_ser.data
        return


class ViewExpenseDetailsSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    expense_name = serializers.CharField()
    description = serializers.CharField()
    total_amount = serializers.DecimalField(max_digits=10, decimal_places=3)
    expense_detail = serializers.SerializerMethodField()
    created_by = serializers.SerializerMethodField()
    category = serializers.SerializerMethodField()
    currency = serializers.SerializerMethodField()

    def get_created_by(self, obj):
        if User.objects.filter(id=obj.created_by.id).exists():
            user_info = User.objects.get(id=obj.created_by.id)
            photo_ser = user_ser.UserInfoSerializer(user_info)
            return photo_ser.data
        return

    def get_category(self, obj):
        if Category.objects.filter(expense=obj.id):
            expense_category = Category.objects.get_category(expense=obj.id)
            expense_cat_ser = ViewCategorySerializer(expense_category, many=True)
            return expense_cat_ser.data
        return

    def get_currency(self, obj):
        if expense_model.ExpenseCurrency.objects.filter(expense=obj.id):
            currency_data = expense_model.ExpenseCurrency.objects.get(expense=obj.id)
            currency_ser = ViewCurrencySerializer(currency_data, many=False)
            return currency_ser.data
        return

    def get_expense_detail(self, obj):
        if expense_model.ExpenseDetails.objects.filter(expense=obj.id, settle=False):
            expense_detail = expense_model.ExpenseDetails.objects.get_detail(expense=obj.id, settle=False)
            expense_ser = ViewExpenseSerializer(expense_detail, many=True)
            return expense_ser.data
        return
